% For Phys. Rev. C choose (uncomment) one of:
\documentclass[aps,prc,superscriptaddress,showpacs,nofootinbib,twocolumn,
eprint=false ] {revtex4}
% \documentclass[aps,prc,superscriptaddress,showpacs,nofootinbib,floatfix,preprint]{revtex4}

\usepackage{doi,graphicx,amssymb,amsmath,gensymb,aas_macros,mathtools,xcolor,placeins}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}
% \usepackage[section]{placeins}

\usepackage{dcolumn}
\newcolumntype{d}[1]{D{.}{.}{#1}}

\usepackage[cal=boondoxo]{mathalfa}
\newcommand{\unit}[1]{\ensuremath{\,\mathrm{#1}}}
\newcommand{\unitn}[1]{\ensuremath{\mathrm{#1}}}
\newcommand{\sym}{\ensuremath{\mathrm{sym}}}
\newcommand{\etal}{\textit{et al.} }
\newcommand{\etaln}{\textit{et al.}}
\newcommand{\LS}{L\&S }
\newcommand{\LSn}{L\&S}
\newcommand{\LL}{L\&L }
\newcommand{\LLn}{L\&L}
\newcommand{\OO}{O\&O }
\newcommand{\OOn}{O\&O}
\newcommand{\ie}{\textit{i.e.}, }
\newcommand{\todo}[1]{{\color{red}$\blacksquare$~\textsf{[TODO: #1]}}}
\newcommand{\toadd}[1]{{\color{blue}$\blacksquare$~\textsf{[ADD: #1]}}}
\newcommand{\sch}[1]{{\color{purple}$\blacksquare$~\textsf{[A COMMENT: #1]}}}
\newcommand{\rob}[1]{{\color{orange}$\blacksquare$~\textsf{[L COMMENT: #1]}}}
\newcommand{\ott}[1]{{\color{ green}$\blacksquare$~\textsf{[C COMMENT: #1]}}}

\newcommand{\figurewidth}{0.8}
\newcommand{\figurehalfwidth}{0.50}

\usepackage{lipsum}
\usepackage{libertineotf}
\usepackage[percent]{overpic}

\bibliographystyle{aipnum4-1}


\begin{document}

\title{APR EOS }
\author{A. S. Schneider}\email{andschn@caltech.edu}
\affiliation{TAPIR, Walter Burke Institute for Theoretical Physics, MC 350-17, 
California Institute of Technology, Pasadena, CA 91125, USA}
\date{\today}
\begin{abstract}
%\begin{description}
Derivation of Equations for an APREOS code based on the SROEOS code. 

\end{abstract}

% PACS numbers
% 26.30.-k Nucleosynthesis in novae, supernovae, and other explosive environments
% 26.60.-c Nuclear matter aspects of neutron stars
% 26.60.Dd Neutron stars cores
% 97.60.Jd Neutron stars (Late stage evolution)
% 26.50.+x Nuclear physics aspects of Supernovae
% 21.65.Mn Equations of State of nuclear matter
% 26.60.Kp Neutron stars equations of state

\pacs{21.65.Mn,26.50.+x,26.60.Kp}

\maketitle

\section{APR model}\label{sec:APR}


We consider a general Hamiltonian for bulk nucleonic matter of the form
\begin{equation}
\mathcal{H}(n,x,T)=\sum_t\frac{\hbar^2}{2m_t^\star(n,x)}\tau_t(n,x,T)
+ \mathcal{U}(n,x)
\end{equation}
where $n$ is the density, $x$ the proton fraction, $T$ the temperature, 
and $t$ denotes the nucleon isospin ($t=n$ or $p$). 
We will omit the dependences on $n$, $x$, and $T$ from now on. 
The effective masses $m_t^\star$ are given by
\begin{equation}
\frac{\hbar^2}{2m_t^\star} = \frac{\hbar^2}{2m_t} + \mathcal{M}_{t}(n,x),
\end{equation}
the nucleon kinetic energy densities $\tau_t$ and the densities $n_t$ are 
\begin{align}
\tau_t & = \frac{1}{2\pi^2}
\left(\frac{2m_t^\star T}{\hbar^2}\right)^{5/2}\mathcal{F}_{3/2} (\eta_t) 
\label{eq:taut}\\
n_t & = \frac{1}{2\pi^2}
\left(\frac{2m_t^\star T}{\hbar^2}\right)^{3/2}\mathcal{F}_{1/2} (\eta_t) 
\label{eq:nt}
\end{align}
where 
\begin{equation}
\mathcal{F}_k(\eta)=\int\frac{u^k du}{1+\exp(u-\eta)}
\end{equation}
is the Fermi integral. The degeneracy parameters $\eta_t$ are related to the 
chemical potentials $\mu_t$ through 
\begin{equation}\label{eq:etat}
\eta_t=\frac{\mu_t-\mathcal{V}_t}{T}
\end{equation}
where the force potential $\mathcal{V}_t$ are obtained from
\begin{equation}
\mathcal{V}_t\equiv\left.\frac{\delta\mathcal{H}}{\delta 
n_t}\right|_{n_{-t},\tau_{\pm t}}.
\end{equation}


\subsection{The interaction potential}


In the APR model the interaction potential is
\begin{equation}
 \mathcal{U}(n,x) = g_1(n)\left[1-(1-2x)^2\right] + g_2(n)\left[(1-2x)^2\right]
\end{equation}
where $n=n_n+n_p$, $x=n_p/n$, and 
\begin{align}
 -\frac{g_1(n)}{n^2} &= 
\left[p_1+p_2n+p_6n^2+(p_{10}+p_{11}n)e^{-p_9^2n^2}\right] \\
 -\frac{g_2(n)}{n^2} &= 
\left[\frac{p_{12}}{n}+p_7+p_8n+p_{13}e^{-p_9^2n^2}\right].
\end{align}

From $n_n=(1-x)n$ and $n_p=xn$, the interaction potential can also be 
written in the form
\begin{equation}
\mathcal{U}(n_n,n_p)=4\frac{g_1}{n^2}n_tn_{-t}+\frac{g_2}{n^2}\left(n_t-n_{-t}
\right)^2
\end{equation}
and, thus, 
\begin{align}\label{eq:dudn}
n^2
\left.\frac{\partial\mathcal{U}}{\partial n_t}\right|_{\tau_{\pm t},n_{-t}}
=& 
4f_1n_tn_{-t}+f_2(n_t-n_{-t})^2 \nonumber\\
&+4g_1n_{-t}+2g_2(n_t-n_{-t})
\end{align}
where we defined
\begin{align}
 -\frac{f_1(n)}{n^2} 
 &= -\frac{1}{n^2}\left[\frac{dg_1}{dn}-\frac{2g_1}{n}\right]\nonumber\\
 &= \left[p_2 + 2p_6n + 
\left(p_{11}-2p_9^2n(p_{10}+p_{11}n)\right) e^{-p_9^2n^2} \right ] \\
 -\frac{f_2(n)}{n^2} 
 &= -\frac{1}{n^2}\left[\frac{dg_2}{dn}-\frac{2g_2}{n}\right] \nonumber\\
 &= \left[-\frac{p_{12}}{n^2} + p_8 + p_{13}(-2p_9^2ne^{-p_9^2n^2})\right]\,.
\end{align}




\subsection{Effective masses}


The effective masses in the APR model are given by
\begin{equation}
\frac{\hbar^2}{2m_t^\star} = \frac{\hbar^2}{2m_t} + \mathcal{M}_{t}(n,x),
\end{equation}
where
\begin{equation}
 \mathcal{M}_{t}(n,x) = (p_3 n + p_5 n_t)e^{-p_4n}.
\end{equation}

Thus, the force potentials are 
\begin{equation}
 \mathcal{V}_t=\left[
 \tau_t\frac{\partial\mathcal{M}_t}{\partial n_t} + 
 \tau_{-t}\frac{\partial\mathcal{M}_{-t}}{\partial n_t} +
 \frac{\partial\mathcal{U}}{\partial n_t}\right]_{n_t,\tau_{\pm t}}
\end{equation}
where the $\mathcal{M}$ contributions are given by 
\begin{align}
\left.\frac{\partial\mathcal{M}_t}{\partial n_t}\right|_{\tau_{\pm t},n_{-t}}
=&\left(p_3+p_5-p_4(p_3n+p_5n_t)\right)e^{-p_4n}\nonumber\\
=&\frac{1-p_4n}{n}\mathcal{M}_t + p_5n_{-t}e^{-p_4n}\\
\left.\frac{\partial\mathcal{M}_{-t}}{\partial n_t}\right|_{\tau_{\pm t},n_t}
=&\left(p_3-p_4(p_3n+p_5n_{-t})\right)e^{-p_4n}\nonumber\\
=&\frac{1-p_4n}{n}\mathcal{M}_{-t} - p_5n_{-t}e^{-p_4n}
\end{align}
and $\left.\tfrac{\partial\mathcal{U}}{\partial n_t}\right|_{n_t,\tau_{\pm t}}$ 
is given by Eq. \eqref{eq:dudn}.


Effective mass derivatives are obtained from
\begin{align}
 \frac{\partial m_t^\star}{\partial n_{t'}}=
 -\frac{2{m_t^\star}^2}{\hbar^2}\frac{\partial\mathcal{M}_t}{\partial n_{t'}} 
\end{align}



\subsection{Single-particle energy spectra}


\begin{align}\label{eq:single_particle_spectra}
\epsilon_t&=k_t^2
\left.\frac{\partial\mathcal{H}}{\partial\tau_t}\right|_{n_{\pm t},\tau_{-t}}+
\left.\frac{\partial\mathcal{H}}{\partial n_t}\right|_{\tau_{\pm t},n_{-t}} 
=\frac{\hbar^2k_t^2}{2m_t^\star} + \mathcal{V}_t
%&=\frac{\hbar^2k_t^2}{2m_t^\star} + \left.\frac{\partial\left(
%\mathcal{M}_t\tau_t+\mathcal{M}_{-t}\tau_{-t}+\mathcal{U} \right)}
%{\partial n_t}\right|_{\tau_{\pm t},n_{-t}}
\end{align}



\subsection{Internal Energy}


\begin{align}\label{eq:energy}
 e=\frac{1}{n}\left[\sum_t\frac{\hbar^2\tau_t}{2m_t^\star} + \mathcal{U}\right]
\end{align}



\subsection{Entropy}


\begin{align}\label{eq:entropy}
 s=\frac{1}{n}\sum_t\left[\frac{5}{3}\frac{\hbar^2\tau_t}{2m_t^\star T}
- n_t\eta_t\right]
\end{align}


\subsection{Free Energy}

The specific free energy $f=F/n$ is given by
\begin{align}
 f
 &=e-Ts\nonumber\\
 &=\frac{1}{n}\left[\sum_t\frac{\hbar^2\tau_t}{2m_t^\star} + \mathcal{U}\right] 
- \left[\frac{T}{n}\sum_t\left[\frac{5}{3}\frac{\hbar^2\tau_t}{2m_t^\star T}
- n_t\eta_t\right]\right]\nonumber\\
 &=\frac{1}{n}\left[\sum_t\left[-\frac{2}{3}\frac{\hbar^2\tau_t}{2m_t^\star} 
+ n_t\mu_t - n_t\mathcal{V}_t\right] - \mathcal{U}\right].
\end{align}


\subsection{Pressure}


\begin{align}\label{eq:pressure}
 p=
\end{align}




\subsection{Entropy derivation}


(There should be an easier way to derive this!)


To compute the entropy we first write explicitly the grand-canonical partition 
function 
\begin{align}\label{eq:partition_function}
\Omega=-\frac{1}{\beta}\sum_t\int_{\epsilon_{t0}}^{\infty}
d\epsilon_t\mathcal{D}(\epsilon_t)
\log\left[1+\exp\left(-\beta(\epsilon_t-\mu_t)\right)\right]
\end{align}
where $t$ is a sum over isospins, $\beta=1/T$ is the inverse temperature, 
$\mathcal{D}(\epsilon_t)$ is the density of states, and 
\begin{equation}
 p_t=\left[1+\exp\left(\beta(\epsilon_t-\mu_t)\right)\right]^{-1}
\end{equation}
is the Fermi-Dirac distribution function. 
Using usual thermodynamics methods, the density of states is 
\begin{align}
\mathcal{D}(\epsilon) = \sum_{\mathbf k}
\delta\left(\epsilon-\epsilon\left(\mathbf{k} \right)\right)
\rightarrow \frac{L^3}{\left(2\pi\right)^3}\int d\mathbf{k}
\delta\left(\epsilon-\epsilon\left(\mathbf{k}\right)\right)
= \frac{4\pi L^3}{\left(2\pi\right)^3}\int dk k^2
\delta\left(\epsilon-\epsilon\left(k\right)\right)
\end{align}
where $\delta(f(k))$ is the Dirac delta, $\boldsymbol{k}$ is a 
continuous variable in three dimensions, and 
$\epsilon(\boldsymbol{k})\equiv\epsilon(k)$. 
From Eq. \ref{eq:single_particle_spectra} and the relation 
\begin{align}
&\delta(f(k))
=\sum_{k_0} \frac{\delta(k-k_0)}{\left|\left.\tfrac{d}{dk}
(\epsilon-\epsilon\left(k\right))\right|\right|_{k=k_0} }\nonumber\\
&\Rightarrow \delta(\epsilon-\epsilon(k))
=\frac{\delta(k-\sqrt{2m^\star(\epsilon-V)/\hbar^2})}{
\hbar^2\left|k\right|/m^\star }
+\frac{\delta(k+\sqrt{2m^\star(\epsilon-V)/\hbar^2})}{
\hbar^2\left|k\right|/m^\star}
\end{align}
where $k_0=\pm\sqrt{2m^\star(\epsilon-V)/\hbar^2}$ are the zeros of $f(k)$,
we have that 
\begin{align}\label{eq:Dk}
\mathcal{D}(\epsilon) =
\frac{8\pi L^3}{\left(2\pi\right)^3}\frac{m^\star}{\hbar^2}
\sqrt{\frac{2m^\star(\epsilon-V)} {\hbar^2} }=
\frac{L^3}{2\pi^2}\left(\frac{2m^\star}{\hbar^2}\right)^{3/2}\sqrt{\epsilon-V}
\end{align}
and, thus, 
\begin{align}\label{eq:partition_function1}
\Omega &= -\frac{L^3}{2\pi^2\beta} 
\sum_t\left(\frac{2m_t^\star}{\hbar^2}\right)^{3/2}
\int_{0}^{\infty} d\omega_t\sqrt{\omega_t} 
\log\left[1+\exp\left(-\beta\omega_t+\eta_t\right)\right]\nonumber\\
&= -\frac{L^3}{3\pi^2}
\sum_t\left(\frac{2m_t^\star}{\hbar^2}\right)^{3/2}\int_{0}^{\infty}d\omega_t
\frac{\omega_t^{3/2}}{1+\exp\left(\beta\omega_t-\eta_t\right)}\nonumber\\
&= -\frac{L^3}{3\pi^2\beta^{5/2}}
\sum_t\left(\frac{2m_t^\star}{\hbar^2}\right)^{3/2}
\mathcal{F}_{3/2}(\eta_t)\nonumber\\
&= -\frac{2L^3}{3}\sum_t\frac{\hbar^2\tau_t}{2m_t^\star T}
\end{align}
In the first line above we made the replacements $\omega\rightarrow\epsilon-V$
and $\eta=\beta(\mu-V)$. 
The second line was obtaining through an integration by parts, while in the 
third we use the definition of the Fermi-Dirac integral with 
$u\rightarrow\beta\omega$. 
In the final line we used Eq. \ref{eq:taut}.


Finally, the specific entropy $s=S/N$ is given by
\begin{align}\label{eq:entropy_der}
 s=\frac{\beta^2}{N}\left(\frac{\partial\Omega}{\partial\beta}\right)_{\mu}
 & =\frac{\beta^2}{N}\left[-\frac{5}{2}\frac{\Omega}{\beta}
  -\frac{L^3}{3\pi^2\beta^{5/2}}\frac{3}{2}
\sum_t\left(\frac{2m_t^\star}{\hbar^2}
\right)^{3/2}\mathcal{F}_{1/2}(\eta_t)
\frac{\partial\eta_t}{\partial\beta}\right]\nonumber\\
  &=\frac{1}{n}\sum_t\left[\frac{5}{3}\frac{\hbar^2\tau_t}{2m_t^\star T}
- n_t\eta_t\right]
\end{align}
where $n=N/L^3$, 
$\partial\mathcal{F}_k(\eta)/\partial\eta=(k+1)\mathcal{F}_{k-1}$, and Eqs. 
\ref{eq:nt} and \ref{eq:etat} were used. 


Another way to derive the entropy is from from 
%\cite{lattimer:78}
\begin{align}
s= \frac{1}{n}\sum_t \int n_t
\left[p_t\log(p_t)+(1-p_t)\log(1-p_t)\right] \mathcal{D}(\epsilon_t)d\epsilon_t
\end{align}



\subsection{Pressure}


The pressure is easily obtained from the partition function
\begin{align}
 p=n^2\left(\frac{f}{\partial n}\right)_T
\end{align}




\section{Derivatives}\label{app:derivatives}

In this appendix we present the derivatives of the three equilibrium 
conditions, 
Equations \eqref{eq:solve3}, with respect to (w.r.t) the independent of 
variables $\zeta'=(n_{no},n_{po},u)$. Note that unlike \LS we set one of the 
independent variables to be $u$ instead of $n_i$ and, therefore, some of our 
expressions will be different than theirs. Using the simplified notation of \LS 
we have that the equilibrium conditions are of the form
\begin{equation}
 A_k=A_{ki}(x_i,n_i)-B_k(x_i,n_i,u)+A_{ko}(n_{no},n_{po})
\end{equation}
where $A_k$ for $k=1$, $2$ and $3$ refers to $P$, $\mu_n$ and $\mu_p$, 
respectively. Note that $A_{1i}=P_i$ while $A_{1o}=P_o+P_\alpha$. Thus, we may 
write
\begin{widetext}
\begin{subequations}\label{eq:Ak}
\begin{equation}
\frac{\partial A_{ki}}{\partial \zeta_j'}=
\begin{cases}
    \dfrac{\partial A_{ki}}{\partial n_i}\bigg\vert_{x_i}\dfrac{\partial 
n_i}{\partial n_{no}}\bigg\vert_{n_{po},u}
   +\dfrac{\partial A_{ki}}{\partial x_i}\bigg\vert_{n_i}\dfrac{\partial 
x_i}{\partial n_{no}}\bigg\vert_{n_{po},u},& \zeta_j' = n_{no}\\
    \dfrac{\partial A_{ki}}{\partial n_i}\bigg\vert_{x_i}\dfrac{\partial 
n_i}{\partial n_{po}}\bigg\vert_{n_{no},u}
   +\dfrac{\partial A_{ki}}{\partial x_i}\bigg\vert_{n_i}\dfrac{\partial 
x_i}{\partial n_{po}}\bigg\vert_{n_{no},u},& \zeta_j' = n_{po}\\
    \dfrac{\partial A_{ki}}{\partial n_i}\bigg\vert_{x_i}\dfrac{\partial 
n_i}{\partial u}\bigg\vert_{n_{no},n_{po}}
   +\dfrac{\partial A_{ki}}{\partial x_i}\bigg\vert_{n_i}\dfrac{\partial 
x_i}{\partial u}\bigg\vert_{n_{no},n_{po}},& \zeta_j' = u.
\end{cases}
\end{equation}
\begin{equation}
\frac{\partial B_{k}}{\partial \zeta_j'}=
\begin{cases}
    \hspace{1.9cm}
    \dfrac{\partial B_{k}}{\partial n_i}\bigg\vert_{x_i}\dfrac{\partial 
n_i}{\partial n_{no}}\bigg\vert_{n_{po},u}
   +\dfrac{\partial B_{k}}{\partial x_i}\bigg\vert_{n_i}\dfrac{\partial 
x_i}{\partial n_{no}}\bigg\vert_{n_{po},u},& \zeta_j' = n_{no}\\
    \hspace{1.9cm}
    \dfrac{\partial B_{k}}{\partial n_i}\bigg\vert_{x_i}\dfrac{\partial 
n_i}{\partial n_{po}}\bigg\vert_{n_{no},u}
   +\dfrac{\partial B_{k}}{\partial x_i}\bigg\vert_{n_i}\dfrac{\partial 
x_i}{\partial n_{po}}\bigg\vert_{n_{no},u},& \zeta_j' = n_{po}\\
    \dfrac{\partial B_{k}}{\partial u}\bigg\vert_{n_i,x_i}+
    \dfrac{\partial B_{k}}{\partial n_i}\bigg\vert_{x_i}\dfrac{\partial 
n_i}{\partial u}\bigg\vert_{n_{no},n_{po}}
   +\dfrac{\partial B_{k}}{\partial x_i}\bigg\vert_{n_i}\dfrac{\partial 
x_i}{\partial u}\bigg\vert_{n_{no},n_{po}},& \zeta_j' = u.
\end{cases}
\end{equation}
\begin{equation}
\frac{\partial A_{ko}}{\partial \zeta_j'}=
\begin{cases}
    \dfrac{\partial A_{ko}}{\partial n_{no}}\bigg\vert_{n_{po}},& \zeta_j' = 
n_{no}\\
    \dfrac{\partial A_{ko}}{\partial n_{po}}\bigg\vert_{n_{no}},& \zeta_j' = 
n_{po}\\
    0, & \zeta_j' = u.
\end{cases}
\end{equation}
\end{subequations}
\end{widetext}
In the LSEOSF90 code we chose as independent variables $\zeta=(\log n_{no},\log 
n_{po},\log u)$ and not $\zeta'$. However, it is straightforward to obtain 
$\partial A_k/\partial \zeta_j$ from the results above as $n_{no}$, $n_{po}$ and 
$u$ are all independent of each other. The derivatives of $B_k$ w.r.t $u$, $x_i$ 
and $n_i$ are easily obtained from Equations \eqref{eq:B} and, thus, not given. 
We do however write down explicitly the expression for the other derivatives. 
The derivatives of the chemical potentials and pressures w.r.t. the variables 
$n_i$ and $x_i$ may be obtained from the relations
\begin{subequations}
\begin{align}
 \frac{\partial}{\partial n_i}\bigg\vert_{x_i}&=(1-x_i)\frac{\partial}{\partial 
n_{ni}}\bigg\vert_{n_{pi}}+x_i\frac{\partial}{\partial 
n_{pi}}\bigg\vert_{n_{ni}},\\
 \frac{\partial}{\partial 
x_i}\bigg\vert_{n_i}&=n_i\left(\frac{\partial}{\partial 
n_{pi}}\bigg\vert_{n_{ni}}-\frac{\partial}{\partial 
n_{ni}}\bigg\vert_{n_{pi}}\right).
\end{align}
\end{subequations}

Using $r$ and $t$ to denote nucleon isospins, the derivatives of $\mu_r$ with 
respect to densities $n_t$ are
\begin{equation}\label{eq:dmudn}
 \frac{\partial \mu_r}{\partial n_t}\bigg\vert_{n_{-t}}=\frac{T 
G_r}{n_{r}}\left[\delta_{rt}+\frac{3n_{r}m^*_r\alpha_{rt}}{\hbar^2}\right]+\frac
{\partial V_r}{\partial n_t}\bigg\vert_{n_{-t}}.
\end{equation}
Recall that $-t$ ($-r$) stands for the opposite isospin of $t$ ($r$). In 
Equation \eqref{eq:dmudn} above we have that 
$\alpha_{rt}=\alpha_2-(\alpha_2-\alpha_1)\delta_{rt}$, where $\delta_{rt}$ is 
the Kronecker delta,
\begin{equation}
 G_t\equiv\frac{2F_{1/2}(\eta_t)}{F_{-1/2}(\eta_t)},
\end{equation}
and, finally, using Equation \eqref{eq:Skyrme} we obtain
\begin{equation}
 \frac{\partial V_r}{\partial 
n_t}\bigg\vert_{n_{-t}}=\frac{\partial(\alpha_1\tau_r+\alpha_2\tau_{-r})}{
\partial n_t}\bigg\vert_{n_{-t}}+W_{rt}.
\end{equation}
Above,
\begin{equation}
 \frac{\partial\tau_r}{\partial 
n_t}\bigg\vert_{n_{-t}}=\frac{m^*_r}{\hbar^2}\left[3TG_t\delta_{rt}+\left(\frac{
9m^*_r}{\hbar^2}Tn_rG_r-5\tau_r\right)\alpha_{rt}\right]
\end{equation}
and
\begin{align}
 W_{rt}=&2a+c\delta(\delta+1)(n_n+n_p)^{\delta-1}\nonumber\\
        &+4d(\delta-1)(\delta-2)n_nn_p(n_n+n_p)^{\delta-3}\nonumber\\
        &+\delta_{-rt}\left(4b+4d\delta(n_n+n_p)^{\delta-1}\right)\nonumber\\
        &+\delta_{rt}\left(8d(\delta-1)\delta(n_n+n_p)^{\delta-2}n_{-r}\right).
\end{align}

From the chemical potential derivatives we obtain the derivatives of inside and 
outside pressures, $P_o$ and $P_i$, respectively
\begin{equation}\label{eq:dPdn}
 \frac{\partial P}{\partial 
n_t}\bigg\vert_{n_{-t}}=\sum_{r}n_r\frac{\partial\mu_r}{\partial 
n_t}\bigg\vert_{n_{-t}}
\end{equation}

To obtain the derivatives in Equations \eqref{eq:Ak} the following derivatives 
are needed
\begin{subequations}
 \begin{align}\label{eq:dxndtu}
  \frac{\partial n_i}{\partial u}\bigg\vert_{n_{no},n_{po}}&=-\frac{n_1}{u}\\
  \frac{\partial x_i}{\partial 
u}\bigg\vert_{n_{no},n_{po}}&=-\frac{n_2-x_in_1}{un_i}\\
  \frac{\partial n_i}{\partial 
n_{to}}\bigg\vert_{n_{-to},u}&=-\frac{1-u}{u}\left[v_n+v_{np}\frac{\partial 
n_\alpha}{\partial n_{to}}\bigg\vert_{n_{-to}}\right]\\
  \frac{\partial x_i}{\partial 
n_{to}}\bigg\vert_{n_{-to},u}&=\bigg[-\frac{x_i}{n_i}\frac{\partial 
x_i}{\partial n_{to}}\bigg\vert_{n_{-to},u}\nonumber\\        
&\qquad-\frac{1-u}{n_iu}\left(v_n\delta_{tp}+v_p\frac{\partial 
n_\alpha}{\partial n_{to}}\right)\bigg]
 \end{align}
\end{subequations}
where we have defined the relations $n_1=n_i-n_ov_n+4 n_\alpha$, 
$n_2=n_ix_i-n_{po}v_n+2n_\alpha$, $n_o=n_{no}+n_{po}$, $v_n=1-n_\alpha 
v_\alpha$, $v_p=2-n_{po}v_\alpha$ and $v_{np}=4-n_ov_\alpha$.

The deratives of the alpha particles pressure and density are
\begin{align}\label{eq:dnadn}
 \frac{\partial P_\alpha}{\partial n_{to}}\bigg\vert_{n_{-to}}&=T\frac{\partial 
n_\alpha}{\partial n_{to}}\bigg\vert_{n_{-to}}
   =n_\alpha\sum_r(2-n_{ro}v_\alpha)\frac{\partial \mu_{ro}}{\partial 
n_{to}}\bigg\vert_{n_{-to}}.
\end{align}


\section{Thermodynamical relations}\label{app:thermo}


Besides the total pressure of the system $P$, internal energy per nucleon $e$ 
and entropy per nucleon $s$ we also determine the derivatives of these 
quantities w.r.t. the density $n$, temperature $T$ and proton fraction $y$. 
These are needed to obtain adiabatic indices $\Gamma$ and to obtain accurate 
interpolations from our EOS table.
\begin{subequations}\label{eq:dPse}
The pressure derivatives are
 \begin{align}
  \frac{\partial P}{\partial n}&=n\frac{\partial^2 F}{\partial n^2},\\
  \frac{\partial P}{\partial y}&=n\left(\hat{\mu}_o+\frac{\partial^2 F}{\partial 
n \partial y}\right),\\
  \frac{\partial P}{\partial T}&=n\left(s+\frac{\partial^2 F}{\partial n 
\partial T}\right).
 \end{align}
The entropy per nucleon derivatives, meanwhile, are
 \begin{align}
  \frac{\partial s}{\partial n}&=-\frac{1}{n^2}\frac{\partial P}{\partial T},\\
  \frac{\partial s}{\partial y}&=-\frac{1}{n}\frac{\partial^2F}{\partial 
T\partial y},\\
  \frac{\partial s}{\partial T}&=-\frac{1}{n}\frac{\partial^2F}{\partial T^2},
 \end{align}
and, finally, the energy per nucleon derivatives are
 \begin{align}
  \frac{\partial e}{\partial n}&=\frac{P}{n^2}+T\frac{\partial s}{\partial n},\\
  \frac{\partial e}{\partial y}&=-\hat{\mu}_o+T\frac{\partial s}{\partial y},\\
  \frac{\partial e}{\partial T}&=T\frac{\partial s}{\partial T}.
 \end{align}
\end{subequations}

The second derivatives of the free energies $F$ are
\begin{subequations}\label{eq:d2F}
 \begin{align}
  \frac{\partial^2F}{\partial n^2}         &=\frac{\partial\mu_{no}}{\partial 
n}-y\frac{\partial\hat{\mu}_o}{\partial n},\\
  \frac{\partial^2F}{\partial n \partial 
y}&=-\hat{\mu}_o-n\frac{\partial\hat{\mu}_o}{\partial n},\\
  \frac{\partial^2F}{\partial n \partial T}&=\frac{\partial\mu_{no}}{\partial 
T}-y\frac{\partial\hat{\mu}_o}{\partial T},\\
  \frac{\partial^2F}{\partial T^2}         &=-n\frac{\partial s}{\partial T},\\
  \frac{\partial^2F}{\partial T \partial y}&=-n\frac{\partial 
\hat{\mu}_o}{\partial T},\\
  \frac{\partial^2F}{\partial y^2}         &=-n\frac{\partial 
\hat{\mu}_o}{\partial y}.
 \end{align}
\end{subequations}

In order to obtain the derivatives shown in Equations \eqref{eq:dPse} and 
\eqref{eq:d2F} we must determine the derivatives of the quantities 
$\xi\equiv(\mu_{no},\mu_{po},s)$ w.r.t. the parameters $\lambda\equiv(n,T,y)$. 
Noting that the quantities $\xi$ are explicit functions of one or more of the 
variables $z\equiv(u,n_i,y_i,\eta_{no},\eta_{po})$ we may write
\begin{equation}
\frac{\partial\xi_i}{\partial\lambda_j}=\frac{\partial\xi_i}{\partial\lambda_j}
\bigg\vert_{z}
 +\frac{\partial\xi_i}{\partial z_k}\bigg\vert_{z_{-k}}\frac{\partial 
z_k}{\partial\lambda_i}
\end{equation}
where repeated indices imply summation and $\lambda_{-j}$ ($z_{-k}$) denotes the 
set $\lambda$ ($z$) excluding the parameter $j$ (variable $k$).
The first RHS term may be directly obtained from the expressions for the 
quantities $\xi$.
As neither the density $n$ nor the proton fraction $y$ appear explicitly in the 
equations for $\xi$ only the derivatives w.r.t. $T$ are nonzero.
We start with the chemical potential $\mu_{to}$  derivatives recalling Equation 
\eqref{eq:eta} which implies that
\begin{equation}
 \mu_t=\mathcal{V}_t+T\eta_t.
\end{equation}
We will omit for now the index $o$. Thus,
\begin{equation}
 \frac{\partial\mu_t}{\partial T}\bigg\vert_{z}=\frac{\partial 
\mathcal{V}_t}{\partial T}+\eta_t.
\end{equation}
The $\mathcal{V}_t$ derivatives are given by
\begin{align}
 \frac{\partial \mathcal{V}_t}{\partial 
T}=&\frac{\partial(\alpha_1\tau_t+\alpha_2\tau_{-t})}{\partial 
T}+4b\frac{\partial n_{t}}{\partial T}\nonumber\\
 &+\frac{\partial (n_n+n_p)}{\partial T}\bigg[c(\delta+1)\delta(n_n+n_p)^{\delta-1}\nonumber\\
 &+4d(\delta-1)(\delta-2)(n_n+n_p)^{\delta-3}n_nn_p\nonumber\\
 &+4d(\delta-1)(n_n+n_p)^{\delta-2}n_{-t}\bigg]\nonumber\\
 &+4d(\delta-1)(n_n+n_p)^{\delta-2}\left[n_{t}\frac{\partial n_{-t}}{\partial T}+n_{-t}\frac{\partial n_{t}}{\partial T}\right]\nonumber\\
 &+4d(n_n+n_p)^{\delta-1}\frac{\partial n_{-t}}{\partial T}.
\end{align}
From Equation \eqref{eq:taut} we have that
\begin{equation}
 \frac{\partial\tau_t}{\partial T}=\frac{5}{2}\frac{\tau_t}{T}+\frac{5}{2}\frac{\tau_t}{m_t^*}\frac{\partial m_t^*}{\partial T}
\end{equation}
where
\begin{equation}
 \frac{\partial m_t^*}{\partial T}=-\frac{2{m_t^*}^2}{\hbar^2}\left(\alpha_1\frac{\partial n_t}{\partial T}+\alpha_2\frac{\partial n_{-t}}{\partial T}\right).
\end{equation}
We are left to evaluate the density derivatives w.r.t. temperature which are given by
\begin{equation}
 \frac{\partial n_{t}}{\partial T}=\frac{\kappa_{-t1}\lambda_{t}-\kappa_{t2}\lambda_{-t}}{\kappa_{t1}\kappa_{-t1}-\kappa_{t2}\kappa_{-t2}}
\end{equation}
where we defined the quantities
\begin{equation}
 \kappa_{ti}=\delta_{1i}+\frac{3\alpha_in_tm_t^*}{\hbar^2}
 \quad\mathrm{and}\quad
 \lambda_{t}=\frac{3}{2}\frac{n_t}{T}.
\end{equation}

Next we compute the entropy density $s$ derivative w.r.t. temperature $T$ at constant $z$.
This calculation is not as straightforward as the derivatives of the chemical potentials shown above.
To simplify the procedure we start writing the total nuclear entropy $S=ns$ as
\begin{equation}
 S=S_o+S_\alpha+S_i+S_{SC}+S_H
\end{equation}
where the subscripts $o$, $\alpha$, $i$, $SC$ and $H$ denote respectively the contributions to the entropy $S$ of outside nucleons, alpha particles, inside nucleons, combined Coulomb and surface terms, and translational terms. Each contribution to the total entropy $S$ is easily obtained from the free energy $F$ and the thermodynamical relation $S=-\partial F/\partial T$. Thus,
\begin{subequations}\label{eq:S}
 \begin{align}
  S_o&= (1-u)(1-n_\alpha v_\alpha)\sum_t\left(\frac{5\hbar^2\tau_{to}}{6m^*_{to}T}-n_{to}\eta_{to}\right)\\
  S_\alpha &=(1-u)n_\alpha\left(\frac{5T}{2}-\mu_\alpha\right)\\
  S_i&= u\sum_t\left(\frac{5\hbar^2\tau_{ti}}{6m^*_{ti}T}-n_{ti}\eta_{ti}\right)\\
  S_{SC}&=-\frac{2}{3}\frac{\beta\mathcal{D}(u)}{h}\frac{\partial h}{\partial T}\\
  S_{H} &=-F_H\left(\frac{1}{h}\frac{\partial h}{\partial T}-\frac{1}{\mu_H-T}\left(\frac{\partial \mu_H}{\partial T}-1\right)\right).
 \end{align}
\end{subequations}
From Equations \eqref{eq:S} it is straightforward to determine $\partial s/\partial T\vert_{z}$.

\begin{equation}
 \frac{\partial\mu_r}{\partial\eta_t}=T\delta_{rt} + \frac{\partial \mu_r}{\partial n_n}\frac{\partial n_n}{\partial \eta_t} + \frac{\partial \mu_r}{\partial n_p}\frac{\partial n_p}{\partial \eta_t}
\end{equation}

\begin{equation}
 \frac{\partial\tau_r}{\partial\eta_t}= \frac{\partial \tau_r}{\partial n_n}\frac{\partial n_n}{\partial \eta_t} + \frac{\partial \tau_r}{\partial n_p}\frac{\partial n_p}{\partial \eta_t}
\end{equation}

\begin{equation}
 \frac{\partial V_r}{\partial\eta_t}=\frac{\partial V_r}{\partial n_n}\frac{\partial n_n}{\partial \eta_t} + \frac{\partial V_r}{\partial n_p}\frac{\partial n_p}{\partial \eta_t}
\end{equation}

\begin{equation}
 \frac{\partial n_r}{\partial\eta_t}= \frac{n_t}{G_t}\frac{(-1)^{j-1}\kappa_{-rj}}{\kappa_{t1}\kappa_{-t1}-\kappa_{t2}\kappa_{-t2}}
\end{equation}
where $j=1$ if $r=t$ and $j=2$ if $r\neq t$.

\begin{equation}
 \frac{\partial P_o}{\partial \eta_t}=\frac{\partial P_o}{\partial n_n}\frac{\partial n_n}{\partial \eta_t} + \frac{\partial P_o}{\partial n_p}\frac{\partial n_p}{\partial \eta_t}
\end{equation}
with the pressure derivatives being given by Equation \eqref{eq:dPdn}.

The alpha particle chemical potential and pressure derivatives are
\begin{equation}
 \frac{\partial P_\alpha}{\partial \eta_t}=T\frac{\partial n_\alpha}{\partial \eta_t}
\end{equation}
where
\begin{equation}
 \frac{\partial n_\alpha}{\partial \eta_t}=\frac{\partial n_\alpha}{\partial n_n}\frac{\partial n_n}{\partial \eta_t} + \frac{\partial n_\alpha}{\partial n_p}\frac{\partial n_p}{\partial \eta_t}
\end{equation}
and ${\partial n_\alpha}/{\partial n_t}$ is given by Equation \eqref{eq:dnadn}

We also have
\begin{equation}
 \frac{\partial \mu_\alpha}{\partial \eta_t}=2\left(\frac{\partial \mu_n}{\partial \eta_t}+\frac{\partial \mu_p}{\partial \eta_t}\right)-v_\alpha\frac{\partial P_o}{\partial \eta_t}
\end{equation}
where ${\partial \mu_r}/{\partial \eta_t}$ was given above.

Other derivatives
\begin{equation}
 \frac{\partial n_t}{\partial T}\bigg\vert_{z}=0
\end{equation}

\begin{equation}
 \frac{\partial \eta_{ti}}{\partial T}\bigg\vert_{z}=-\frac{3}{2}\frac{G_{ti}}{T}
\end{equation}

\begin{equation}
 \frac{\partial\tau_{ri}}{\partial T}= \frac{5}{2}\frac{\tau_{ri}}{T}-\frac{9}{2}\frac{m_{ri}^*n_{ri}G_{ni}}{\hbar^2}
\end{equation}

\begin{equation}
 \frac{\partial V_{ri}}{\partial T}= \alpha_1\frac{\partial\tau_{ri}}{\partial T} +  \alpha_2\frac{\partial\tau_{-ri}}{\partial T}
\end{equation}

\begin{equation}
 \frac{\partial P_i}{\partial T}= \frac{\hbar^2}{2}\left(\frac{5}{3m_{ni}^*}-\frac{1}{m_n}\right)+\frac{\hbar^2}{2}\left(\frac{5}{3m_{pi}^*}-\frac{1}{m_p}\right)
\end{equation}

\begin{equation}
 \frac{\partial \mu_{ri}}{\partial T}= \eta_{ri} + T\frac{\partial\eta_{ri}}{\partial T} + \frac{\partial V_{ri}}{\partial T}
\end{equation}



\end{document}



