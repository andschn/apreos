!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published
!    by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
Module Maxwell_Energy_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE MAXWELL_ENERGY(n, T, y, &
                        F_o, F_i, F_alpha, F_TR, F_SC, U_APR, &
                        n_no, n_po, n_ni, n_pi, n_alpha, n_heavy, &
                        A_heavy, Z_heavy, rad, F, P, S, E, &
                        DF_Dn, DF_Dy, DF_DT, DP_Dn, DP_Dy, DP_DT, &
                        DS_Dn, DS_Dy, DS_DT, DE_Dn, DE_Dy, DE_DT, &
                        mu_n, mu_p, Meff_n, Meff_p,  &
                        Dmu_n_DT, Dmu_n_Dn, Dmu_n_Dy, &
                        Dmu_p_DT, Dmu_p_Dn, Dmu_p_Dy )

    USE Kind_Types_Mod, ONLY : I4B, DP, LGCL
    USE Global_Variables_Mod, only : IS_TEST
    USE Physical_Constants_Mod, ONLY : zero, one
    USE Do_Maxwell_Mod
    USE Maxwell_Free_Energy_Mod 
    USE Alpha_Properties_Mod, ONLY : ALPHA_PROPERTIES
    USE Adiabatic_Index_Mod

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: n, T, y
    REAL(DP), INTENT(OUT) :: F_o, F_i, F_alpha, F_TR, F_SC, U_APR
    REAL(DP), INTENT(OUT) :: n_no, n_po, n_ni, n_pi, n_alpha, n_heavy
    REAL(DP), INTENT(OUT) :: A_heavy, Z_heavy, rad, F, P, S, E
    REAL(DP), INTENT(OUT) :: DF_Dn, DF_Dy, DF_DT, DP_Dn, DP_Dy, DP_DT
    REAL(DP), INTENT(OUT) :: DS_Dn, DS_Dy, DS_DT, DE_Dn, DE_Dy, DE_DT
    REAL(DP), INTENT(OUT) :: mu_n, mu_p, Meff_n, Meff_p
    REAL(DP), INTENT(OUT) :: Dmu_n_DT, Dmu_n_Dn, Dmu_n_Dy
    REAL(DP), INTENT(OUT) :: Dmu_p_DT, Dmu_p_Dn, Dmu_p_Dy

    REAL(DP) :: nl, nh
    REAL(DP) :: v, DvDn

    REAL(DP) :: Ul_APR,Uh_APR
    REAL(DP) :: Meffnl,Meffpl,Meffnh,Meffph
    REAL(DP) :: Fl, DFl_Dn, DFl_Dy, DFl_DT
    REAL(DP) :: Fh, DFh_Dn, DFh_Dy, DFh_DT
    REAL(DP) :: El, DEl_Dn, DEl_Dy, DEl_DT
    REAL(DP) :: Eh, DEh_Dn, DEh_Dy, DEh_DT
    REAL(DP) :: Sl, DSl_Dn, DSl_Dy, DSl_DT
    REAL(DP) :: Sh, DSh_Dn, DSh_Dy, DSh_DT
    REAL(DP) :: Pl, DPl_Dn, DPl_Dy, DPl_DT
    REAL(DP) :: Ph, DPh_Dn, DPh_Dy, DPh_DT
    REAL(DP) :: munl, Dmunl_Dn, Dmunl_Dy, Dmunl_DT
    REAL(DP) :: munh, Dmunh_Dn, Dmunh_Dy, Dmunh_DT
    REAL(DP) :: mupl, Dmupl_Dn, Dmupl_Dy, Dmupl_DT
    REAL(DP) :: muph, Dmuph_Dn, Dmuph_Dy, Dmuph_DT

    REAL(DP) :: muhatl, muhath
    REAL(DP) :: Dmuhatl_Dn, Dmuhath_Dn
    REAL(DP) :: Dmuhatl_DT, Dmuhath_DT
    REAL(DP) :: Dmuhatl_Dy, Dmuhath_Dy
    REAL(DP) :: DvDnn, DvDnp
    REAL(DP) :: D2vDnnDn, D2vDnpDn
    REAL(DP) :: D2vDnnDy, D2vDnpDy
    REAL(DP) :: D2vDnnDT, D2vDnpDT

    REAL(DP) :: D2Fh_Dy2, D2Fl_Dy2

    REAL(DP) :: e_alpha, p_alpha, s_alpha, mu_alpha, gamma

    INTEGER(I4B) :: objective

    LOGICAL(LGCL) :: do_maxwell

!   set all output to zero
    F_o = zero
    F_i = zero
    F_alpha = zero
    F_TR = zero
    F_SC = zero
    U_APR = zero

    n_no = zero
    n_po = zero
    n_ni = zero
    n_pi = zero
    n_alpha = zero
    n_heavy = zero

    A_heavy = zero
    Z_heavy = zero

    rad = zero
    F = zero
    P = zero
    S = zero
    E = zero

    DF_Dn = zero
    DF_Dy = zero
    DF_DT = zero

    DP_Dn = zero
    DP_Dy = zero
    DP_DT = zero

    DS_Dn = zero
    DS_Dy = zero
    DS_DT = zero

    DE_Dn = zero
    DE_Dy = zero
    DE_DT = zero

    CALL DO_MAXWELL_CONSTRUCTION(n,y,T,nl,nh,do_maxwell)

    v = (n-nl)/(nh-nl)
    DvDn = one/(nh-nl)

    CALL MAXWELL_FREE_ENERGY (n,y,T,nl,nh,Ul_APR,Uh_APR,&
                              Meffnl,Meffpl,Meffnh,Meffph,&
                              Fl, DFl_Dn, DFl_Dy, DFl_DT,&
                              Fh, DFh_Dn, DFh_Dy, DFh_DT,&
                              El, DEl_Dn, DEl_Dy, DEl_DT,&
                              Eh, DEh_Dn, DEh_Dy, DEh_DT,&
                              Sl, DSl_Dn, DSl_Dy, DSl_DT,&
                              Sh, DSh_Dn, DSh_Dy, DSh_DT,&
                              Pl, DPl_Dn, DPl_Dy, DPl_DT,&
                              Ph, DPh_Dn, DPh_Dy, DPh_DT,&
                              munl, Dmunl_Dn, Dmunl_Dy, Dmunl_DT,&
                              munh, Dmunh_Dn, Dmunh_Dy, Dmunh_DT,&
                              mupl, Dmupl_Dn, Dmupl_Dy, Dmupl_DT,&
                              muph, Dmuph_Dn, Dmuph_Dy, Dmuph_DT )

!   compute total thermodynamical quantities
    U_APR = v*Uh_APR + (one-v)*Ul_APR

    F = v*Fh + (one-v)*Fl

    DF_Dn = DvDn*(Fh - Fl)
    DF_DT = v*DFh_DT + (one-v)*DFl_DT
    DF_Dy = v*DFh_Dy + (one-v)*DFl_Dy

    S = v*Sh + (one-v)*Sl

    DS_Dn = DvDn*(Sh - Sl)
    DS_DT = v*DSh_DT + (one-v)*DSl_DT
    DS_Dy = v*DSh_Dy + (one-v)*DSl_Dy

    E = F + T*S

    DE_Dn = DF_Dn + T*DS_Dn
    DE_DT = DF_DT + T*DS_DT + S
    DE_Dy = DF_Dy + T*DS_Dy

    P = - v*Fh - (one-v)*Fl + n*DvDn*(Fh - Fl)

    DP_Dn = zero
    DP_DT = - v*DFh_DT - (one-v)*DFl_DT + n*DvDn*(DFh_DT - DFl_DT)
    DP_Dy = - v*DFh_Dy - (one-v)*DFl_Dy + n*DvDn*(DFh_Dy - DFl_Dy)

    muhatl = munl - mupl
    muhath = munh - muph

    Dmuhatl_Dn = Dmunl_Dn - Dmupl_Dn
    Dmuhath_Dn = Dmunh_Dn - Dmuph_Dn

    Dmuhatl_DT = Dmunl_DT - Dmupl_DT
    Dmuhath_DT = Dmunh_DT - Dmuph_DT

    Dmuhatl_Dy = Dmunl_Dy - Dmupl_Dy
    Dmuhath_Dy = Dmunh_Dy - Dmuph_Dy

    mu_n = -      y /n*(v*DFh_Dy + (one-v)*DFl_Dy) + DvDn*(Fh - Fl)
    mu_p = - (y-one)/n*(v*DFh_Dy + (one-v)*DFl_Dy) + DvDn*(Fh - Fl)

    Dmu_n_Dn  =      y /n*(v/n*DFh_Dy + (one-v)/n*DFl_Dy - DvDn*(DFh_Dy-DFl_Dy))
    Dmu_p_Dn  = (y-one)/n*(v/n*DFh_Dy + (one-v)/n*DFl_Dy - DvDn*(DFh_Dy-DFl_Dy))

    Dmu_n_DT  =      y /n*(v*DSh_Dy + (one-v)*DSl_Dy) - DvDn*(Sh - Sl)
    Dmu_p_DT  = (y-one)/n*(v*DSh_Dy + (one-v)*DSl_Dy) - DvDn*(Sh - Sl)

!    write (*,*) DFh_Dy, -(nh*munh-Ph-Fh)/y, (nh*muph-Ph-Fh)/(one-y)
!    write (*,*) DFl_Dy, -(nl*munl-Pl-Fl)/y, (nl*mupl-Pl-Fl)/(one-y)

    D2Fh_Dy2 = -(DFh_Dy+n*Dmunh_Dy-DPh_Dy-DFh_Dy)/y
    D2Fl_Dy2 = -(DFl_Dy+n*Dmunl_Dy-DPl_Dy-DFl_Dy)/y

    Dmu_n_Dy  = mu_n/ y      -      y /n*(v*D2Fh_Dy2 + (one-v)*D2Fl_Dy2) &
              + DvDn*(DFh_Dy - DFl_Dy)
    Dmu_p_Dy  = mu_p/(y-one) - (one-y)/n*(v*D2Fh_Dy2 + (one-v)*D2Fl_Dy2) &
              + DvDn*(DFh_Dy - DFl_Dy)

    Meff_n = v*Meffnh + (one-v)*Meffnl
    Meff_p = v*Meffph + (one-v)*Meffpl

!   get alpha particle's properties (?)

    n_no = n*(one-y)
    n_po = n*y

    objective = 0

    CALL ALPHA_PROPERTIES(objective, n_no, n_po, mu_n, mu_p, T, F, P, &
     n_alpha, mu_alpha, p_alpha, s_alpha, e_alpha, f_alpha)

    CALL GET_ADIABATIC_INDEX (n,one,y,T,P,S,E,&
         DP_Dn,DP_DT,DS_Dn,DS_DT,DE_Dn,DE_DT,gamma)

  END SUBROUTINE MAXWELL_ENERGY

END MODULE Maxwell_Energy_Mod
