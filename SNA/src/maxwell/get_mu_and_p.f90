!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published
!    by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
Module Get_Mu_and_P_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE GET_MU_AND_P (n, y, T, mu, DmuDn, P, DPDn, flag)

    USE Kind_Types_Mod, ONLY : I4B, DP
    USE Global_Variables_Mod, only : IS_TEST
    USE Get_Uniform_Variables_Mod
    USE Find_Uniform_Solution_Mod, ONLY : Find_Uniform_Solution
    USE Physical_Constants_Mod, ONLY : temp_mev_to_kelvin, &
        press_cgs_to_eos,rho_cgs_to_EOS, Mass_e, ZERO, ONE
!   USE Global_Variables_Mod, ONLY : include_muons
!   USE wrap, ONLY : wrap_timmes
    USE Free_Energy_Mod, ONLY : FREE_ENERGY

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: n, y, T
    INTEGER(I4B), INTENT(IN) :: flag

    REAL(DP), INTENT(OUT) :: mu, DmuDn
    REAL(DP), INTENT(OUT) :: P,  DPDn

    REAL(DP) :: UNIFORM_SOL_GUESS(1), UNIFORM_SOL(1)
    REAL(DP) :: F_uniform, uniform_residue

    LOGICAL :: retry, uniform_solution

    REAL(DP) :: Abar, Zbar, rho, T_K
    REAL(DP) :: etot, ptot, stot
    REAL(DP) :: dedT, dpdT, dsdT
    REAL(DP) :: dedr, dpdr, dsdr
    REAL(DP) :: dedy, dpdy, dsdy
    REAL(DP) :: gamma, sound
    REAL(DP) :: etaele, detadT, detadr, detady
    REAL(DP) :: xxe, xxp, xxmu, xxam

    REAL(DP) :: mu_e, Dmu_e_Dn
    REAL(DP) :: P_e, DP_e_Dn

    REAL(DP) :: log10_n_p, log10_n_n, log10_u
    REAL(DP) :: F_o, F_i, F_alpha, F_TR, F_SC, U_APR
    REAL(DP) :: n_ni, n_pi, n_alpha, n_heavy
    REAL(DP) :: A_heavy, Z_heavy, rad
    REAL(DP) :: Meff_n, Meff_p
    REAL(DP) :: dLog10n_n_dn, dLog10n_p_dn, dLog10u_dn
    REAL(DP) :: dLog10n_n_dT, dLog10n_p_dT, dLog10u_dT
    REAL(DP) :: non_uniform_eq(3), non_uniform_jac(3,3)

    REAL(DP) :: F, S, E
    REAL(DP) :: DF_Dn, DF_Dy, DF_DT, DP_Dn, DP_Dy, DP_DT
    REAL(DP) :: DS_Dn, DS_Dy, DS_DT, DE_Dn, DE_Dy, DE_DT
    REAL(DP) :: n_n, n_p, mu_n, mu_p
    REAL(DP) :: Dmu_n_DT, Dmu_n_Dn, Dmu_n_Dy
    REAL(DP) :: Dmu_p_DT, Dmu_p_Dn, Dmu_p_Dy

    REAL(DP) :: x_in, n_in
    REAL(DP) :: Dmul_Dn, Dmuh_Dn

    INTEGER(I4B) :: objective, error

!---------------------------------------------------------------------------
!   get electron properties
!   need e- chemical potential for total chemical potential

    Abar = 1.d0
    Zbar = y

!   convert temperature to K and density to g/cm^3 before call to wrap_timmes
    T_K = T*temp_mev_to_kelvin
    rho = n/rho_cgs_to_EOS

!   CALL WRAP_TIMMES(Abar,Zbar,rho,T_K,etot,ptot,stot,&
!       dedT,dpdT,dsdT,dedr,dpdr,dsdr,dedy,dpdy,dsdy,gamma,&
!       etaele,detadT,detadr,detady,&
!       sound,xxe,xxp,xxmu,xxam,include_muons)

!   mu_e     = T*etaele + Mass_e
!   dmu_e_dn = T*detadr/rho_cgs_to_EOS
!   P_e      = ptot*press_cgs_to_eos
!   dP_e_dn  = dpdr*press_cgs_to_eos/rho_cgs_to_eos

!---------------------------------------------------------------------------
!   get nucleon properties

    objective = 3

    retry = .false.
    uniform_solution = .false.

    UNIFORM_SOL_GUESS = zero

!    CALL Find_Uniform_Solution(n,T,y,UNIFORM_SOL_GUESS,UNIFORM_SOL,&
!                   F_uniform,uniform_residue,retry,uniform_solution)

!    x_in = uniform_sol(1)

!    CALL GET_UNIFORM_VARIABLES (n, y, x_in, log10_n_p, log10_n_n, log10_u)

    log10_n_p = log10(n*y)
    log10_n_n = log10(n*(one-y))
    log10_u   = -290.d0

    CALL FREE_ENERGY( log10_n_n, log10_n_p, log10_u, n, T, y, flag, &
                      F_o, F_i, F_alpha, F_TR, F_SC, U_APR, &
                      n_n, n_p, n_ni, n_pi, n_alpha, n_heavy, &
                      A_heavy, Z_heavy, rad, F, P, S, E, &
                      DF_Dn, DF_Dy, DF_DT, DP_Dn, DP_Dy, DP_DT, &
                      DS_Dn, DS_Dy, DS_DT, DE_Dn, DE_Dy, DE_DT, &
                      mu_n, mu_p, Meff_n, Meff_p, &
                      Dmu_n_DT, Dmu_n_Dn, Dmu_n_Dy, &
                      Dmu_p_DT, Dmu_p_Dn, Dmu_p_Dy, &
                      dLog10n_n_dn, dLog10n_p_dn, dLog10u_dn, &
                      dLog10n_n_dT, dLog10n_p_dT, dLog10u_dT, &
                      non_uniform_eq, non_uniform_jac, objective, error )

    mu = (one-y)*mu_n + y*(mu_p + zero*mu_e)
    DmuDn = (one-y)*dmu_n_dn + y*(dmu_p_dn + zero*dmu_e_dn)

!    write (*,*) n, mu_n, mu_p, mu_e
!    write (*,*) n, dmu_n_dn, dmu_p_dn, dmu_e_dn
!    read  (*,*)

    P = P !+ P_e
    DPDn = DP_Dn !+ DP_e_Dn

  END SUBROUTINE GET_MU_AND_P

END Module Get_Mu_and_P_Mod
