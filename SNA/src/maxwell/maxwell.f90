!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published
!    by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
Module Do_Maxwell_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE DO_MAXWELL_CONSTRUCTION(n_max,y_max,T_max,nl,nh,&
                                     max_construction)
!   USE Allocatable_Tables_Mod
    USE Kind_Types_Mod, ONLY : I4B, DP, LGCL
    USE Global_Variables_Mod, only : IS_TEST
    USE Get_Mu_and_P_Mod
    USE Main_output_Mod
    USE Skyrme_Potential_Mod, ONLY : Transition_Fit

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: n_max, y_max, T_max
    REAL(DP), INTENT(OUT) :: nl, nh
    LOGICAL(LGCL), INTENT(OUT) :: max_construction

    REAL(DP) :: n_trans

    REAL(DP) :: nl0, nh0
    REAL(DP) :: den, sqrt_dmuldn, sqrt_dmuhdn
    REAL(DP) :: Pl, Ph
    REAL(DP) :: DPl_Dn, DPh_Dn
    REAL(DP) :: mul, muh
    REAL(DP) :: Dmul_Dn, Dmuh_Dn

    REAL(DP) :: fm, gm
    REAL(DP) :: Df_Dnl, Df_Dnh
    REAL(DP) :: Dg_Dnl, Dg_Dnh

    INTEGER(I4B) :: i, j, k


    max_construction = .false.
!   perform Maxwell construction for LDP-HDP phase transition
!   Move this to subroutine in the future
    n_trans = Transition_Fit(y_max)

!---------------------------------------------------------------------------
!   get physical properties of LDP
    CALL GET_MU_AND_P (n_trans, y_max, T_max, mul, Dmul_Dn, Pl, DPl_Dn, -1)

!---------------------------------------------------------------------------
!   get physical properties of HDP
    CALL GET_MU_AND_P (n_trans, y_max, T_max, muh, Dmuh_Dn, Ph, DPh_Dn, +1)

!---------------------------------------------------------------------------
!   compute initial guess for coexistence boundaries

    sqrt_dmuldn = sqrt(dmul_dn)
    sqrt_dmuhdn = sqrt(dmuh_dn)

    den = sqrt_dmuldn + sqrt_dmuhdn

    nl0 = n_trans + (muh - mul)/den/sqrt_dmuldn
    nh0 = n_trans + (mul - muh)/den/sqrt_dmuhdn

    if (n_max > nl0 -0.01d0 .and. n_max < nh0 + 0.01d0) then

      nl = nl0
      nh = nh0

      CALL GET_MU_AND_P (nl, y_max, T_max, mul, Dmul_Dn, Pl, DPl_Dn, -1)
      CALL GET_MU_AND_P (nh, y_max, T_max, muh, Dmuh_Dn, Ph, DPh_Dn, +1)

      fm = Pl - Ph
      gm = mul - muh

      Df_Dnl = + DPl_Dn
      Df_Dnh = - DPh_Dn

      Dg_Dnl = + Dmul_Dn
      Dg_Dnh = - Dmuh_Dn

      do while (abs(fm) > 1.d-6 .and. abs(gm) > 1.d-6)

        nl = nl + (fm*Dg_Dnh - gm*Df_Dnh) / &
                  (Df_Dnh*Dg_Dnl - Df_Dnl*Dg_Dnh)

        nh = nh + (fm*Dg_Dnl - gm*Df_Dnl) / &
                  (Df_Dnl*Dg_Dnh - Df_Dnh*Dg_Dnl)

        CALL GET_MU_AND_P (nl, y_max, T_max, mul, Dmul_Dn, Pl, DPl_Dn, -1)
        CALL GET_MU_AND_P (nh, y_max, T_max, muh, Dmuh_Dn, Ph, DPh_Dn, +1)

        fm = Pl - Ph
        gm = mul - muh

        Df_Dnl = + DPl_Dn
        Df_Dnh = - DPh_Dn

        Dg_Dnl = + Dmul_Dn
        Dg_Dnh = - Dmuh_Dn

      enddo

      if (n_max > nl .and. n_max < nh) max_construction = .true.

    endif


  END SUBROUTINE DO_MAXWELL_CONSTRUCTION

END Module Do_Maxwell_Mod
