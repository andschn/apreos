!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published
!    by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
Module Maxwell_Free_Energy_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE MAXWELL_FREE_ENERGY (n,y,T,nl,nh,Ul_APR,Uh_APR,  &
                                  Meffnl,Meffpl,Meffnh,Meffph,&
                                  Fl, DFl_Dn, DFl_Dy, DFl_DT, &
                                  Fh, DFh_Dn, DFh_Dy, DFh_DT, &
                                  El, DEl_Dn, DEl_Dy, DEl_DT, &
                                  Eh, DEh_Dn, DEh_Dy, DEh_DT, &
                                  Sl, DSl_Dn, DSl_Dy, DSl_DT, &
                                  Sh, DSh_Dn, DSh_Dy, DSh_DT, &
                                  Pl, DPl_Dn, DPl_Dy, DPl_DT, &
                                  Ph, DPh_Dn, DPh_Dy, DPh_DT, &
                                  munl, Dmunl_Dn, Dmunl_Dy, Dmunl_DT,&
                                  munh, Dmunh_Dn, Dmunh_Dy, Dmunh_DT,&
                                  mupl, Dmupl_Dn, Dmupl_Dy, Dmupl_DT,&
                                  muph, Dmuph_Dn, Dmuph_Dy, Dmuph_DT )

    USE Kind_Types_Mod, ONLY : I4B, DP, LGCL
    USE Physical_Constants_Mod, ONLY : one
    USE Free_Energy_Mod

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: n, y, T
    REAL(DP), INTENT(IN) :: nl, nh

    REAL(DP), INTENT(OUT) :: Ul_APR, Uh_APR
    REAL(DP), INTENT(OUT) :: Meffnl, Meffpl, Meffnh, Meffph

    REAL(DP), INTENT(OUT) :: Fl, DFl_Dn, DFl_Dy, DFl_DT
    REAL(DP), INTENT(OUT) :: Fh, DFh_Dn, DFh_Dy, DFh_DT

    REAL(DP), INTENT(OUT) :: El, DEl_Dn, DEl_Dy, DEl_DT
    REAL(DP), INTENT(OUT) :: Eh, DEh_Dn, DEh_Dy, DEh_DT

    REAL(DP), INTENT(OUT) :: Sl, DSl_Dn, DSl_Dy, DSl_DT
    REAL(DP), INTENT(OUT) :: Sh, DSh_Dn, DSh_Dy, DSh_DT

    REAL(DP), INTENT(OUT) :: Pl, DPl_Dn, DPl_Dy, DPl_DT
    REAL(DP), INTENT(OUT) :: Ph, DPh_Dn, DPh_Dy, DPh_DT

    REAL(DP), INTENT(OUT) :: munl, Dmunl_Dn, Dmunl_Dy, Dmunl_DT
    REAL(DP), INTENT(OUT) :: munh, Dmunh_Dn, Dmunh_Dy, Dmunh_DT

    REAL(DP), INTENT(OUT) :: mupl, Dmupl_Dn, Dmupl_Dy, Dmupl_DT
    REAL(DP), INTENT(OUT) :: muph, Dmuph_Dn, Dmuph_Dy, Dmuph_DT

    INTEGER(I4B) :: objective, flag, error

    REAL(DP) :: vol, log10n_no, log10n_po, log10u

    REAL(DP) ::  Fl_o, Fl_i, Fl_alpha, Fl_TR, Fl_SC
    REAL(DP) ::  nl_no, nl_po, nl_ni, nl_pi, nl_alpha, nl_heavy
    REAL(DP) ::  Al, Zl, radl !, Fl, Pl, Sl, El
    !REAL(DP) ::  DFl_Dn, DFl_Dy, DFl_DT, DPl_Dn, DPl_Dy, DPl_DT
    !REAL(DP) ::  DSl_Dn, DSl_Dy, DSl_DT, DEl_Dn, DEl_Dy, DEl_DT
    !REAL(DP) ::  mul_no, mul_po, Meffl_no, Meffl_po
    !REAL(DP) ::  Dmul_no_DT, Dmul_no_Dn, Dmul_no_Dy
    !REAL(DP) ::  Dmul_po_DT, Dmul_po_Dn, Dmul_po_Dy
    REAL(DP) ::  dLog10nl_no_dn, dLog10nl_po_dn, dLog10ul_dn
    REAL(DP) ::  dLog10nl_no_dT, dLog10nl_po_dT, dLog10ul_dT

    REAL(DP) ::  Fh_o, Fh_i, Fh_alpha, Fh_TR, Fh_SC
    REAL(DP) ::  nh_no, nh_po, nh_ni, nh_pi, nh_alpha, nh_heavy
    REAL(DP) ::  Ah, Zh, radh !, Fh, Ph, Sh, Eh
    !REAL(DP) ::  DFh_Dn, DFh_Dy, DFh_DT, DPh_Dn, DPh_Dy, DPh_DT
    !REAL(DP) ::  DSh_Dn, DSh_Dy, DSh_DT, DEh_Dn, DEh_Dy, DEh_DT
    !REAL(DP) ::  muh_no, muh_po, Meffh_no, Meffh_po
    !REAL(DP) ::  Dmuh_no_DT, Dmuh_no_Dn, Dmuh_no_Dy
    !REAL(DP) ::  Dmuh_po_DT, Dmuh_po_Dn, Dmuh_po_Dy
    REAL(DP) ::  dLog10nh_no_dn, dLog10nh_po_dn, dLog10uh_dn
    REAL(DP) ::  dLog10nh_no_dT, dLog10nh_po_dT, dLog10uh_dT

    REAL(DP) :: non_uniform_eq(3), non_uniform_jac(3,3)

    log10u = -290.d0
    objective = 3

!   get low density properties
    log10n_no = log10(nl*(one-y))
    log10n_po = log10(nl*y)

    flag = - 1

    CALL FREE_ENERGY  ( log10n_no, log10n_po, log10u, nl, T, y, flag, &
                        Fl_o, Fl_i, Fl_alpha, Fl_TR, Fl_SC, Ul_APR, &
                        nl_no, nl_po, nl_ni, nl_pi, nl_alpha, nl_heavy, &
                        Al, Zl, radl, Fl, Pl, Sl, El, &
                        DFl_Dn, DFl_Dy, DFl_DT, DPl_Dn, DPl_Dy, DPl_DT, &
                        DSl_Dn, DSl_Dy, DSl_DT, DEl_Dn, DEl_Dy, DEl_DT, &
                        munl, mupl, Meffnl, Meffpl, &
                        Dmunl_DT, Dmunl_Dn, Dmunl_Dy, &
                        Dmupl_DT, Dmupl_Dn, Dmupl_Dy, &
                        dLog10nl_no_dn, dLog10nl_po_dn, dLog10ul_dn, &
                        dLog10nl_no_dT, dLog10nl_po_dT, dLog10ul_dT, &
                        non_uniform_eq, non_uniform_jac, objective, error )

! write (*,*)
! write (*,"(5ES15.5)")  log10n_no, log10n_po, log10u
! write (*,"(5ES15.5)")  nl, T, y
! write (*,"(5ES15.5)")  Fl_o, Fl_i, Fl_alpha, Fl_TR, Fl_SC
! write (*,"(6ES15.5)")  nl_no, nl_po, nl_ni, nl_pi, nl_alpha, nl_heavy
! write (*,"(3ES15.5)")  Al, Zl, radl
! write (*,"(4ES15.5)")  Fl, Pl, Sl, El
! write (*,"(6ES15.5)")  DFl_Dn, DFl_Dy, DFl_DT, DPl_Dn, DPl_Dy, DPl_DT
! write (*,"(6ES15.5)")  DSl_Dn, DSl_Dy, DSl_DT, DEl_Dn, DEl_Dy, DEl_DT
! write (*,"(5ES15.5)")  munl, mupl, Meffnl, Meffpl
! write (*,"(5ES15.5)")  Dmunl_DT, Dmunl_Dn, Dmunl_Dy
! write (*,"(5ES15.5)")  Dmupl_DT, Dmupl_Dn, Dmupl_Dy
! write (*,"(5ES15.5)")  dLog10nl_no_dn, dLog10nl_po_dn, dLog10ul_dn
! write (*,"(5ES15.5)")  dLog10nl_no_dT, dLog10nl_po_dT, dLog10ul_dT
! write (*,*)

!   get high density properties
    log10n_no = log10(nh*(one-y))
    log10n_po = log10(nh*y)

    flag = + 1

    CALL FREE_ENERGY  ( log10n_no, log10n_po, log10u, nh, T, y, flag, &
                        Fh_o, Fh_i, Fh_alpha, Fh_TR, Fh_SC, Uh_APR, &
                        nh_no, nh_po, nh_ni, nh_pi, nh_alpha, nh_heavy, &
                        Ah, Zh, radh, Fh, Ph, Sh, Eh, &
                        DFh_Dn, DFh_Dy, DFh_DT, DPh_Dn, DPh_Dy, DPh_DT, &
                        DSh_Dn, DSh_Dy, DSh_DT, DEh_Dn, DEh_Dy, DEh_DT, &
                        munh, muph, Meffnh, Meffph, &
                        Dmunh_DT, Dmunh_Dn, Dmunh_Dy, &
                        Dmuph_DT, Dmuph_Dn, Dmuph_Dy, &
                        dLog10nh_no_dn, dLog10nh_po_dn, dLog10uh_dn, &
                        dLog10nh_no_dT, dLog10nh_po_dT, dLog10uh_dT, &
                        non_uniform_eq, non_uniform_jac, objective, error )
! write (*,*)
! write (*,"(5ES15.5)")  log10n_no, log10n_po, log10u
! write (*,"(5ES15.5)")  nh, T, y
! write (*,"(5ES15.5)")  Fh_o, Fh_i, Fh_alpha, Fh_TR, Fh_SC
! write (*,"(6ES15.5)")  nh_no, nh_po, nh_ni, nh_pi, nh_alpha, nh_heavy
! write (*,"(3ES15.5)")  Ah, Zh, radh
! write (*,"(4ES15.5)")  Fh, Ph, Sh, Eh
! write (*,"(6ES15.5)")  DFh_Dn, DFh_Dy, DFh_DT, DPh_Dn, DPh_Dy, DPh_DT
! write (*,"(6ES15.5)")  DSh_Dn, DSh_Dy, DSh_DT, DEh_Dn, DEh_Dy, DEh_DT
! write (*,"(5ES15.5)")  munh, muph, Meffnh, Meffph
! write (*,"(5ES15.5)")  Dmunh_DT, Dmunh_Dn, Dmunh_Dy
! write (*,"(5ES15.5)")  Dmuph_DT, Dmuph_Dn, Dmuph_Dy
! write (*,"(5ES15.5)")  dLog10nh_no_dn, dLog10nh_po_dn, dLog10uh_dn
! write (*,"(5ES15.5)")  dLog10nh_no_dT, dLog10nh_po_dT, dLog10uh_dT
! write (*,*)

  END SUBROUTINE MAXWELL_FREE_ENERGY

END Module Maxwell_Free_Energy_Mod
