!    This file is pacrt of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!

MODULE Read_Skyrme_Coefficients_Mod

  IMPLICIT NONE

  CONTAINS

  SUBROUTINE READ_SKYRME_COEFFICIENTS

    USE Physical_Constants_Mod, &
      ONLY : ZERO, ONE, TWO, HALF, THREE, PI, R_3_2, &
      Mass_n, Mass_p, Neut_Prot_Mass_Diff, B_alpha, v_alpha, M_alpha, &
      Hbarc_Square, N_Q, Neutron_Mass_MeV, Proton_Mass_MeV

    USE Kind_Types_Mod, ONLY : DP
    USE Skyrme_Coefficients_Mod
    USE Make_Tables_Mod
    USE Global_Variables_Mod, & 
      ONLY : include_muons, input_skyrme_filename, ldp_only
    USE Determine_Nuclear_Properties_Mod, ONLY : SATURATION_DENSITY
    USE Print_Parameters_Mod

    IMPLICIT NONE

    REAL(DP) :: Dummy_a, Dummy_b, n_sat, e_bind, stiffening_ratio, &
    stiffening_exponent, stiffening_symmetry
    CHARACTER(LEN=128) :: command

    NAMELIST /TABLE_INPUT/ make_hdf5_table, write_solutions_to_file, &
                           redefine_print_parameters, &
                           filename_base, output_directory, &
                           include_muons, ldp_only

    NAMELIST /TABLE_OUTPUT/ print_p, print_s, print_e, &
                            print_muh, print_mun, print_mup, &
                            print_dpdn, print_dsdn, print_dmuhdn, &
                            print_dpdt, print_dsdt, print_dmuhdt, &
                            print_dpdy, print_dsdy, print_dmuhdy, &
                            print_abar, print_zbar, print_r, print_u, &
                            print_xn, print_xp, print_xa, print_xh, &
                            print_meff_n, print_meff_p, print_uapr

    NAMELIST /SKYRME_TYPE/ Skyrme_parametrization_type, non_local_terms, &
                           Use_default_constants, High_density_stiffening

    NAMELIST /SKYRME_COEFFICIENTS/ Coeff_P

    NAMELIST /SKYRME_STIFFENING/   stiffening_ratio, stiffening_exponent,  &
                                   stiffening_symmetry


!   Set default values for some parameters
    make_hdf5_table  = .TRUE.
    write_solutions_to_file = .FALSE.
    redefine_print_parameters = .FALSE.
    filename_base = 'EOS'
    output_directory = 'EOS'
    include_muons = .FALSE.

    Use_default_constants = 0
    High_density_stiffening = .FALSE.
    non_local_terms = 1

    OPEN(10,FILE=trim(adjustl(input_skyrme_filename)), &
         FORM='FORMATTED',STATUS='OLD',ACTION='READ')
    READ(10,NML=TABLE_INPUT)
!   create output directory (may be system/compiler dependent)
    command = ADJUSTL('mkdir ') // ADJUSTL(TRIM(output_directory))
    CALL SYSTEM(command)
    IF (write_solutions_to_file) THEN
!     create output directories for solutions
      command = ADJUSTL('mkdir ') // ADJUSTL(TRIM(output_directory))// ADJUSTL(TRIM('/SOL'))
      CALL SYSTEM(command)
      command = ADJUSTL('mkdir ') // ADJUSTL(TRIM(output_directory))// ADJUSTL(TRIM('/NO_SOL'))
      CALL SYSTEM(command)
    ENDIF
    READ(10,NML=SKYRME_TYPE)

    SELECT CASE (Use_default_constants)
!     Use Default values for neutron and proton masses and alpha particle
!     binding energy
      CASE(0)
        Mass_n  = neutron_mass_MeV !939.56540D0
        Mass_p  = proton_mass_MeV  !938.27204D0
        Neut_Prot_Mass_Diff = Mass_n - Mass_p
        b_alpha = 28.29552D0 + TWO*Neut_Prot_Mass_Diff
        v_alpha = 24.0D0
      CASE(1)
        Mass_n  = 939.d0 !939.5654D0
        Mass_p  = 939.d0 !939.5654D0
        Neut_Prot_Mass_Diff = 0.d0 !1.29D0
        b_alpha = 28.3D0
        v_alpha = 24.0D0
    END SELECT

    N_Q   = (Mass_n/(TWO*PI*Hbarc_Square))**R_3_2
    M_alpha = 2.D0*(Mass_n + Mass_p) - B_alpha

!   set all Skyrme coefficients to zero
    Coeff_P = zero

!   read Skyrme coefficients
    READ(10,NML=SKYRME_COEFFICIENTS)

!   rename coefficients
    cp1  = coeff_p( 1) ; cp2  = coeff_p( 2) ; cp3  = coeff_p( 3)
    cp4  = coeff_p( 4) ; cp5  = coeff_p( 5) ; cp6  = coeff_p( 6)
    cp7  = coeff_p( 7) ; cp8  = coeff_p( 8) ; cp9  = coeff_p( 9)
    cp10 = coeff_p(10) ; cp11 = coeff_p(11) ; cp12 = coeff_p(12)
    cp13 = coeff_p(13) ; cp14 = coeff_p(14) ; cp15 = coeff_p(15)
    cp16 = coeff_p(16) ; cp17 = coeff_p(17) ; cp18 = coeff_p(18)
    cp19 = coeff_p(19) ; cp20 = coeff_p(20) ; cp21 = coeff_p(21)

!   if Standard Skyrme given then convert to Simplified Skyrme
    SELECT CASE (Skyrme_parametrization_type)
!     Otherwise, convert from standard Skyrme coefficients to simplified ones
      CASE (0)
        CONTINUE
      CASE(1)
        CONTINUE
      CASE DEFAULT
        STOP 'Skyrme_parametrization_type must be 0 or 1!'
    END SELECT

    CALL PRINT_SKYRME_COEFFICIENTS

!   set all print variables to true
    print_p = .TRUE.
    print_s = .TRUE.
    print_e = .TRUE.

    print_muh = .TRUE.
    print_mun = .TRUE.
    print_mup = .TRUE.

    print_dpdn = .TRUE.
    print_dsdn = .TRUE.
    print_dmuhdn = .TRUE.

    print_dpdt = .TRUE.
    print_dsdt = .TRUE.
    print_dmuhdt = .TRUE.

    print_dpdy = .TRUE.
    print_dsdy = .TRUE.
    print_dmuhdy = .TRUE.

    print_abar = .TRUE.
    print_zbar = .TRUE.

    print_r = .TRUE.
    print_u = .TRUE.

    print_uapr = .TRUE.

    print_xn = .TRUE.
    print_xp = .TRUE.
    print_xa = .TRUE.
    print_xh = .TRUE.

    print_meff_n = .TRUE.
    print_meff_p = .TRUE.
    print_uapr   = .TRUE.

!   If redefining print parameters only print variables set to true
!   in TABLE_OUTPUT namelist
    SELECT CASE (redefine_print_parameters)
      CASE (.FALSE.)
        CONTINUE
      CASE(.TRUE.)
        READ(10,NML=TABLE_OUTPUT)
    END SELECT

    CLOSE(10)

  END SUBROUTINE READ_SKYRME_COEFFICIENTS


  SUBROUTINE PRINT_SKYRME_COEFFICIENTS

    USE Kind_Types_Mod, ONLY : I4B
    USE Make_Tables_Mod, ONLY : output_directory
    USE Skyrme_Coefficients_Mod

    IMPLICIT NONE

    INTEGER(I4B) :: i
    CHARACTER(*), PARAMETER :: OUTPUT_FORMAT   = "(1A10,1ES20.12,1A10)"
    CHARACTER(*), PARAMETER :: OUTPUT_FORMAT_1 = "(1A2,1I1,1A7,1ES20.12,1A18)"
    CHARACTER(*), PARAMETER :: OUTPUT_FORMAT_2 = "(1A6,1I1,1A3,1ES20.12,1A20)"
    CHARACTER(*), PARAMETER :: OUTPUT_FORMAT_3 = "(1A3,1I1,1A6,1ES20.12,1A20)"
    CHARACTER(LEN=128) :: File_Name


    File_Name = ADJUSTL(TRIM(output_directory)) // &
                ADJUSTL(TRIM('/Skyrme_coefficients.dat'))

    OPEN(10,file=File_Name)

!   Write APR coefficients
    IF (Skyrme_parametrization_type == 1) then
      WRITE(10,*)
      WRITE(10,*) "Used simplified Skyrme coefficients"
      WRITE(10,*)
    ELSE
      WRITE(10,*)
      WRITE(10,*) "Used standard Skyrme coefficients"
      WRITE(10,*)

      WRITE(10,OUTPUT_FORMAT) 'p_1     = ', Coeff_P(1),  'MeV fm^3'
      WRITE(10,OUTPUT_FORMAT) 'p_2     = ', Coeff_P(2),  'MeV fm^6'
      WRITE(10,OUTPUT_FORMAT) 'p_3     = ', Coeff_P(3),  'MeV fm^5'
      WRITE(10,OUTPUT_FORMAT) 'p_4     = ', Coeff_P(4),  '    fm^3'
      WRITE(10,OUTPUT_FORMAT) 'p_5     = ', Coeff_P(5),  'MeV fm^5'
      WRITE(10,OUTPUT_FORMAT) 'p_6     = ', Coeff_P(6),  'MeV fm^9'
      WRITE(10,OUTPUT_FORMAT) 'p_7     = ', Coeff_P(7),  'MeV fm^3'
      WRITE(10,OUTPUT_FORMAT) 'p_8     = ', Coeff_P(8),  'MeV fm^6'
      WRITE(10,OUTPUT_FORMAT) 'p_9     = ', Coeff_P(9),  '    fm^3'
      WRITE(10,OUTPUT_FORMAT) 'p_10    = ', Coeff_P(10), 'MeV fm^3'
      WRITE(10,OUTPUT_FORMAT) 'p_11    = ', Coeff_P(11), 'MeV fm^6'
      WRITE(10,OUTPUT_FORMAT) 'p_12    = ', Coeff_P(12), 'MeV     '
      WRITE(10,OUTPUT_FORMAT) 'p_13    = ', Coeff_P(13), 'MeV fm^3'
      WRITE(10,OUTPUT_FORMAT) 'p_14    = ', Coeff_P(14), 'MeV fm^6'
      WRITE(10,OUTPUT_FORMAT) 'p_15    = ', Coeff_P(15), 'MeV fm^6'
      WRITE(10,OUTPUT_FORMAT) 'p_16    = ', Coeff_P(16), '    fm^3'
      WRITE(10,OUTPUT_FORMAT) 'p_17    = ', Coeff_P(17), 'MeV fm^6'
      WRITE(10,OUTPUT_FORMAT) 'p_18    = ', Coeff_P(18), '    fm^3'
      WRITE(10,OUTPUT_FORMAT) 'p_19    = ', Coeff_P(19), '    fm^-3'
      WRITE(10,OUTPUT_FORMAT) 'p_20    = ', Coeff_P(20), '    fm^-3'
      WRITE(10,OUTPUT_FORMAT) 'p_21    = ', Coeff_P(21), 'MeV fm^6'
    ENDIF

  END SUBROUTINE PRINT_SKYRME_COEFFICIENTS


END MODULE Read_Skyrme_Coefficients_Mod
