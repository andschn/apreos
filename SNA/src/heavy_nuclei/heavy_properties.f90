!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Heavy_Properties_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE HEAVY_PROPERTIES(n_i, y_i, u, T, &
    Du_DT, Du_Dn, Du_Dy, &
    Dn_i_DT, Dn_i_Dn, Dn_i_Dy,     &
    Dy_i_DT, Dy_i_Dn, Dy_i_Dy,     &
    F_i, DF_i_Dn_ni, DF_i_Dn_pi, DF_i_DT, &
    E_i, DE_i_Dn_ni, DE_i_Dn_pi, DE_i_DT, &
    S_i, DS_i_Dn_ni, DS_i_Dn_pi, DS_i_DT, &
    F_SC, DF_SC_Du, DF_SC_Dn_i, DF_SC_DT, DF_SC_Dy_i,                        &
    D2F_SC_Du2, D2F_SC_DuDn_i, D2F_SC_DuDT, D2F_SC_DuDy_i, D2F_SC_Dn_i2,     &
    D2F_SC_Dn_iDT, D2F_SC_Dn_iDy_i, D2F_SC_DT2, D2F_SC_DTDy_i, D2F_SC_Dy_i2, &
    F_TR, DF_TR_Du, DF_TR_Dn_i, DF_TR_DT, DF_TR_Dy_i,                        &
    D2F_TR_Du2, D2F_TR_DuDn_i, D2F_TR_DuDT, D2F_TR_DuDy_i, D2F_TR_Dn_i2,     &
    D2F_TR_Dn_iDT, D2F_TR_Dn_iDy_i, D2F_TR_DT2, D2F_TR_DTDy_i, D2F_TR_Dy_i2, &
    F_h, DF_h_DT, DF_h_Dn, DF_h_Dy, &
    S_h, DS_h_DT, DS_h_Dn, DS_h_Dy )

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : ONE, TWO, FOUR, R_3_2, &
        v_alpha, b_alpha, HC2 => Hbarc_Square

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: n_i, y_i, u, T
    REAL(DP), INTENT(IN) :: Du_DT, Du_Dn, Du_Dy
    REAL(DP), INTENT(IN) :: Dn_i_DT, Dn_i_Dn, Dn_i_Dy
    REAL(DP), INTENT(IN) :: Dy_i_DT, Dy_i_Dn, Dy_i_Dy
    REAL(DP), INTENT(IN) :: F_i, DF_i_Dn_ni, DF_i_Dn_pi, DF_i_DT
    REAL(DP), INTENT(IN) :: E_i, DE_i_Dn_ni, DE_i_Dn_pi, DE_i_DT
    REAL(DP), INTENT(IN) :: S_i, DS_i_Dn_ni, DS_i_Dn_pi, DS_i_DT

    REAL(DP), INTENT(IN) :: F_SC, DF_SC_Du, DF_SC_Dn_i, DF_SC_DT, DF_SC_Dy_i
    REAL(DP), INTENT(IN) :: D2F_SC_Du2, D2F_SC_DuDn_i, D2F_SC_DuDT
    REAL(DP), INTENT(IN) :: D2F_SC_DuDy_i, D2F_SC_Dn_i2
    REAL(DP), INTENT(IN) :: D2F_SC_Dn_iDT, D2F_SC_Dn_iDy_i, D2F_SC_DT2
    REAL(DP), INTENT(IN) :: D2F_SC_DTDy_i, D2F_SC_Dy_i2
    REAL(DP), INTENT(IN) :: F_TR, DF_TR_Du, DF_TR_Dn_i, DF_TR_DT, DF_TR_Dy_i
    REAL(DP), INTENT(IN) :: D2F_TR_Du2, D2F_TR_DuDn_i, D2F_TR_DuDT
    REAL(DP), INTENT(IN) :: D2F_TR_DuDy_i, D2F_TR_Dn_i2
    REAL(DP), INTENT(IN) :: D2F_TR_Dn_iDT, D2F_TR_Dn_iDy_i, D2F_TR_DT2
    REAL(DP), INTENT(IN) :: D2F_TR_DTDy_i, D2F_TR_Dy_i2

    REAL(DP), INTENT(OUT) :: F_h, DF_h_DT, DF_h_Dn, DF_h_Dy
    REAL(DP), INTENT(OUT) :: S_h, DS_h_DT, DS_h_Dn, DS_h_Dy

    REAL(DP) :: Fi, Ei, Si
    REAL(DP) :: DF_i_Dy_i, DE_i_Dy_i, DS_i_Dy_i
    REAL(DP) :: DF_i_Dn_i, DE_i_Dn_i, DS_i_Dn_i
    REAL(DP) :: DFi_Dy_i, DEi_Dy_i, DSi_Dy_i
    REAL(DP) :: DFi_Dn_i, DEi_Dn_i, DSi_Dn_i
    REAL(DP) :: DFi_DT,   DEi_DT,   DSi_DT

    REAL(DP) :: Ftr, Etr, Str
    REAL(DP) :: DFtr_Du,   DEtr_Du,   DStr_Du
    REAL(DP) :: DFtr_Dy_i, DEtr_Dy_i, DStr_Dy_i
    REAL(DP) :: DFtr_Dn_i, DEtr_Dn_i, DStr_Dn_i
    REAL(DP) :: DFtr_DT,   DEtr_DT,   DStr_DT

    REAL(DP) :: Fsc, Esc, Ssc
    REAL(DP) :: DFsc_Du,   DEsc_Du,   DSsc_Du
    REAL(DP) :: DFsc_Dy_i, DEsc_Dy_i, DSsc_Dy_i
    REAL(DP) :: DFsc_Dn_i, DEsc_Dn_i, DSsc_Dn_i
    REAL(DP) :: DFsc_DT,   DEsc_DT,   DSsc_DT

    DF_i_Dn_i = (one-y_i)*DF_i_Dn_ni + y_i*DF_i_Dn_pi
    DF_i_Dy_i = n_i*(DF_i_Dn_pi - DF_i_Dn_ni)

    DE_i_Dn_i = (one-y_i)*DE_i_Dn_ni + y_i*DE_i_Dn_pi
    DE_i_Dy_i = n_i*(DE_i_Dn_pi - DE_i_Dn_ni)

    DS_i_Dn_i = (one-y_i)*DS_i_Dn_ni + y_i*DS_i_Dn_pi
    DS_i_Dy_i = n_i*(DS_i_Dn_pi - DS_i_Dn_ni)

!   Inside nucleons total free energy and its partial derivatives
    Fi          = F_i
    DFi_Dy_i    = DF_i_Dy_i
    DFi_Dn_i    = DF_i_Dn_i
    DFi_DT      = DF_i_DT

!   Inside nucleons total internal energy and its partial derivatives
    Ei          = E_i
    DEi_Dy_i    = DE_i_Dy_i
    DEi_Dn_i    = DE_i_Dn_i
    DEi_DT      = DE_i_DT

!   Inside nucleons total entropy and its partial derivatives
!    Si          = S_i
!    DSi_Dy_i    = DS_i_Dy_i
!    DSi_Dn_i    = DS_i_Dn_i
!    DSi_DT      = DS_i_DT

    Si        = (Ei       - Fi)      /T
    DSi_Dy_i  = (DEi_Dy_i - DFi_Dy_i)/T
    DSi_Dn_i  = (DEi_Dn_i - DFi_Dn_i)/T
    DSi_DT    = (DEi_DT   - DFi_DT)  /T - (Ei - Fi)/T**TWO

!   Total Translational free energy and partial derivatives
    Ftr       = F_TR
    DFtr_Du   = DF_TR_Du
    DFtr_Dy_i = DF_TR_Dy_i
    DFtr_Dn_i = DF_TR_Dn_i
    DFtr_DT   = DF_TR_DT

!   Total Translational internal energy and partial derivatives
    Etr       = Ftr*(ONE  - T*DF_TR_DT/F_TR)
    DEtr_Du   = DFtr_DU   - T*D2F_TR_DuDT
    DEtr_Dy_i = DFtr_Dy_i - T*D2F_TR_DTDy_i
    DEtr_Dn_i = DFtr_Dn_i - T*D2F_TR_Dn_iDT
    DEtr_DT   =           - T*D2F_TR_DT2

!   Total Translational entropy and partial derivatives
    Str        = (Etr       - Ftr)      /T
    DStr_Du    = (DEtr_Du   - DFtr_Du)  /T
    DStr_Dy_i  = (DEtr_Dy_i - DFtr_Dy_i)/T
    DStr_Dn_i  = (DEtr_Dn_i - DFtr_Dn_i)/T
    DStr_DT    = (DEtr_DT   - DFtr_DT)  /T - (Etr - Ftr)/T**TWO

!   Total Surface+Coulomb free energy and partial derivatives
    Fsc       = F_SC
    DFsc_Du   = DF_SC_Du
    DFsc_Dy_i = DF_SC_Dy_i
    DFsc_Dn_i = DF_SC_Dn_i
    DFsc_DT   = DF_SC_DT

!   Total Surface+Coulomb internal energy and partial derivatives
    Esc       = Fsc*(ONE  - T*DF_SC_DT/F_SC)
    DEsc_Du   = DFsc_DU   - T*D2F_SC_DuDT
    DEsc_Dy_i = DFsc_Dy_i - T*D2F_SC_DTDy_i
    DEsc_Dn_i = DFsc_Dn_i - T*D2F_SC_Dn_iDT
    DEsc_DT   =           - T*D2F_SC_DT2

!   Total Surface+Coulomb entropy and partial derivatives
    Ssc        = (Esc       - Fsc)      /T
    DSsc_Du    = (DEsc_Du   - DFsc_Du)  /T
    DSsc_Dy_i  = (DEsc_Dy_i - DFsc_Dy_i)/T
    DSsc_Dn_i  = (DEsc_Dn_i - DFsc_Dn_i)/T
    DSsc_DT    = (DEsc_DT   - DFsc_DT)  /T - (Esc - Fsc)/T**TWO

!   total heavy nuclei free energy and entropy
    F_h = Ftr + Fsc + u*Fi
    S_h = Str + Ssc + u*Si

!   total free energy first derivatives w.r.t. (T,n,y)
!    DF_h_DT = - u*Si + Du_DT*Fi - Str - Ssc

    DF_h_DT = F_i*Du_DT + u*(DFi_DT + DFi_Dn_i*Dn_i_DT + DFi_Dy_i*Dy_i_DT)  &
            + DFsc_DT + DFsc_Dn_i*Dn_i_DT + DFsc_Dy_i*Dy_i_DT + DFsc_Du*Du_DT &
            + DFtr_DT + DFtr_Dn_i*Dn_i_DT + DFtr_Dy_i*Dy_i_DT + DFtr_Du*Du_DT

!    S_h = - DF_h_DT

!    write (*,"(5ES20.12)") Du_DT*Fi, -u*Si, -Ssc, -Str
!    write (*,"(5ES20.12)") u*DFi_DT, + u*(DFi_DT + DFi_Dn_i*Dn_i_DT + DFi_Dy_i*Dy_i_DT),  &
!            + DFsc_DT, + DFsc_Dn_i*Dn_i_DT + DFsc_Dy_i*Dy_i_DT + DFsc_Du*Du_DT, &
!            + DFtr_DT, + DFtr_Dn_i*Dn_i_DT + DFtr_Dy_i*Dy_i_DT + DFtr_Du*Du_DT

    DS_h_DT = Si*Du_DT + u*(DSi_DT + DSi_Dn_i*Dn_i_DT + DSi_Dy_i*Dy_i_DT)  &
            + DSsc_DT + DSsc_Dn_i*Dn_i_DT + DSsc_Dy_i*Dy_i_DT + DSsc_Du*Du_DT &
            + DStr_DT + DStr_Dn_i*Dn_i_DT + DStr_Dy_i*Dy_i_DT + DStr_Du*Du_DT

    DF_h_Dn = Fi*Du_Dn + u*(DFi_Dn_i*Dn_i_Dn + DFi_Dy_i*Dy_i_Dn)  &
            + DFsc_Dn_i*Dn_i_Dn + DFsc_Dy_i*Dy_i_Dn + DFsc_Du*Du_Dn &
            + DFtr_Dn_i*Dn_i_Dn + DFtr_Dy_i*Dy_i_Dn + DFtr_Du*Du_Dn

    DS_h_Dn = Si*Du_Dn + u*(DSi_Dn_i*Dn_i_Dn + DSi_Dy_i*Dy_i_Dn)  &
            + DSsc_Dn_i*Dn_i_Dn + DSsc_Dy_i*Dy_i_Dn + DSsc_Du*Du_Dn &
            + DStr_Dn_i*Dn_i_Dn + DStr_Dy_i*Dy_i_Dn + DStr_Du*Du_Dn
!
    DF_h_Dy = Fi*Du_Dy + u*(DFi_Dn_i*Dn_i_Dy + DFi_Dy_i*Dy_i_Dy)  &
            + DFsc_Dn_i*Dn_i_Dy + DFsc_Dy_i*Dy_i_Dy + DFsc_Du*Du_Dy &
            + DFtr_Dn_i*Dn_i_Dy + DFtr_Dy_i*Dy_i_Dy + DFtr_Du*Du_Dy

    DS_h_Dy = Si*Du_Dy + u*(DSi_Dn_i*Dn_i_Dy + DSi_Dy_i*Dy_i_Dy)  &
            + DSsc_Dn_i*Dn_i_Dy + DSsc_Dy_i*Dy_i_Dy + DSsc_Du*Du_Dy &
            + DStr_Dn_i*Dn_i_Dy + DStr_Dy_i*Dy_i_Dy + DStr_Du*Du_Dy

  END SUBROUTINE HEAVY_PROPERTIES

END MODULE Heavy_Properties_Mod
