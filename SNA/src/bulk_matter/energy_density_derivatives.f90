!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Energy_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_ENERGY_DENSITY_DERIVATIVES(n, E, tau_n, tau_p, L_n, L_p, &
    Dtau_n_Dn_n, Dtau_p_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_p, &
    DM_n_Dn_n, DM_p_Dn_n, DM_n_Dn_p, DM_p_Dn_p, DUAPR_Dn_n, DUAPR_Dn_p, &
    DE_Dn_n, DE_Dn_p)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : two

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n, E
    REAL(DP), INTENT(IN)  :: L_n, L_p
    REAL(DP), INTENT(IN)  :: tau_n, tau_p
    REAL(DP), INTENT(IN)  :: Dtau_n_Dn_n, Dtau_p_Dn_n
    REAL(DP), INTENT(IN)  :: Dtau_n_Dn_p, Dtau_p_Dn_p
    REAL(DP), INTENT(IN)  :: DM_n_Dn_n, DM_p_Dn_n
    REAL(DP), INTENT(IN)  :: DM_n_Dn_p, DM_p_Dn_p
    REAL(DP), INTENT(IN)  :: DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP), INTENT(OUT) :: DE_Dn_n, DE_Dn_p
!
!   specific energy (energy per nucleon) density derivatives
    DE_Dn_n = Dtau_n_Dn_n*L_n + Dtau_p_Dn_n*L_p + &
              tau_n*DM_n_Dn_n + tau_p*DM_p_Dn_n + DUAPR_Dn_n
    !De_Dn_n = DE_Dn_n/n - e/n

    DE_Dn_p = Dtau_n_Dn_p*L_n + Dtau_p_Dn_p*L_p + &
              tau_n*DM_n_Dn_p + tau_p*DM_p_Dn_p + DUAPR_Dn_p
    !De_Dn_p = DE_Dn_p/n - e/n


  END SUBROUTINE SKYRME_ENERGY_DENSITY_DERIVATIVES

END MODULE Skyrme_Energy_Density_Derivatives_Mod
