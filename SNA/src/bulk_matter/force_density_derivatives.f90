!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Force_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_FORCE_DENSITY_DERIVATIVES( tau_n, tau_p, &
            Dtau_n_Dn_n,Dtau_n_Dn_p,Dtau_p_Dn_n,Dtau_p_Dn_p, &
            DM_n_Dn_n, DM_n_Dn_p, DM_p_Dn_n, DM_p_Dn_p, &
            D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2, &
            D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2, &
            D2UAPR_Dn_n2, D2UAPR_Dn_nn_p, D2UAPR_Dn_pn_n, D2UAPR_Dn_p2, &
            Dv_n_Dn_n, Dv_n_Dn_p, Dv_p_Dn_n, Dv_p_Dn_p)

    USE Kind_Types_Mod, ONLY : DP, I4B

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: tau_n, tau_p
    REAL(DP), INTENT(IN)  :: Dtau_n_Dn_n, Dtau_n_Dn_p
    REAL(DP), INTENT(IN)  :: Dtau_p_Dn_n, Dtau_p_Dn_p
    REAL(DP), INTENT(IN)  :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP), INTENT(IN)  :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP), INTENT(IN)  :: D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2
    REAL(DP), INTENT(IN)  :: D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2
    REAL(DP), INTENT(IN)  :: D2UAPR_Dn_n2, D2UAPR_Dn_nn_p
    REAL(DP), INTENT(IN)  :: D2UAPR_Dn_pn_n, D2UAPR_Dn_p2
    REAL(DP), INTENT(OUT) :: Dv_n_Dn_n, Dv_n_Dn_p
    REAL(DP), INTENT(OUT) :: Dv_p_Dn_n, Dv_p_Dn_p

    Dv_n_Dn_n = Dtau_n_Dn_n*DM_n_Dn_n + Dtau_p_Dn_n*DM_p_Dn_n + &
                tau_n*D2M_n_Dn_n2 + tau_p*D2M_p_Dn_n2 + D2UAPR_Dn_n2
    Dv_n_Dn_p = Dtau_n_Dn_p*DM_n_Dn_n + Dtau_p_Dn_p*DM_p_Dn_n + &
                tau_n*D2M_n_Dn_nn_p + tau_p*D2M_p_Dn_pn_n + D2UAPR_Dn_nn_p
    Dv_p_Dn_n = Dtau_p_Dn_n*DM_p_Dn_p + Dtau_n_Dn_n*DM_n_Dn_p + &
                tau_p*D2M_p_Dn_pn_n + tau_n*D2M_n_Dn_nn_p + D2UAPR_Dn_pn_n
    Dv_p_Dn_p = Dtau_p_Dn_p*DM_p_Dn_p + Dtau_n_Dn_p*DM_n_Dn_p + &
                tau_p*D2M_p_Dn_p2 + tau_n*D2M_n_Dn_p2 + D2UAPR_Dn_p2

  END SUBROUTINE SKYRME_FORCE_DENSITY_DERIVATIVES

END MODULE Skyrme_Force_Density_Derivatives_Mod
