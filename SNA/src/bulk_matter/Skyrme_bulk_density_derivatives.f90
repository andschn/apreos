!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Bulk_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_BULK_DENSITY_DERIVATIVES(flag, &
    log10_n_n, log10_n_p, T, G_n, G_p, &
    Deta_n_Dn_n, Deta_n_Dn_p, Deta_p_Dn_n, Deta_p_Dn_p, &
    Dtau_n_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_n, Dtau_p_Dn_p, &
    Dmu_n_Dn_n,  Dmu_n_Dn_p,  Dmu_p_Dn_n,  Dmu_p_Dn_p,  &
    Dv_n_Dn_n,   Dv_n_Dn_p,   Dv_p_Dn_n,   Dv_p_Dn_p,   &
    DMeff_n_Dn_n, DMeff_n_Dn_p, DMeff_p_Dn_n, DMeff_p_Dn_p, &
    DP_Dn_n, DP_Dn_p, DS_Dn_n, DS_Dn_p, &
    DE_Dn_n, DE_Dn_p, DF_Dn_n, DF_Dn_p  )

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Skyrme_Bulk_Mod
    USE Skyrme_Coefficients_Mod
    USE Skyrme_Aux_Fermi_Integrals_Mod
    USE Skyrme_Effective_Mass_Density_Derivatives_Mod
    USE Skyrme_Potential_Density_Derivatives_Mod
    USE Skyrme_Eta_Density_Derivatives_Mod
    USE Skyrme_Tau_Density_Derivatives_Mod
    USE Skyrme_Mu_Density_Derivatives_Mod
    USE Skyrme_Force_Density_Derivatives_Mod
    USE Skyrme_Pressure_Density_Derivatives_Mod
    USE Skyrme_Entropy_Density_Derivatives_Mod
    USE Skyrme_Energy_Density_Derivatives_Mod
    USE Skyrme_Free_Energy_Density_Derivatives_Mod

    IMPLICIT NONE

    INTEGER(I4B), INTENT(IN) :: flag
    REAL(DP), INTENT(IN)  :: T, log10_n_n, log10_n_p
    REAL(DP), INTENT(OUT) :: G_n, G_p
    REAL(DP), INTENT(OUT) :: Deta_n_Dn_n, Deta_n_Dn_p
    REAL(DP), INTENT(OUT) :: Deta_p_Dn_n, Deta_p_Dn_p
    REAL(DP), INTENT(OUT) :: Dtau_n_Dn_n, Dtau_n_Dn_p
    REAL(DP), INTENT(OUT) :: Dtau_p_Dn_n, Dtau_p_Dn_p
    REAL(DP), INTENT(OUT) :: Dmu_n_Dn_n, Dmu_n_Dn_p
    REAL(DP), INTENT(OUT) :: Dmu_p_Dn_n, Dmu_p_Dn_p
    REAL(DP), INTENT(OUT) :: Dv_n_Dn_n, Dv_n_Dn_p
    REAL(DP), INTENT(OUT) :: Dv_p_Dn_n, Dv_p_Dn_p
    REAL(DP), INTENT(OUT) :: DMeff_n_Dn_n, DMeff_n_Dn_p
    REAL(DP), INTENT(OUT) :: DMeff_p_Dn_n, DMeff_p_Dn_p
    REAL(DP), INTENT(OUT) :: DP_Dn_n, DP_Dn_p
    REAL(DP), INTENT(OUT) :: DS_Dn_n, DS_Dn_p
    REAL(DP), INTENT(OUT) :: DE_Dn_n, DE_Dn_p
    REAL(DP), INTENT(OUT) :: DF_Dn_n, DF_Dn_p

    REAL(DP) :: n_n,n_p,n,y,delta
    REAL(DP) :: Meff_n, Meff_p
    REAL(DP) :: M_n, M_p, L_n, L_p
    REAL(DP) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP) :: DMeff_n_Dn, DMeff_p_Dn
    REAL(DP) :: DMeff_n_Dy, DMeff_p_Dy
    REAL(DP) :: eta_n, eta_p
    REAL(DP) :: tau_n, tau_p
    REAL(DP) :: v_n, v_p
    REAL(DP) :: mu_n, mu_p
    REAL(DP) :: g1, g2, f1, f2, h1, h2
    REAL(DP) :: UAPR, DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP) :: F, P, S, E
    REAL(DP) :: D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2
    REAL(DP) :: D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2
    REAL(DP) :: D2UAPR_Dn_n2, D2UAPR_Dn_nn_p
    REAL(DP) :: D2UAPR_Dn_pn_n, D2UAPR_Dn_p2
    REAL(DP) :: Fm12_n, Fm12_p, F12_n, F12_p, F32_n, F32_p
    REAL(DP) :: Q_nn, Q_np, Q_pn, Q_pp
    LOGICAL :: high_density

!   call subroutine to obtain bulk properties
    CALL SKYRME_BULK_PROPERTIES(log10_n_n,log10_n_p,T,flag,&
          n_n,n_p,n,y,delta,Meff_n,Meff_p,M_n,M_p,L_n,L_p,&
          DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
          eta_n,eta_p,tau_n,tau_p,v_n,v_p,mu_n,mu_p,&
          g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,P,F,S,E,high_density)

    CALL SKYRME_AUX_FERMI_INTEGRALS(eta_n,eta_p,&
          Fm12_n,Fm12_p,F12_n,F12_p,F32_n,F32_p,G_n,G_p)

    CALL SKYRME_EFFECTIVE_MASS_DENSITY_DERIVATIVES(n, y, &
          n_n, n_p, Meff_n, Meff_p, M_n, M_p, &
          DM_n_Dn_n, DM_n_Dn_p, DM_p_Dn_n, DM_p_Dn_p, &
          DMeff_n_Dn_n, DMeff_n_Dn_p, DMeff_p_Dn_n, DMeff_p_Dn_p, &
          DMeff_n_Dn, DMeff_p_Dn, DMeff_n_Dy, DMeff_p_Dy, &
          D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2, &
          D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2)

    CALL SKYRME_TAU_DENSITY_DERIVATIVES(n_n,n_p,tau_n,tau_p, &
          Meff_n,Meff_p,F12_n,F12_p,F32_n,F32_p,G_n,G_p, &
          DMeff_n_Dn_n,DMeff_n_Dn_p,DMeff_p_Dn_n,DMeff_p_Dn_p, &
          Q_nn,Q_np,Q_pn,Q_pp,Dtau_n_Dn_n,Dtau_n_Dn_p,Dtau_p_Dn_n,Dtau_p_Dn_p)

    CALL SKYRME_POTENTIAL_DENSITY_DERIVATIVES(high_density, &
          n,delta,n_n,n_p,g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,&
          h1,h2,D2UAPR_Dn_n2,D2UAPR_Dn_nn_p,D2UAPR_Dn_pn_n,D2UAPR_Dn_p2)

    CALL SKYRME_FORCE_DENSITY_DERIVATIVES( tau_n, tau_p, &
          Dtau_n_Dn_n,Dtau_n_Dn_p,Dtau_p_Dn_n,Dtau_p_Dn_p, &
          DM_n_Dn_n, DM_n_Dn_p, DM_p_Dn_n, DM_p_Dn_p, &
          D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2, &
          D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2, &
          D2UAPR_Dn_n2, D2UAPR_Dn_nn_p, D2UAPR_Dn_pn_n, D2UAPR_Dn_p2, &
          Dv_n_Dn_n, Dv_n_Dn_p, Dv_p_Dn_n, Dv_p_Dn_p)

    CALL SKYRME_ETA_DENSITY_DERIVATIVES(Fm12_n,Fm12_p,Q_nn,Q_np,Q_pn,Q_pp,&
          Deta_n_Dn_n,Deta_n_Dn_p,Deta_p_Dn_n,Deta_p_Dn_p)

    CALL SKYRME_MU_DENSITY_DERIVATIVES(T,Deta_n_Dn_n, Deta_n_Dn_p,&
          Deta_p_Dn_n,Deta_p_Dn_p,Dv_n_Dn_n,Dv_n_Dn_p,Dv_p_Dn_n,Dv_p_Dn_p, &
          Dmu_n_Dn_n, Dmu_n_Dn_p,Dmu_p_Dn_n, Dmu_p_Dn_p)

    CALL SKYRME_PRESSURE_DENSITY_DERIVATIVES(n_n,n_p,&
          Dmu_n_Dn_n,Dmu_n_Dn_p,Dmu_p_Dn_n,Dmu_p_Dn_p,&
          DP_Dn_n,DP_Dn_p)

    CALL SKYRME_ENTROPY_DENSITY_DERIVATIVES(n_n,n_p,n,T,s, &
          eta_n,eta_p,tau_n,tau_p,L_n,L_p, &
          DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p, &
          Deta_n_Dn_n, Deta_n_Dn_p, Deta_p_Dn_n, Deta_p_Dn_p, &
          Dtau_n_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_n, Dtau_p_Dn_p, &
          DS_Dn_n,DS_Dn_p)

    CALL SKYRME_ENERGY_DENSITY_DERIVATIVES(n, E, tau_n, tau_p, L_n, L_p, &
          Dtau_n_Dn_n, Dtau_p_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_p, &
          DM_n_Dn_n, DM_p_Dn_n, DM_n_Dn_p, DM_p_Dn_p, DUAPR_Dn_n, DUAPR_Dn_p, &
          DE_Dn_n, DE_Dn_p)

    CALL SKYRME_FREE_ENERGY_DENSITY_DERIVATIVES(T,&
          DE_Dn_n,DS_Dn_n,DE_Dn_p,DS_Dn_p,DF_Dn_n,DF_Dn_p)

!    write (*,*) 'dmudn'
!    write (*,*) DMu_n_Dn_n, DMu_n_Dn_p, DMu_p_Dn_n, DMu_p_Dn_p
!    write (*,*) Dtau_n_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_n, Dtau_p_Dn_p
!    write (*,*) Deta_n_Dn_n, Deta_n_Dn_p, Deta_p_Dn_n, Deta_p_Dn_p
!    write (*,*) Dv_n_Dn_n, Dv_n_Dn_p, Dv_p_Dn_n, Dv_p_Dn_p

  END SUBROUTINE SKYRME_BULK_DENSITY_DERIVATIVES

END MODULE Skyrme_Bulk_Density_Derivatives_Mod
