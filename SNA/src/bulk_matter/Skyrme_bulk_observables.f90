!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Bulk_Observables_Mod

  IMPLICIT NONE

CONTAINS

  FUNCTION SKYRME_BULK_INCOMPRESSIBILITY(log10_n_n,log10_n_p,T) &
    RESULT(INCOMPRESSIBILITY)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : ZERO, ONE, TWO
    USE Skyrme_Bulk_Mod
    USE Skyrme_Bulk_Density_Derivatives_Mod

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: log10_n_n,log10_n_p,T
    REAL(DP) :: INCOMPRESSIBILITY
    REAL(DP) :: Meff_n, Meff_p
    REAL(DP) :: M_n,M_p,L_n,L_p
    REAL(DP) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP) :: DMeff_n_Dn_n, DMeff_n_Dn_p
    REAL(DP) :: DMeff_p_Dn_n, DMeff_p_Dn_p
    REAL(DP) :: eta_n, eta_p
    REAL(DP) :: tau_n, tau_p
    REAL(DP) :: v_n, v_p
    REAL(DP) :: G_n, G_p
    REAL(DP) :: mu_n, mu_p
    REAL(DP) :: g1, g2, f1, f2
    REAL(DP) :: UAPR, DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP) :: n_n, n_p, n, y, delta
    REAL(DP) :: Deta_n_Dn_n, Deta_n_Dn_p
    REAL(DP) :: Deta_p_Dn_n, Deta_p_Dn_p
    REAL(DP) :: Dtau_n_Dn_n, Dtau_n_Dn_p
    REAL(DP) :: Dtau_p_Dn_n, Dtau_p_Dn_p
    REAL(DP) :: Dmu_n_Dn_n, Dmu_n_Dn_p
    REAL(DP) :: Dmu_p_Dn_n, Dmu_p_Dn_p
    REAL(DP) :: Dv_n_Dn_n, Dv_n_Dn_p
    REAL(DP) :: Dv_p_Dn_n, Dv_p_Dn_p
    REAL(DP) :: Dp_Dn_n, Dp_Dn_p
    REAL(DP) :: Ds_Dn_n, Ds_Dn_p
    REAL(DP) :: De_Dn_n, De_Dn_p
    REAL(DP) :: Df_Dn_n, Df_Dn_p
    REAL(DP) :: p, f, s, e
    REAL(DP) :: dP_dn, d2f_dn2
    INTEGER(I4B) :: flag
    LOGICAL :: high_density

    flag = 0

    CALL SKYRME_BULK_PROPERTIES(log10_n_n,log10_n_p,T,flag, &
      n_n,n_p,n,y,delta,Meff_n,Meff_p,M_n,M_p,L_n,L_p,&
      DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
      eta_n,eta_p,tau_n,tau_p,v_n,v_p,mu_n,mu_p,&
      g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,P,F,S,E,high_density)

    CALL SKYRME_BULK_DENSITY_DERIVATIVES(flag, log10_n_n, log10_n_p, T, G_n, G_p, &
      Deta_n_Dn_n, Deta_n_Dn_p, Deta_p_Dn_n, Deta_p_Dn_p, &
      Dtau_n_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_n, Dtau_p_Dn_p, &
      Dmu_n_Dn_n,  Dmu_n_Dn_p,  Dmu_p_Dn_n,  Dmu_p_Dn_p,  &
      Dv_n_Dn_n,   Dv_n_Dn_p,   Dv_p_Dn_n,   Dv_p_Dn_p,   &
      DMeff_n_Dn_n, DMeff_n_Dn_p, DMeff_p_Dn_n, DMeff_p_Dn_p, &
      DP_Dn_n, DP_Dn_p, Ds_Dn_n, Ds_Dn_p, De_Dn_n, De_Dn_p, Df_Dn_n, Df_Dn_p)

    dP_dn = (one-y)*DP_Dn_n + y*DP_Dn_p

    d2f_dn2 = (- two*P/n + dP_dn)/n**two

!    write (*,*) log10_n_n, log10_n_p, T !
!    write (*,*) n_n, n_p, n !
!    write (*,*) y, delta !
!    write (*,*)
!    write (*,*) M_n, M_p !
!    write (*,*) L_n, L_p !
!    write (*,*)
!    write (*,*) DM_n_Dn_n, DM_n_Dn_p !
!    write (*,*) DM_p_Dn_n, DM_p_Dn_p !
!    write (*,*)
!    write (*,*) eta_n, eta_p !
!    write (*,*) tau_n, tau_p !
!    write (*,*) v_n, v_p !
!    write (*,*) mu_n, mu_p !
!    write (*,*)
!    write (*,*) g1, g2, f1, f2 !
!    write (*,*) UAPR, DUAPR_Dn_n, DUAPR_Dn_p !
!    write (*,*)
!    write (*,*) P,F,S,E !
!    write (*,*)
!    write (*,*) G_n, G_p !
!    write (*,*)
!    write (*,*) Deta_n_Dn_n, Deta_n_Dn_p !
!    write (*,*) Deta_p_Dn_n, Deta_p_Dn_p !
!    write (*,*)
!    write (*,*) Dtau_n_Dn_n, Dtau_n_Dn_p !
!    write (*,*) Dtau_p_Dn_n, Dtau_p_Dn_p !
!    write (*,*)
!    write (*,*) Dmu_n_Dn_n,  Dmu_n_Dn_p
!    write (*,*) Dmu_p_Dn_n,  Dmu_p_Dn_p
!    write (*,*)
!    write (*,*) Dv_n_Dn_n,   Dv_n_Dn_p !
!    write (*,*) Dv_p_Dn_n,   Dv_p_Dn_p !
!    write (*,*)
!    write (*,*) DP_Dn_n, DP_Dn_p !
!    write (*,*) Ds_Dn_n, Ds_Dn_p
!    write (*,*) De_Dn_n, De_Dn_p
!    write (*,*) Df_Dn_n, Df_Dn_p
!    stop

    INCOMPRESSIBILITY = 9.0_DP*n*n*(d2f_dn2)

  END FUNCTION SKYRME_BULK_INCOMPRESSIBILITY

  ! calculate this numerically cause I'm too lazy to do the derivatives
  FUNCTION SKYRME_BULK_SKEWNESS(log10_n_n,log10_n_p,T) RESULT(SKEWNESS)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, &
    ONLY : ZERO, ONE, TWO, FOUR, R_5_3, Hbarc_Square, TWO_PI_SQUARE, &
           Mass_n, Mass_p, Neut_Prot_Mass_Diff
    USE Skyrme_Bulk_Mod

    REAL(DP), INTENT(IN) :: log10_n_n,log10_n_p,T
    REAL(DP) :: log10_n_n_up,log10_n_p_up
    REAL(DP) :: log10_n_n_dn,log10_n_p_dn
    REAL(DP) :: Meff_n, Meff_p
    REAL(DP) :: M_n,M_p,L_n,L_p
    REAL(DP) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP) :: eta_n, eta_p
    REAL(DP) :: tau_n, tau_p
    REAL(DP) :: v_n, v_p
    REAL(DP) :: G_n, G_p
    REAL(DP) :: mu_n, mu_p
    REAL(DP) :: g1, g2, f1, f2
    REAL(DP) :: UAPR, DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP) :: n_n, n_p, n, y, delta
    REAL(DP) :: Deta_n_Dn_n, Deta_n_Dn_p
    REAL(DP) :: Deta_p_Dn_n, Deta_p_Dn_p
    REAL(DP) :: Dtau_n_Dn_n, Dtau_n_Dn_p
    REAL(DP) :: Dtau_p_Dn_n, Dtau_p_Dn_p
    REAL(DP) :: Dmu_n_Dn_n, Dmu_n_Dn_p
    REAL(DP) :: Dmu_p_Dn_n, Dmu_p_Dn_p
    REAL(DP) :: Dv_n_Dn_n, Dv_n_Dn_p
    REAL(DP) :: Dv_p_Dn_n, Dv_p_Dn_p
    REAL(DP) :: DP_d_n_n, DP_d_n_p
    REAL(DP) :: P, F, S, E
    REAL(DP) :: d2F_dn2_up, d2F_dn2_dn, d3F_dn3
    REAL(DP) :: SKEWNESS, Ksat
    REAL(DP), PARAMETER :: eps = 1.0D-4
    INTEGER(I4B) :: flag
    LOGICAL :: high_density

    flag = 0

    CALL SKYRME_BULK_PROPERTIES(log10_n_n,log10_n_p,T,flag, &
      n_n,n_p,n,y,delta,Meff_n,Meff_p,M_n,M_p,L_n,L_p,&
      DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
      eta_n,eta_p,tau_n,tau_p,v_n,v_p,mu_n,mu_p,&
      g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,P,F,S,E,high_density)

    log10_n_n_up = log10(n_n*(one+eps))
    log10_n_p_up = log10(n_p*(one+eps))

    d2F_dn2_up = SKYRME_BULK_INCOMPRESSIBILITY(log10_n_n_up,log10_n_p_up,T)
    d2F_dn2_up = d2F_dn2_up/(3.0_DP*n*(one+eps))**two

    log10_n_n_dn = log10(n_n*(one-eps))
    log10_n_p_dn = log10(n_p*(one-eps))

    d2F_dn2_dn = SKYRME_BULK_INCOMPRESSIBILITY(log10_n_n_dn,log10_n_p_dn,T)
    d2F_dn2_dn = d2F_dn2_dn/(3.0_DP*n*(one-eps))**two

    log10_n_n_dn = log10(n_n)
    log10_n_p_dn = log10(n_p)

    Ksat = SKYRME_BULK_INCOMPRESSIBILITY(log10_n_n_dn,log10_n_p_dn,T)

    d3F_dn3 = (d2F_dn2_up - d2F_dn2_dn)/(two*eps*n)
    ! write (*,*) d2F_dn2_up, d2F_dn2_dn, Ksat
    SKEWNESS = 27.D0*n*n*n*d3f_dn3 !- 6.0_DP*Ksat

  END FUNCTION SKYRME_BULK_SKEWNESS

END MODULE Skyrme_Bulk_Observables_Mod
