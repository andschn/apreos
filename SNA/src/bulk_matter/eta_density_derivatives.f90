!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Eta_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_ETA_DENSITY_DERIVATIVES(Fm12_n,Fm12_p,Q_nn,Q_np,Q_pn,Q_pp,&
          Deta_n_Dn_n,Deta_n_Dn_p,Deta_p_Dn_n,Deta_p_Dn_p)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : two

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: Fm12_n, Fm12_p
    REAL(DP), INTENT(IN)  :: Q_nn, Q_np, Q_pn, Q_pp
    REAL(DP), INTENT(OUT) :: Deta_n_Dn_n, Deta_n_Dn_p
    REAL(DP), INTENT(OUT) :: Deta_p_Dn_n, Deta_p_Dn_p

    Deta_n_Dn_n = two/Fm12_n*Q_nn
    Deta_n_Dn_p = two/Fm12_n*Q_np
    Deta_p_Dn_n = two/Fm12_p*Q_pn
    Deta_p_Dn_p = two/Fm12_p*Q_pp

  END SUBROUTINE SKYRME_ETA_DENSITY_DERIVATIVES

END MODULE Skyrme_Eta_Density_Derivatives_Mod
