!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Bulk_Mod

  USE Kind_Types_Mod, ONLY : DP, I4B
  !USE Define_Operators_Mod
  USE Skyrme_Coefficients_Mod
  USE Fermi_Integrals_Mod
  USE Skyrme_Density_Mod
  USE Skyrme_Effective_Mass_Mod
  USE Skyrme_Eta_Mod
  USE Skyrme_Tau_Mod
  USE Skyrme_Mu_Mod
  USE Skyrme_Force_Mod
  USE Skyrme_Potential_Mod
  USE Skyrme_Pressure_Mod
  USE Skyrme_Entropy_Mod
  USE Skyrme_Energy_Mod
  USE Skyrme_Free_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_BULK_PROPERTIES(log10n_n,log10n_p,T,flag, &
    n_n,n_p,n,y,delta,Meff_n,Meff_p,M_n,M_p,L_n,L_p,&
    DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
    eta_n,eta_p,tau_n,tau_p,v_n,v_p,mu_n,mu_p,&
    g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,P,F,S,E,high_density)

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: log10n_n, log10n_p, T
    INTEGER(I4B), INTENT(IN) :: flag

    REAL(DP), INTENT(OUT) :: n_n, n_p, n, y, delta
    REAL(DP), INTENT(OUT) :: Meff_n, Meff_p
    REAL(DP), INTENT(OUT) :: L_n, L_p
    REAL(DP), INTENT(OUT) :: M_n, M_p
    REAL(DP), INTENT(OUT) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP), INTENT(OUT) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP), INTENT(OUT) :: eta_n, eta_p
    REAL(DP), INTENT(OUT) :: tau_n, tau_p
    REAL(DP), INTENT(OUT) :: v_n, v_p
    REAL(DP), INTENT(OUT) :: mu_n, mu_p
    REAL(DP), INTENT(OUT) :: g1, g2, f1, f2
    REAL(DP), INTENT(OUT) :: UAPR
    REAL(DP), INTENT(OUT) :: DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP), INTENT(OUT) :: p, f, e, s
    LOGICAL, INTENT(OUT) :: high_density

!   Compute densities and proton fraction
    CALL SKYRME_DENSITY(log10n_n,log10n_p,n_n,n_p,n,y,delta)

!   Compute effective masses
    CALL SKYRME_EFFECTIVE_MASS(n_n,n_p,n,y,T,M_n,M_p,L_n,L_p,Meff_n,Meff_p,&
         DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,1)

!   Compute degeneracy parameters
    CALL SKYRME_ETA(log10n_n,log10n_p,n_n,n_p,Meff_n,Meff_p,T,eta_n,eta_p)

!   Compute kinetic energy densities
    CALL SKYRME_TAU(eta_n,eta_p,Meff_n, Meff_p,T,tau_n,tau_p)

!   compute interaction potential
    CALL SKYRME_POTENTIAL(n,delta,n_n,n_p,g1,g2,f1,f2,&
                              UAPR,DUAPR_Dn_n,DUAPR_Dn_p,flag,high_density)

!   compute force potential
    CALL SKYRME_FORCE(tau_n,tau_p,DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
      DUAPR_Dn_n,DUAPR_Dn_p,v_n,v_p)

!   compute chemical potentials
    CALL SKYRME_MU(eta_n,eta_p,v_n,v_p,T,mu_n,mu_p)

!   compute specific entropy
    CALL SKYRME_ENTROPY(n_n,n_p,tau_n,tau_p,eta_n,eta_p,L_n,L_p,n,T,S)

!   compute specific internal energy
    CALL SKYRME_ENERGY(n,y,T,tau_n,tau_p,L_n,L_p,UAPR,E)

!   compute specific free energy
    CALL SKYRME_FREE(E,T,S,F)

!   compute pressure
    CALL SKYRME_PRESSURE(n_n,n_p,mu_n,mu_p,n,f,p)

!    write (*,"(8ES20.12,L2)") n_n, n_p, f1, f2, g1, g2, DUAPR_Dn_n, DUAPR_Dn_p, high_density

  END SUBROUTINE SKYRME_BULK_PROPERTIES

  FUNCTION SKYRME_BULK_ENERGY(log10n_n,log10n_p,T) &
    RESULT(ENERGY)

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: log10n_n,log10n_p,T
    INTEGER(I4B) :: flag
    REAL(DP) :: ENERGY, P, F, S, E
    REAL(DP) :: UAPR, DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP) :: Meff_n, Meff_p
    REAL(DP) :: L_n, L_p, M_n, M_p
    REAL(DP) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP) :: eta_n, eta_p
    REAL(DP) :: tau_n, tau_p
    REAL(DP) :: v_n, v_p
    REAL(DP) :: mu_n, mu_p
    REAL(DP) :: g1, g2, f1, f2
    REAL(DP) :: n_n, n_p, n, y, delta
    LOGICAL :: high_density

    flag = 0

    CALL SKYRME_BULK_PROPERTIES(log10n_n,log10n_p,T,flag,&
    n_n,n_p,n,y,delta,Meff_n,Meff_p,M_n,M_p,L_n,L_p,&
    DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
    eta_n,eta_p,tau_n,tau_p,v_n,v_p,mu_n,mu_p,&
    g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,P,F,S,E,high_density)

!    write (*,*) log10n_n,log10n_p,T,&
!    n_n,n_p,n,y,delta,Meff_n,Meff_p,M_n,M_p,L_n,L_p,&
!    DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
!    eta_n,eta_p,tau_n,tau_p,v_n,v_p,mu_n,mu_p,&
!    g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,P,F,S,E

    ENERGY = E

  END FUNCTION SKYRME_BULK_ENERGY

  FUNCTION SKYRME_BULK_PRESSURE(log10n_n,log10n_p,T) &
    RESULT(PRESSURE)

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: log10n_n,log10n_p,T
    INTEGER(I4B) :: flag
    REAL(DP) :: PRESSURE, P, F, S, E
    REAL(DP) :: UAPR, DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP) :: Meff_n, Meff_p
    REAL(DP) :: L_n, L_p, M_n, M_p
    REAL(DP) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP) :: eta_n, eta_p
    REAL(DP) :: tau_n, tau_p
    REAL(DP) :: v_n, v_p
    REAL(DP) :: mu_n, mu_p
    REAL(DP) :: g1, g2, f1, f2
    REAL(DP) :: n_n, n_p, n, y, delta
    LOGICAL :: high_density

    flag = 0

    CALL SKYRME_BULK_PROPERTIES(log10n_n,log10n_p,T,flag,&
    n_n,n_p,n,y,delta,Meff_n,Meff_p,M_n,M_p,L_n,L_p,&
    DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
    eta_n,eta_p,tau_n,tau_p,v_n,v_p,mu_n,mu_p,&
    g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,P,F,S,E,high_density)

    PRESSURE = P

  END FUNCTION SKYRME_BULK_PRESSURE

END MODULE Skyrme_Bulk_Mod
