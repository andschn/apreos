!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Potential_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_POTENTIAL(n,delta,n_n,n_p,g1,g2,f1,f2,&
                              UAPR,DUAPR_Dn_n,DUAPR_Dn_p,flag,high_density)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Skyrme_Coefficients_Mod
    USE Physical_Constants_Mod, ONLY : ONE, TWO, FOUR, n_low, &
                                      Neut_Prot_Mass_Diff

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n, delta, n_n, n_p
    INTEGER(I4B), INTENT(IN) :: flag
    REAL(DP), INTENT(OUT) :: UAPR, DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP), INTENT(OUT) :: g1, g2, f1, f2
    LOGICAL, INTENT(OUT) :: high_density
    REAL(DP) :: EXP_mp9n2, dEXP_mp9n2_dn
    REAL(DP) :: nmp19, nmp20
    REAL(DP) :: EXP_p18nmp19, EXP_p16nmp20
    REAL(DP) :: DEXP_p18nmp19_Dn, DEXP_p16nmp20_Dn
    REAL(DP) :: g1L, g2L, g1H, g2H
    REAL(DP) :: f1L, f2L, f1H, f2H
    REAL(DP) :: Ulow, Uhigh, y, pol_fit

    high_density = .false.

!    write (*,*) n, n_low

    if (n <= n_low) then
      EXP_mp9n2 = EXP(-(cp9*n_low)**TWO)
      dEXP_mp9n2_dn = -(two*n_low*cp9**two)*EXP_mp9n2

!     compute g1L and g2L at n_low
      g1L = -(cp1 + cp2*n_low + cp6*n_low*n_low + (cp10 + cp11*n_low)*EXP_mp9n2)
      g2L = -(cp12/n_low + cp7 + cp8*n_low + cp13*EXP_mp9n2)

!     compute derivatives at n_low
      f1L = - (cp2 + two*cp6*n_low + cp11*EXP_mp9n2 &
               + (cp10 + cp11*n_low)*dEXP_mp9n2_dn)
      f2L = - (- cp12/n_low**two + cp8 + cp13*dEXP_mp9n2_dn)

!     set values of g1L and g2L using derivatives
      g1L = g1L + (n-n_low)*f1L
      g2L = g2L + (n-n_low)*f2L
    endif

    if (n >  n_low) then
!     set output quantities to zero
      EXP_mp9n2 = EXP(-(cp9*n)**TWO)
      dEXP_mp9n2_dn = -(two*n*cp9**two)*EXP_mp9n2

!     compute density functions g1 and g2
      g1L = - (cp1 + cp2*n + cp6*n*n + (cp10 + cp11*n)*EXP_mp9n2)
      g2L = - (cp12/n + cp7 + cp8*n + cp13*EXP_mp9n2)

!     if high density check whether phase
!     with pions is the most stable one
      y = n_p/n

      pol_fit = transition_fit(y) 

      Uhigh = 1.d100

      if ((n > pol_fit .and. flag == 0) .or. flag == +1) then
        nmp19 = n - cp19
        nmp20 = n - cp20
        EXP_p18nmp19 = EXP(cp18*nmp19)
        EXP_p16nmp20 = EXP(cp16*nmp20)
        g1H = g1L - (cp17*nmp19 + cp21*nmp19*nmp19)*EXP_p18nmp19
        g2H = g2L - (cp15*nmp20 + cp14*nmp20*nmp20)*EXP_p16nmp20
        Uhigh = four*g1H*n_n*n_p + g2H*(n_n-n_p)**two
        high_density = .true.
      endif

      Ulow  = four*g1L*n_n*n_p + g2L*(n_n-n_p)**two
      
!      if (Uhigh < Ulow) high_density = .true.

      if (high_density) then
      !write (*,*) 'high', n, delta
        g1 = g1H
        g2 = g2H
      else
        g1 = g1L
        g2 = g2L
      endif

!     compute density functions f1 and f2
      f1L = - (cp2 + two*cp6*n + cp11*EXP_mp9n2 + (cp10 + cp11*n)*dEXP_mp9n2_dn)
      f2L = - (- cp12/n**two + cp8 + cp13*dEXP_mp9n2_dn)

      if (high_density) then
        DEXP_p18nmp19_Dn = cp18*EXP(cp18*nmp19)
        DEXP_p16nmp20_Dn = cp16*EXP(cp16*nmp20)
        f1H = f1L - (cp17 + two*cp21*nmp19)*EXP_p18nmp19 &
                  - (cp17*nmp19 + cp21*nmp19*nmp19)*DEXP_p18nmp19_Dn
        f2H = f2L - (cp15 + two*cp14*nmp20)*EXP_p16nmp20 &
                  - (cp15*nmp20 + cp14*nmp20*nmp20)*DEXP_p16nmp20_Dn
        f1 = f1H
        f2 = f2H
      else
        f1 = f1L
        f2 = f2L
      endif
    endif

!   compute interaction potential
    UAPR =  four*g1*n_n*n_p + g2*(n_n-n_p)**two - n_p*Neut_Prot_Mass_Diff

!   interaction potential derivatives w.r.t. n_n and n_p
    DUAPR_Dn_n = (four*f1*n_n*n_p + f2*(n_n-n_p)**two &
                + four*g1*n_p + two*g2*(n_n-n_p))
    DUAPR_Dn_p = (four*f1*n_p*n_n + f2*(n_p-n_n)**two &
                + four*g1*n_n + two*g2*(n_p-n_n)) - Neut_Prot_Mass_Diff

!   interaction potential derivatives w.r.t. n and x
!    DUAPR_Dn = (one-y)*DUAPR_Dn_n + y*DUAPR_Dn_p
!    DUAPR_Dx = -n*DUAPR_Dn_n + n*DUAPR_Dn_p

  END SUBROUTINE SKYRME_POTENTIAL

  FUNCTION Transition_Fit(y) RESULT(transition)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Global_Variables_Mod, ONLY : LDP_ONLY 

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: y
    REAL(DP) :: transition

    transition = 0.1956d0 + 0.3389d0*y + 0.2918d0*y**2 &
              - 1.2614d0*y**3 + 0.6307d0*y**4

!   if flag set to LDP only then 
!    set transition to very high density
!    so it is not included
    if (ldp_only) transition = 1.d10

  END FUNCTION Transition_Fit

END MODULE Skyrme_Potential_Mod
