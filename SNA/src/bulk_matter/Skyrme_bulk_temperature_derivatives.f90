!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Bulk_Temperature_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_BULK_TEMPERATURE_DERIVATIVES( n_n, n_p, n, T, p, f, s, e, &
    Meff_n, Meff_p, L_n, L_p, DM_n_Dn_n, DM_n_Dn_p, DM_p_Dn_n, DM_p_Dn_p, &
    DMeff_n_Dn_n, DMeff_n_Dn_p, DMeff_p_Dn_n, DMeff_p_Dn_p, &
    D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2, &
    D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2, &
    Dtau_n_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_n, Dtau_p_Dn_p, &
    DMu_n_Dn_n, DMu_n_Dn_p, DMu_p_Dn_n, DMu_p_Dn_p, &
    DUAPR_Dn_n, DUAPR_Dn_p, &
    D2UAPR_Dn_n2, D2UAPR_Dn_nn_p, D2UAPR_Dn_pn_n, D2UAPR_Dn_p2,&
    DP_Dn_n, DP_Dn_p, DS_Dn_n, DS_Dn_p, DE_Dn_n, DE_Dn_p, DF_Dn_n, DF_Dn_p, &
    eta_n, eta_p, tau_n, tau_p, v_n, v_p, mu_n, mu_p, G_n, G_p, &
    Dtau_n_Deta_n, Dtau_n_Deta_p, Dtau_p_Deta_n, Dtau_p_Deta_p, &
    DMu_n_Deta_n, DMu_n_Deta_p, DMu_p_Deta_n, DMu_p_Deta_p, &
    DP_Deta_n, DP_Deta_p, DS_Deta_n, DS_Deta_p, &
    DE_Deta_n, DE_Deta_p, DF_Deta_n, DF_Deta_p, &
    Dn_n_DT, Dn_p_DT, Dtau_n_DT, Dtau_p_DT, Dmu_n_DT, Dmu_p_DT, &
    DP_DT, DS_DT, DE_DT, DF_DT, fixed )
    ! if fixed = 0 then determine derivatives w.r.t. T with fixed eta_p, eta_n
    ! if fixed = 1 then determine derivatives w.r.t. T with fixed n_n, n_p

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Skyrme_Coefficients_Mod
    USE Skyrme_Bulk_Mod
    USE Fermi_Integrals_Mod
    USE Physical_Constants_Mod, &
        ONLY : ZERO, ONE, TWO, THREE, FOUR, R_3_2, R_5_2, R_5_3, R_9_2, &
        Mass_n, Mass_p, hc2 => Hbarc_Square

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: n_n, n_p, n, T
    REAL(DP), INTENT(IN) :: p, f, s, e
    REAL(DP), INTENT(IN) :: Meff_n, Meff_p
    REAL(DP), INTENT(IN) :: L_n, L_p
    REAL(DP), INTENT(IN) :: G_n, G_p
    REAL(DP), INTENT(IN) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP), INTENT(IN) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP), INTENT(IN) :: DMeff_n_Dn_n, DMeff_n_Dn_p
    REAL(DP), INTENT(IN) :: DMeff_p_Dn_n, DMeff_p_Dn_p
    REAL(DP), INTENT(IN) :: D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2
    REAL(DP), INTENT(IN) :: D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2
    REAL(DP), INTENT(IN) :: Dtau_n_Dn_n, Dtau_n_Dn_p
    REAL(DP), INTENT(IN) :: Dtau_p_Dn_n, Dtau_p_Dn_p
    REAL(DP), INTENT(IN) :: DMu_n_Dn_n, DMu_n_Dn_p
    REAL(DP), INTENT(IN) :: DMu_p_Dn_n, DMu_p_Dn_p
    REAL(DP), INTENT(IN) :: DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP), INTENT(IN) :: D2UAPR_Dn_n2, D2UAPR_Dn_nn_p
    REAL(DP), INTENT(IN) :: D2UAPR_Dn_pn_n, D2UAPR_Dn_p2
    REAL(DP), INTENT(IN) :: DP_Dn_n, DP_Dn_p
    REAL(DP), INTENT(IN) :: DS_Dn_n, DS_Dn_p
    REAL(DP), INTENT(IN) :: DE_Dn_n, DE_Dn_p
    REAL(DP), INTENT(IN) :: DF_Dn_n, DF_Dn_p
    REAL(DP), INTENT(IN) :: Dtau_n_Deta_n, Dtau_n_Deta_p
    REAL(DP), INTENT(IN) :: Dtau_p_Deta_n, Dtau_p_Deta_p
    REAL(DP), INTENT(IN) :: DMu_n_Deta_n, DMu_n_Deta_p
    REAL(DP), INTENT(IN) :: DMu_p_Deta_n, DMu_p_Deta_p
    REAL(DP), INTENT(IN) :: DP_Deta_n, DP_Deta_p
    REAL(DP), INTENT(IN) :: DS_Deta_n, DS_Deta_p
    REAL(DP), INTENT(IN) :: DE_Deta_n, DE_Deta_p
    REAL(DP), INTENT(IN) :: DF_Deta_n, DF_Deta_p
    REAL(DP), INTENT(IN) :: eta_n, eta_p
    REAL(DP), INTENT(IN) :: tau_n, tau_p
    REAL(DP), INTENT(IN) :: v_n, v_p
    REAL(DP), INTENT(IN) :: mu_n, mu_p

    INTEGER(I4B), INTENT(IN) :: fixed

    REAL(DP), INTENT(OUT) :: Dn_n_DT, Dn_p_DT
    REAL(DP), INTENT(OUT) :: Dtau_n_DT, Dtau_p_DT
    REAL(DP), INTENT(OUT) :: Dmu_n_DT, Dmu_p_DT
    REAL(DP), INTENT(OUT) :: DP_DT, DS_DT, DE_DT, DF_DT

    REAL(DP) :: S0, Dn_DT, DUAPR_DT
    REAL(DP) :: Dv_n_DT, Dv_p_DT, DMeff_n_DT, DMeff_p_DT
    REAL(DP) :: Deta_n_DT, Deta_p_DT
    REAL(DP) :: DM_n_DT, DM_p_DT
    REAL(DP) :: R_nn, R_np, R_pn, R_pp
    REAL(DP) :: aux_n, aux_p, aux_nn, aux_np, aux_pn, aux_pp

!   compute derivatives w.r.t. T with fixed eta.
!   To keep eta fixed derivatives of qttes for which
!   eta apperas explicitely need to be recalculated
    IF (fixed == 0) THEN
!     compute some auxiliary values
      R_nn = R_3_2*n_n/Meff_n*DMeff_n_Dn_n
      R_np = R_3_2*n_n/Meff_n*DMeff_n_Dn_p
      R_pn = R_3_2*n_p/Meff_p*DMeff_p_Dn_n
      R_pp = R_3_2*n_p/Meff_p*DMeff_p_Dn_p

      aux_n = R_3_2*n_n/T
      aux_p = R_3_2*n_p/T

      aux_nn = one - R_nn
      aux_np = one - R_np
      aux_pn = one - R_pn
      aux_pp = one - R_pp

!     derivatives of n_t w.r.t. T (fixed eta)
      Dn_n_DT = (aux_n*aux_pp + aux_p*R_np)/(aux_nn*aux_pp - R_pn*R_np)
      Dn_p_DT = (aux_p*aux_nn + aux_n*R_pn)/(aux_pp*aux_nn - R_np*R_pn)
      Dn_DT = Dn_n_DT + Dn_p_DT

!     derivative of M^star w.r.t. T (fixed eta)
      DMeff_n_DT = DMeff_n_Dn_n*Dn_n_DT + DMeff_n_Dn_p*Dn_p_DT
      DMeff_p_DT = DMeff_p_Dn_n*Dn_n_DT + DMeff_p_Dn_p*Dn_p_DT

!     eta does not appear explicitely in M_t, so this is fine
      DM_n_DT = DM_n_Dn_n*Dn_n_DT + DM_n_Dn_p*Dn_p_DT
      DM_p_DT = DM_p_Dn_n*Dn_n_DT + DM_p_Dn_p*Dn_p_DT

!     derivative of tau_t w.r.t. T (fixed eta)
!     eta appears explicitely in tau_t so CANNOT do
!     Dtau_t_DT = Dtau_t_Dn_n*Dn_n_DT + Dtau_t_Dn_p*Dn_p_DT
      Dtau_n_DT = R_5_2*tau_n*(one/T + DMeff_n_DT/Meff_n)
      Dtau_p_DT = R_5_2*tau_p*(one/T + DMeff_p_DT/Meff_p)

!     derivative of v_t w.r.t. T (fixed eta)
!     eta appears explicitely in v_t so CANNOT do
!     Dv_t_DT = Dv_t_Dn_n*Dn_n_DT + Dv_t_Dn_p*Dn_p_DT
      Dv_n_DT = Dtau_n_DT*DM_n_Dn_n + Dtau_p_DT*DM_p_Dn_n &
              + tau_n*(D2M_n_Dn_n2*Dn_n_DT + D2M_n_Dn_nn_p*Dn_p_DT) &
              + tau_p*(D2M_p_Dn_n2*Dn_n_DT + D2M_p_Dn_pn_n*Dn_p_DT) &
              + D2UAPR_Dn_n2*Dn_n_DT + D2UAPR_Dn_nn_p*Dn_p_DT

      Dv_p_DT = Dtau_n_DT*DM_n_Dn_p + Dtau_p_DT*DM_p_Dn_p &
              + tau_n*(D2M_n_Dn_nn_p*Dn_n_DT + D2M_n_Dn_p2*Dn_p_DT) &
              + tau_p*(D2M_p_Dn_pn_n*Dn_n_DT + D2M_p_Dn_p2*Dn_p_DT) &
              + D2UAPR_Dn_nn_p*Dn_n_DT + D2UAPR_Dn_p2*Dn_p_DT

!     derivative of mu_t w.r.t. T (fixed eta)
      Dmu_n_DT = eta_n + Dv_n_DT
      Dmu_p_DT = eta_p + Dv_p_DT

!     derivative of UAPR w.r.t. T (fixed eta)
!     eta does not appear explicitely in UAPR, so this is fine
      DUAPR_DT = DUAPR_Dn_n*Dn_n_DT + DUAPR_Dn_p*Dn_p_DT

!     derivative of e w.r.t. T (fixed eta)
!     E = (tau_n*L_n + tau_p*L_p + UAPR)
!     e = E/n
      DE_DT = Dtau_n_DT*L_n + Dtau_p_DT*L_p &
            + tau_n*DM_n_DT + tau_p*DM_p_DT + DUAPR_DT

!     derivative of s w.r.t. T (fixed eta)
!     S = (R_5_3*(tau_n*L_n + tau_p*L_p) - T*(n_n*eta_n + n_p*eta_p))/T
      DS_DT = R_5_3*(Dtau_n_DT*L_n + Dtau_p_DT*L_p) &
            + R_5_3*(tau_n*DM_n_DT + tau_p*DM_p_DT) &
            - T*(Dn_n_DT*eta_n + Dn_p_DT*eta_p) &
            - (n_n*eta_n + n_p*eta_p)

      DS_DT = DS_DT/T - S/T

!     derivative of f w.r.t. T (fixed eta)
      DF_DT = DE_DT - T*DS_DT - S

!     derivative of p w.r.t. T (fixed eta)
!     P = n_n*mu_n + n_p*mu_p - F
      DP_DT = Dn_n_DT*mu_n + Dn_p_DT*mu_p &
            + n_n*Dmu_n_DT + n_p*Dmu_p_DT &
            - DF_DT

    ENDIF

    IF (fixed == 1) THEN
!     derivative of n_t w.r.t. T (fixed n_i, y_i)
      Dn_n_DT = zero
      Dn_p_DT = zero

!     derivative of eta_t w.r.t. T (fixed n_i, x_i)
      Deta_n_DT = - R_3_2/T*G_n
      Deta_p_DT = - R_3_2/T*G_p

!     derivative of tau_t w.r.t. T (fixed n_i, x_i)
      !Dtau_n_DT = Dtau_n_Deta_n*Deta_n_DT + Dtau_n_Deta_p*Deta_p_DT
      !Dtau_p_DT = Dtau_p_Deta_n*Deta_n_DT + Dtau_p_Deta_p*Deta_p_DT
      Dtau_n_DT = R_5_2*tau_n/T-R_9_2*Meff_n/hc2*n_n*G_n
      Dtau_p_DT = R_5_2*tau_p/T-R_9_2*Meff_p/hc2*n_p*G_p

!     derivative of V_t w.r.t. T (fixed n_i, x_i)
      Dv_n_DT = Dtau_n_DT*DM_n_Dn_n + Dtau_p_DT*DM_p_Dn_n
      Dv_p_DT = Dtau_n_DT*DM_n_Dn_p + Dtau_p_DT*DM_p_Dn_p

!     derivative of mu_t w.r.t. T (fixed n_i, x_i)
      Dmu_n_DT = eta_n + T*Deta_n_DT + Dv_n_DT
      Dmu_p_DT = eta_p + T*Deta_p_DT + Dv_p_DT

!     derivative of e w.r.t. T (fixed n_i, x_i)
!     E = (tau_n*L_n + tau_p*L_p + UAPR)
      DE_DT = Dtau_n_DT*L_n + Dtau_p_DT*L_p

!     derivative of s w.r.t. T (fixed n_i, x_i)
!     S = R_5_3*(tau_n*L_n + tau_p*L_p) - T*(n_n*eta_n + n_p*eta_p)
!     s = S/T
      DS_DT = R_5_3*(Dtau_n_DT*L_n + Dtau_p_DT*L_p) &
            - T*(n_n*Deta_n_DT + n_p*Deta_p_DT) &
            - (n_n*eta_n + n_p*eta_p)
      DS_DT = DS_DT/T - S/T**two

!     derivative of f w.r.t. T (fixed n_i, x_i)
      DF_DT = - S

!     derivative of p w.r.t. T (fixed n_i, x_i)
!     P = n_n*mu_n + n_p*mu_p - F
      DP_DT = n_n*Dmu_n_DT + n_p*Dmu_p_DT - DF_DT
    ENDIF

!    write (*,*) fixed
!    write (*,*) n_n, n_p, T
!    write (*,*) Meff_n, Meff_p
!    write (*,*) DMeff_n_Dn_n, DMeff_n_Dn_p, DMeff_p_Dn_n, DMeff_p_Dn_p
!    write (*,*) Dtau_n_DT, Dtau_p_DT
!    write (*,*) Deta_n_DT, Deta_p_DT
!    write (*,*) Dv_n_DT, Dv_p_DT
!    write (*,*) Dmu_n_DT, Dmu_p_DT
!    write (*,*) Dn_n_DT, Dn_p_DT, DP_DT
!    write (*,*)

  END SUBROUTINE SKYRME_BULK_TEMPERATURE_DERIVATIVES

END MODULE Skyrme_Bulk_Temperature_Derivatives_Mod
