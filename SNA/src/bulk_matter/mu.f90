!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Mu_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_MU(eta_n, eta_p, v_n, v_p, T, mu_n, mu_p)
!  Given log10(n_n) and log10(n_p) compute n_n, n_p, n, y

    USE Kind_Types_Mod, ONLY : DP, I4B

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: eta_n, eta_p, v_n, v_p, T
    REAL(DP), INTENT(OUT) :: mu_n, mu_p

!   neutron and proton chemical potentials
    mu_n = eta_n*T + v_n
    mu_p = eta_p*T + v_p

  END SUBROUTINE SKYRME_MU

END MODULE Skyrme_Mu_Mod
