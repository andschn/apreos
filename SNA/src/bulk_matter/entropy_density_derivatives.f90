!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Entropy_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_ENTROPY_DENSITY_DERIVATIVES(n_n,n_p,n,T,S, &
    eta_n,eta_p,tau_n,tau_p,L_n,L_p, &
    DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p, &
    Deta_n_Dn_n, Deta_n_Dn_p, Deta_p_Dn_n, Deta_p_Dn_p, &
    Dtau_n_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_n, Dtau_p_Dn_p, &
    DS_Dn_n,DS_Dn_p)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : two, R_5_3

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n_n, n_p, n, T, s
    REAL(DP), INTENT(IN)  :: eta_n, eta_p
    REAL(DP), INTENT(IN)  :: tau_n, tau_p
    REAL(DP), INTENT(IN)  :: L_n, L_p
    REAL(DP), INTENT(IN)  :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP), INTENT(IN)  :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP), INTENT(IN)  :: Deta_n_Dn_n, Deta_n_Dn_p
    REAL(DP), INTENT(IN)  :: Deta_p_Dn_n, Deta_p_Dn_p
    REAL(DP), INTENT(IN)  :: Dtau_n_Dn_n, Dtau_n_Dn_p
    REAL(DP), INTENT(IN)  :: Dtau_p_Dn_n, Dtau_p_Dn_p
    REAL(DP), INTENT(OUT) :: DS_Dn_n, DS_Dn_p

!   entropy derivatives
    DS_Dn_n = R_5_3*(Dtau_n_Dn_n*L_n  + Dtau_p_Dn_n*L_p + &
                tau_n*DM_n_Dn_n +  tau_p*DM_p_Dn_n) - &
              T*(eta_n + n_n*Deta_n_Dn_n + n_p*Deta_p_Dn_n)
    DS_Dn_n = DS_Dn_n/T
    !Ds_Dn_n = Ds_Dn_n/n/T - s/T/n**two

    DS_Dn_p = R_5_3*(Dtau_n_Dn_p*L_n  + Dtau_p_Dn_p*L_p + &
                tau_n*DM_n_Dn_p +  tau_p*DM_p_Dn_p) - &
              T*(eta_p + n_n*Deta_n_Dn_p + n_p*Deta_p_Dn_p)
    DS_Dn_p = DS_Dn_p/T
    !Ds_Dn_p = Ds_Dn_p/n/T - s/T/n**two

  END SUBROUTINE SKYRME_ENTROPY_DENSITY_DERIVATIVES

END MODULE Skyrme_Entropy_Density_Derivatives_Mod
