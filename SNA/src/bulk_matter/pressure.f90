!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Pressure_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_PRESSURE(n_n,n_p,mu_n,mu_p,n,F,P)

    USE Kind_Types_Mod, ONLY : DP, I4B

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n_n, n_p
    REAL(DP), INTENT(IN)  :: mu_n, mu_p
    REAL(DP), INTENT(IN)  :: n, f
    REAL(DP), INTENT(OUT) :: p
!
!   pressure of bulk nucleons
    P = n_n*mu_n + n_p*mu_p - F

  END SUBROUTINE SKYRME_PRESSURE

END MODULE Skyrme_Pressure_Mod
