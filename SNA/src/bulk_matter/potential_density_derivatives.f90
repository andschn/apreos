!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Potential_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_POTENTIAL_DENSITY_DERIVATIVES(high_density,&
          n,delta,n_n,n_p,g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,&
          h1,h2,D2UAPR_Dn_n2,D2UAPR_Dn_nn_p,D2UAPR_Dn_pn_n,D2UAPR_Dn_p2)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Skyrme_Coefficients_Mod
    USE Physical_Constants_Mod, ONLY : zero, one, two, three, four, n_low

    IMPLICIT NONE

    LOGICAL, INTENT(IN) :: high_density
    REAL(DP), INTENT(IN)  :: n, delta, n_n, n_p
    REAL(DP), INTENT(IN)  :: UAPR, DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP), INTENT(IN)  :: g1, g2, f1, f2
    REAL(DP), INTENT(OUT) :: h1, h2
    REAL(DP), INTENT(OUT) :: D2UAPR_Dn_n2, D2UAPR_Dn_nn_p
    REAL(DP), INTENT(OUT) :: D2UAPR_Dn_pn_n, D2UAPR_Dn_p2
    REAL(DP) :: EXP_mp9n2, dEXP_mp9n2_dn, d2EXP_mp9n2_dn2
    REAL(DP) :: nmp19, nmp20
    REAL(DP) :: EXP_p18nmp19, EXP_p16nmp20
    REAL(DP) :: DEXP_p18nmp19_Dn, DEXP_p16nmp20_Dn
    REAL(DP) :: D2EXP_p18nmp19_Dn2, D2EXP_p16nmp20_Dn2
    REAL(DP) :: h1L, h2L, h1H, h2H

    EXP_mp9n2 = EXP(-(cp9*n)**two)
    dEXP_mp9n2_dn = -(two*n*cp9**two)*EXP_mp9n2
    d2EXP_mp9n2_dn2 = -(two*n*cp9**two)*dEXP_mp9n2_dn + dEXP_mp9n2_dn/n

    if (n < n_low) then
      h1 = zero
      h2 = zero
    else
!     compute density functions h1 and h2
      h1L = - (two*cp6 + two*cp11*dEXP_mp9n2_dn + (cp10 + cp11*n)*d2EXP_mp9n2_dn2)
      h2L = - (two*cp12/n**three + cp13*d2EXP_mp9n2_dn2)

      h1 = h1L
      h2 = h2L

      if (high_density) then
        nmp19 = n - cp19
        nmp20 = n - cp20
        EXP_p18nmp19 = EXP(cp18*nmp19)
        EXP_p16nmp20 = EXP(cp16*nmp20)
        DEXP_p18nmp19_Dn = cp18*EXP(cp18*nmp19)
        DEXP_p16nmp20_Dn = cp16*EXP(cp16*nmp20)
        D2EXP_p18nmp19_Dn2 = cp18*cp18*EXP(cp18*nmp19)
        D2EXP_p16nmp20_Dn2 = cp16*cp16*EXP(cp16*nmp20)
        h1H = h1L - two*cp21*EXP_p18nmp19 &
                  - (cp17 + two*cp21*nmp19)*DEXP_p18nmp19_Dn &
                  - (cp17 + two*cp21*nmp19)*DEXP_p18nmp19_Dn &
                  - (cp17*nmp19 + cp21*nmp19*nmp19)*D2EXP_p18nmp19_Dn2
        h2H = h2L - two*cp14*EXP_p16nmp20 &
                  - (cp15 + two*cp14*nmp20)*DEXP_p16nmp20_Dn &
                  - (cp15 + two*cp14*nmp20)*DEXP_p16nmp20_Dn &
                  - (cp15*nmp20 + cp14*nmp20*nmp20)*D2EXP_p16nmp20_Dn2
        h1 = h1H
        h2 = h2H
      endif
    endif

!   Compute APR potential U derivatives w.r.t. n_t
    D2UAPR_Dn_n2   = (four*h1*n_n*n_p + h2*(n_n-n_p)**two &
                    + four*f1*n_p + two*f2*(n_n-n_p) &
                    + four*f1*n_p + two*f2*(n_n-n_p) + two*g2)

    D2UAPR_Dn_nn_p = (four*h1*n_n*n_p + h2*(n_n-n_p)**two &
                    + four*f1*n_p + two*f2*(n_n-n_p) &
                    + four*f1*n_n - two*f2*(n_n-n_p) + four*g1 - two*g2)

    D2UAPR_Dn_pn_n = (four*h1*n_p*n_n + h2*(n_p-n_n)**two &
                    + four*f1*n_n + two*f2*(n_p-n_n) &
                    + four*f1*n_p - two*f2*(n_p-n_n) + four*g1 - two*g2)

    D2UAPR_Dn_p2   = (four*h1*n_p*n_n + h2*(n_p-n_n)**two &
                    + four*f1*n_n + two*f2*(n_p-n_n) &
                    + four*f1*n_n + two*f2*(n_p-n_n) + two*g2)

!    write (*,*) '--------'
!    write (*,*) g1, g2
!    write (*,*) f1, f2
!    write (*,*) h1, h2
!    write (*,*) D2UAPR_Dn_n2, D2UAPR_Dn_nn_p
!    write (*,*) D2UAPR_Dn_pn_n, D2UAPR_Dn_p2
!    write (*,*) '--------'

  END SUBROUTINE SKYRME_POTENTIAL_DENSITY_DERIVATIVES

END MODULE Skyrme_Potential_Density_Derivatives_Mod
