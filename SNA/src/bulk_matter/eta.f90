!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Eta_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_ETA(log10_n_n,log10_n_p,n_n,n_p,Meff_n,Meff_p,T,eta_n,eta_p)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Fermi_Integrals_Mod, ONLY : inverse_fermi_one_half
    USE Physical_Constants_Mod, ONLY : ZERO, ONE, TWO, TEN, R_3_2, pi, &
      Hbarc_Square, TWO_PI_SQUARE, log10_dens_min

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: log10_n_n, log10_n_p, n_n, n_p, Meff_n, Meff_p, T
    REAL(DP), INTENT(OUT) :: eta_n, eta_p
    REAL(DP) :: two_mneut_t, two_mprot_t

!   compute degeneracy parameters
!   set a couple of auxiliary values
    two_mneut_t = two*Meff_n*T
    two_mprot_t = two*Meff_p*T

    eta_n = TWO_PI_SQUARE*n_n*(hbarc_square/two_mneut_t)**R_3_2
    eta_n = inverse_fermi_one_half(eta_n)

    eta_p = TWO_PI_SQUARE*n_p*(hbarc_square/two_mprot_t)**R_3_2
    eta_p = inverse_fermi_one_half(eta_p)

!   if dens_t < dens_min then
!    correct value for degeneracy parameters
!    using relevant asymptotic formulae

!     F_-1/2(x) = sqrt(pi)*exp(x)
!     F_+1/2(x) = (1/2)*sqrt(pi)*exp(x)
!     F_+3/2(x) = (3/4)*sqrt(pi)*exp(x)
!     F_+5/2(x) = (15/8)*sqrt(pi)*exp(x)

    if (log10_n_n<log10_dens_min) then
      eta_n = ((two_mneut_t/hbarc_square)**R_3_2)/TWO_PI_SQUARE
      eta_n = log10_n_n*log(ten)-log(sqrt(pi)*eta_n/two)
    endif

    if (log10_n_p<log10_dens_min) then
      eta_p = ((two_mprot_t/hbarc_square)**R_3_2)/TWO_PI_SQUARE
      eta_p = log10_n_p*log(ten)-log(sqrt(pi)*eta_p/two)
    endif

  END SUBROUTINE SKYRME_ETA

END MODULE Skyrme_Eta_Mod
