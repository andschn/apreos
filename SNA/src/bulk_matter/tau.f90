!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Tau_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_TAU(eta_n,eta_p,Meff_n,Meff_p,T,tau_n,tau_p)
!  Given eta_n and eta_p compute tau_n, tau_p

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Fermi_Integrals_Mod, ONLY : fermi_three_halves
    USE Physical_Constants_Mod, ONLY : one, two, R_5_2, Hbarc_Square, &
                                       TWO_PI_SQUARE

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: eta_n, eta_p, T
    REAL(DP), INTENT(IN)  :: Meff_n, Meff_p
    REAL(DP), INTENT(OUT) :: tau_n, tau_p
    REAL(DP) :: two_mneut_t, two_mprot_t

!   set a couple of auxiliary values
    two_mneut_t = two*Meff_n*T
    two_mprot_t = two*Meff_p*T

!   compute kinetic energies
!    cap tau_t to a min value of -200.d0
!    to avoid underflow issues
    tau_n = (two_mneut_t/hbarc_square)**R_5_2
    tau_n = tau_n*fermi_three_halves(max(eta_n,-2.d2))/TWO_PI_SQUARE
    tau_p = (two_mprot_t/hbarc_square)**R_5_2
    tau_p = tau_p*fermi_three_halves(max(eta_p,-2.d2))/TWO_PI_SQUARE

  END SUBROUTINE SKYRME_TAU

END MODULE Skyrme_Tau_Mod
