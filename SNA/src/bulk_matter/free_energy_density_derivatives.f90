!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Free_Energy_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_FREE_ENERGY_DENSITY_DERIVATIVES(T,&
    DE_Dn_n,DS_Dn_n,DE_Dn_p,DS_Dn_p,DF_Dn_n,DF_Dn_p)

    USE Kind_Types_Mod, ONLY : DP, I4B

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: T
    REAL(DP), INTENT(IN)  :: DE_Dn_n, DE_Dn_p
    REAL(DP), INTENT(IN)  :: DS_Dn_n, DS_Dn_p
    REAL(DP), INTENT(OUT) :: DF_Dn_n, DF_Dn_p

!   pressure derivatives
    DF_Dn_n = DE_Dn_n - T*DS_Dn_n
    DF_Dn_p = DE_Dn_p - T*DS_Dn_p

  END SUBROUTINE SKYRME_FREE_ENERGY_DENSITY_DERIVATIVES

END MODULE Skyrme_Free_Energy_Density_Derivatives_Mod
