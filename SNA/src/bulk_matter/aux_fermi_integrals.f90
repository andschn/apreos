!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Aux_Fermi_Integrals_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_AUX_FERMI_INTEGRALS(eta_n,eta_p,&
              Fm12_n,Fm12_p,F12_n,F12_p,F32_n,F32_p,G_n,G_p)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : half, one, two, three, four, pi
    USE Fermi_Integrals_Mod, ONLY : fermi_minus_one_half, fermi_one_half, &
                                  fermi_three_halves


    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: eta_n, eta_p
    REAL(DP), INTENT(OUT) :: Fm12_n, Fm12_p
    REAL(DP), INTENT(OUT) :: F12_n, F12_p
    REAL(DP), INTENT(OUT) :: F32_n, F32_p
    REAL(DP), INTENT(OUT) :: G_n,G_p

!   compute F_-1/2(eta_t), F_1/2(eta_t), F_3/2(eta_t)
!   if dens_t < dens_min then
!    correct value for degeneracy parameters
!    using relevant asymptotic formulae

!     F_-1/2(x) = sqrt(pi)*exp(x)
!     F_+1/2(x) = (1/2)*sqrt(pi)*exp(x)
!     F_+3/2(x) = (3/4)*sqrt(pi)*exp(x)
!     F_+5/2(x) = (15/8)*sqrt(pi)*exp(x)
!     G_t = 2*F_{+1/2}(eta_t)/F_{-1/2}(eta_t)
    if (eta_n>-200.d0) then
      Fm12_n= fermi_minus_one_half(eta_n)
      F12_n = fermi_one_half(eta_n)
      F32_n = fermi_three_halves(eta_n)
      G_n   = two*(F12_n/Fm12_n)
    else ! cap for very small eta_n to avoid under/overflows later
      Fm12_n= sqrt(pi)*exp(-200.d0)
      F12_n = half*sqrt(pi)*exp(-200.d0)
      F32_n = three/four*sqrt(pi)*exp(-200.d0)
      G_n   = one
    endif

    if (eta_p>-200.d0) then
      Fm12_p= fermi_minus_one_half(eta_p)
      F12_p = fermi_one_half(eta_p)
      F32_p = fermi_three_halves(eta_p)
      G_p   = two*(F12_p/Fm12_p)
    else ! cap for very small eta_n to avoid under/overflows later
      Fm12_p= sqrt(pi)*exp(-200.d0)
      F12_p = half*sqrt(pi)*exp(-200.d0)
      F32_p = three/four*sqrt(pi)*exp(-200.d0)
      G_p   = one
    endif

  END SUBROUTINE SKYRME_AUX_FERMI_INTEGRALS

END MODULE Skyrme_Aux_Fermi_Integrals_Mod
