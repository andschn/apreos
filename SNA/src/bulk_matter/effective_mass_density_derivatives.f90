!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Effective_Mass_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_EFFECTIVE_MASS_DENSITY_DERIVATIVES(n, y, &
        n_n, n_p, Meff_n, Meff_p, M_n, M_p, &
        DM_n_Dn_n, DM_n_Dn_p, DM_p_Dn_n, DM_p_Dn_p, &
        DMeff_n_Dn_n, DMeff_n_Dn_p, DMeff_p_Dn_n, DMeff_p_Dn_p, &
        DMeff_n_Dn, DMeff_p_Dn, DMeff_n_Dy, DMeff_p_Dy, &
        D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2, &
        D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Skyrme_Coefficients_Mod

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Skyrme_Coefficients_Mod
    USE Physical_Constants_Mod, ONLY : one, two, Hbarc_Square

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n, y, n_n, n_p
    REAL(DP), INTENT(IN)  :: Meff_n, Meff_p, M_n, M_p
    REAL(DP), INTENT(IN)  :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP), INTENT(IN)  :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP), INTENT(OUT) :: DMeff_n_Dn_n, DMeff_n_Dn_p
    REAL(DP), INTENT(OUT) :: DMeff_p_Dn_n, DMeff_p_Dn_p
    REAL(DP), INTENT(OUT) :: DMeff_n_Dn, DMeff_p_Dn
    REAL(DP), INTENT(OUT) :: DMeff_n_Dy, DMeff_p_Dy
    REAL(DP), INTENT(OUT) :: D2M_n_Dn_n2, D2M_n_Dn_nn_p, D2M_n_Dn_p2
    REAL(DP), INTENT(OUT) :: D2M_p_Dn_n2, D2M_p_Dn_pn_n, D2M_p_Dn_p2
    REAL(DP) :: aux_n, aux_p
    REAL(DP) :: aux1, aux2, Daux1_Dn, Daux2_Dn
    REAL(DP) :: p4n, EXP_mp4n

    p4n = cp4*n

!!   effective mass Meff_t derivatives w.r.t. n_t'
    aux_n = - two*Meff_n**two/hbarc_square
    aux_p = - two*Meff_p**two/hbarc_square

    DMeff_n_Dn_n = aux_n*DM_n_Dn_n
    DMeff_n_Dn_p = aux_n*DM_n_Dn_p
    DMeff_p_Dn_n = aux_p*DM_p_Dn_n
    DMeff_p_Dn_p = aux_p*DM_p_Dn_p

!   effective mass Meff_t derivatives w.r.t. n and x
    DMeff_n_Dn = (one-y)*DMeff_n_Dn_n + y*DMeff_n_Dn_p
    DMeff_p_Dn = (one-y)*DMeff_p_Dn_n + y*DMeff_p_Dn_p
    DMeff_n_Dy = -n*DMeff_n_Dn_n + n*DMeff_n_Dn_p
    DMeff_p_Dy = -n*DMeff_p_Dn_n + n*DMeff_p_Dn_p

!   compute exponentials present in APR EOS
    EXP_mp4n  = EXP(-p4n)

!   compute effective masses
!   set values of auxiliary functions M_n and M_p
!    M_n = n*(p3 + (one-y)*p5)*EXP_mp4n
!    M_p = n*(p3 + (    y)*p5)*EXP_mp4n
!
    aux1 = (one-p4n)/n
    aux2 = cp5*EXP_mp4n

!   DM_n_Dn_n = aux1*M_n + aux2*y
!   DM_n_Dn_p = aux1*M_n - aux2*(one-y)
!   DM_p_Dn_n = aux1*M_p - aux2*y
!   DM_p_Dn_p = aux1*M_p + aux2*(one-y)

    Daux1_Dn = -one/n/n
    Daux2_Dn = -cp4*aux2

    D2M_n_Dn_n2   = Daux1_Dn*M_n + aux1*DM_n_Dn_n + (Daux2_Dn - aux2/n)*y
    D2M_n_Dn_nn_p = Daux1_Dn*M_n + aux1*DM_n_Dn_p + Daux2_Dn*y + aux2*(one-y)/n
    D2M_n_Dn_p2   = Daux1_Dn*M_n + aux1*DM_n_Dn_p - (Daux2_Dn - aux2/n)*(one-y)

    D2M_p_Dn_n2   = Daux1_Dn*M_p + aux1*DM_p_Dn_n - (Daux2_Dn - aux2/n)*y
    D2M_p_Dn_pn_n = Daux1_Dn*M_p + aux1*DM_p_Dn_n + Daux2_Dn*(one-y) + aux2*y/n
    D2M_p_Dn_p2   = Daux1_Dn*M_p + aux1*DM_p_Dn_p + (Daux2_Dn - aux2/n)*(one-y)

  END SUBROUTINE SKYRME_EFFECTIVE_MASS_DENSITY_DERIVATIVES

END MODULE Skyrme_Effective_Mass_Density_Derivatives_Mod
