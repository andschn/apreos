!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Bulk_Eta_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_BULK_ETA_DERIVATIVES(n, T, Y, n_n, n_p, s, Meff_n, Meff_p, &
    tau_n, tau_p, mu_n, mu_p, eta_n, eta_p, G_n, G_p, L_n, L_p, &
    Deta_n_Dn_n, Deta_n_Dn_p, Deta_p_Dn_n, Deta_p_Dn_p, &
    Dtau_n_Dn_n, Dtau_n_Dn_p, Dtau_p_Dn_n, Dtau_p_Dn_p, &
    Dmu_n_Dn_n,  Dmu_n_Dn_p,  Dmu_p_Dn_n,  Dmu_p_Dn_p,  &
    DM_n_Dn_n, DM_n_Dn_p, DM_p_Dn_n, DM_p_Dn_p, Dp_Dn_n, Dp_Dn_p, &
    DP_Deta_n, DP_Deta_p, DF_Deta_n, DF_Deta_p, &
    DS_Deta_n, DS_Deta_p, DE_Deta_n, DE_Deta_p, &
    Dn_n_Deta_n,   Dn_n_Deta_p,   Dn_p_Deta_n,   Dn_p_Deta_p,   &
    Dmu_n_Deta_n,  Dmu_n_Deta_p,  Dmu_p_Deta_n,  Dmu_p_Deta_p,  &
    Dtau_n_Deta_n, Dtau_n_Deta_p, Dtau_p_Deta_n, Dtau_p_Deta_p )

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, &
        ONLY : ONE, TWO, R_3_2, R_5_3, Hbarc_Square
    USE Skyrme_Coefficients_Mod
    USE Fermi_Integrals_Mod
    USE Skyrme_Bulk_Mod

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: T, n, y, n_n, n_p, s
    REAL(DP), INTENT(IN) :: Meff_n, Meff_p
    REAL(DP), INTENT(IN) :: tau_n, tau_p
    REAL(DP), INTENT(IN) :: mu_n, mu_p
    REAL(DP), INTENT(IN) :: eta_n, eta_p
    REAL(DP), INTENT(IN) :: G_n, G_p
    REAL(DP), INTENT(IN) :: L_n, L_p
    REAL(DP), INTENT(IN) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP), INTENT(IN) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP), INTENT(IN) :: Deta_n_Dn_n, Deta_n_Dn_p
    REAL(DP), INTENT(IN) :: Deta_p_Dn_n, Deta_p_Dn_p
    REAL(DP), INTENT(IN) :: Dtau_n_Dn_n, Dtau_n_Dn_p
    REAL(DP), INTENT(IN) :: Dtau_p_Dn_n, Dtau_p_Dn_p
    REAL(DP), INTENT(IN) :: Dmu_n_Dn_n, Dmu_n_Dn_p
    REAL(DP), INTENT(IN) :: Dmu_p_Dn_n, Dmu_p_Dn_p
    REAL(DP), INTENT(IN) :: DP_Dn_n, DP_Dn_p

    REAL(DP), INTENT(OUT) :: Dp_Deta_n, Dp_Deta_p
    REAL(DP), INTENT(OUT) :: Df_Deta_n, Df_Deta_p
    REAL(DP), INTENT(OUT) :: Ds_Deta_n, Ds_Deta_p
    REAL(DP), INTENT(OUT) :: De_Deta_n, De_Deta_p
    REAL(DP), INTENT(OUT) :: Dn_n_Deta_n, Dn_n_Deta_p
    REAL(DP), INTENT(OUT) :: Dn_p_Deta_n, Dn_p_Deta_p
    REAL(DP), INTENT(OUT) :: Dmu_n_Deta_n, Dmu_n_Deta_p
    REAL(DP), INTENT(OUT) :: Dmu_p_Deta_n,  Dmu_p_Deta_p
    REAL(DP), INTENT(OUT) :: Dtau_n_Deta_n, Dtau_n_Deta_p
    REAL(DP), INTENT(OUT) :: Dtau_p_Deta_n, Dtau_p_Deta_p

    REAL(DP) :: aux_n, aux_p, A_nn, A_pp, DEN
    REAL(DP) :: DMeff_n_Dn_n, DMeff_n_Dn_p
    REAL(DP) :: DMeff_p_Dn_n, DMeff_p_Dn_p
    REAL(DP) :: R_nn, R_np, R_pn, R_pp
    REAL(DP) :: Dv_n_Deta_n, Dv_n_Deta_p
    REAL(DP) :: Dv_p_Deta_n, Dv_p_Deta_p
    REAL(DP) :: DM_n_Deta_n, DM_n_Deta_p
    REAL(DP) :: DM_p_Deta_n, DM_p_Deta_p
    REAL(DP) :: Dn_Deta_n, Dn_Deta_p

!   auxiliary values
    aux_n = - two*Meff_n**two/hbarc_square
    aux_p = - two*Meff_p**two/hbarc_square

    DMeff_n_Dn_n = aux_n*DM_n_Dn_n
    DMeff_n_Dn_p = aux_n*DM_n_Dn_p
    DMeff_p_Dn_n = aux_p*DM_p_Dn_n
    DMeff_p_Dn_p = aux_p*DM_p_Dn_p

    R_nn = R_3_2*n_n/Meff_n*DMeff_n_Dn_n
    R_np = R_3_2*n_n/Meff_n*DMeff_n_Dn_p
    R_pn = R_3_2*n_p/Meff_p*DMeff_p_Dn_n
    R_pp = R_3_2*n_p/Meff_p*DMeff_p_Dn_p

    A_nn = n_n/G_n
    A_pp = n_p/G_p

    DEN = (one - R_pp - R_nn - R_np*R_pn + R_nn*R_pp)

!   derivative of n_t w.r.t. eta_r (fixed T)
!   wrong!!
!   Dn_n_Deta_n = (A_nn*(one-R_nn)*(one-R_pp) + A_pp*R_np*R_np)*(one-R_pp)/DEN
!   Dn_n_Deta_p = (A_pp*(one-R_nn)*(one-R_pp) + A_nn*R_pn*R_pn)*(   +R_np)/DEN
!   Dn_p_Deta_n = (A_nn*(one-R_nn)*(one-R_pp) + A_pp*R_np*R_np)*(   +R_pn)/DEN
!   Dn_p_Deta_p = (A_pp*(one-R_nn)*(one-R_pp) + A_nn*R_pn*R_pn)*(one-R_nn)/DEN

!   correct form!
    Dn_n_Deta_n = A_nn*(one-R_pp)/DEN
    Dn_n_Deta_p = A_pp*(    R_np)/DEN
    Dn_p_Deta_n = A_nn*(    R_pn)/DEN
    Dn_p_Deta_p = A_pp*(one-R_nn)/DEN

!   derivative of n_t w.r.t. eta_r (fixed T)
!    Dn_n_Deta_n = n_n/G_n/(one-R_nn)
!    Dn_n_Deta_p = n_n/G_n/(   -R_np)
!    Dn_p_Deta_n = n_p/G_p/(   -R_pn)
!    Dn_p_Deta_p = n_p/G_p/(one-R_pp)

!   check that (Dnn_Detan Dnn_Detap) (Detan_Dnn Detan_Dnp) = ( 1 0 )
!              (Dnp_Detan Dnp_Detap) (Detap_Dnn Detap_Dnp)   ( 0 1 )
!    write (*,*)
!    write (*,*) Dn_n_Deta_n*Deta_n_Dn_n + Dn_n_Deta_p*Deta_p_Dn_n
!    write (*,*) Dn_n_Deta_n*Deta_n_Dn_p + Dn_n_Deta_p*Deta_p_Dn_p
!    write (*,*) Dn_p_Deta_n*Deta_n_Dn_n + Dn_p_Deta_p*Deta_p_Dn_n
!    write (*,*) Dn_p_Deta_n*Deta_n_Dn_p + Dn_p_Deta_p*Deta_p_Dn_p
!    write (*,*)


!   derivative of mu_t w.r.t. eta_r (fixed T)
    Dmu_n_Deta_n = Dmu_n_Dn_n*Dn_n_Deta_n + Dmu_n_Dn_p*Dn_p_Deta_n
    Dmu_n_Deta_p = Dmu_n_Dn_n*Dn_n_Deta_p + Dmu_n_Dn_p*Dn_p_Deta_p
    Dmu_p_Deta_n = Dmu_p_Dn_n*Dn_n_Deta_n + Dmu_p_Dn_p*Dn_p_Deta_n
    Dmu_p_Deta_p = Dmu_p_Dn_n*Dn_n_Deta_p + Dmu_p_Dn_p*Dn_p_Deta_p

!   derivative of tau_t w.r.t. eta_r (fixed T)
    Dtau_n_Deta_n = Dtau_n_Dn_n*Dn_n_Deta_n + Dtau_n_Dn_p*Dn_p_Deta_n
    Dtau_n_Deta_p = Dtau_n_Dn_n*Dn_n_Deta_p + Dtau_n_Dn_p*Dn_p_Deta_p
    Dtau_p_Deta_n = Dtau_p_Dn_n*Dn_n_Deta_n + Dtau_p_Dn_p*Dn_p_Deta_n
    Dtau_p_Deta_p = Dtau_p_Dn_n*Dn_n_Deta_p + Dtau_p_Dn_p*Dn_p_Deta_p

!   derivatives of effective mass auxiliary functions M_t w.r.t. eta_r (fixed T)
    DM_n_Deta_n = DM_n_Dn_n*Dn_n_Deta_n + DM_n_Dn_p*Dn_p_Deta_n
    DM_n_Deta_p = DM_n_Dn_n*Dn_n_Deta_p + DM_n_Dn_p*Dn_p_Deta_p
    DM_p_Deta_n = DM_p_Dn_n*Dn_n_Deta_n + DM_p_Dn_p*Dn_p_Deta_n
    DM_p_Deta_p = DM_p_Dn_n*Dn_n_Deta_p + DM_p_Dn_p*Dn_p_Deta_p

!   derivative of n w.r.t. eta_r (fixed T)
    Dn_Deta_n = Dn_n_Deta_n + Dn_p_Deta_n
    Dn_Deta_p = Dn_n_Deta_p + Dn_p_Deta_p

!   derivative of p w.r.t. eta_r (fixed T)
    Dp_Deta_n = DP_Dn_n*Dn_n_Deta_n + DP_Dn_p*Dn_p_Deta_n
    Dp_Deta_p = DP_Dn_n*Dn_n_Deta_p + DP_Dn_p*Dn_p_Deta_p

!   derivative of f w.r.t. eta_r (fixed T)
    DF_Deta_n = n_n*Dmu_n_Deta_n + Dn_n_Deta_n*mu_n &
              + n_p*Dmu_p_Deta_n + Dn_p_Deta_n*mu_p - DP_Deta_n
    DF_Deta_p = n_n*Dmu_n_Deta_p + Dn_n_Deta_p*mu_n &
              + n_p*Dmu_p_Deta_p + Dn_p_Deta_p*mu_p - DP_Deta_p

!   derivative of s w.r.t. eta_r (fixed T)
    DS_Deta_n = R_5_3*(Dtau_n_Deta_n*L_n  + Dtau_p_Deta_n*L_p + &
                        tau_n*DM_n_Deta_n  + tau_p*DM_p_Deta_n) &
              - T*(Dn_n_Deta_n*eta_n + Dn_p_Deta_n*eta_p + n_n)
    DS_Deta_n = DS_Deta_n/T !/n - s*Dn_Deta_n/n**two/T

    DS_Deta_p = R_5_3*(Dtau_n_Deta_p*L_n  + Dtau_p_Deta_p*L_p + &
                        tau_n*DM_n_Deta_p  + tau_p*DM_p_Deta_p) &
              - T*(Dn_n_Deta_p*eta_n + Dn_p_Deta_p*eta_p + n_p)
    DS_Deta_p = DS_Deta_p/T !/n - s*Dn_Deta_p/n**two/T

!   derivative of e w.r.t. eta_r (fixed T)
    DE_Deta_n = DF_Deta_n + T*DS_Deta_n
    DE_Deta_p = DF_Deta_p + T*DS_Deta_p

  END SUBROUTINE SKYRME_BULK_ETA_DERIVATIVES

END MODULE Skyrme_Bulk_Eta_Derivatives_Mod
