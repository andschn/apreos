!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Entropy_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_ENTROPY(n_n,n_p,tau_n,tau_p,eta_n,eta_p,L_n,L_p,n,T,S)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : R_5_3

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n_n, n_p, n, T
    REAL(DP), INTENT(IN)  :: tau_n, tau_p
    REAL(DP), INTENT(IN)  :: eta_n, eta_p
    REAL(DP), INTENT(IN)  :: L_n, L_p
    REAL(DP), INTENT(OUT) :: S
!
!   entropy density (entropy per unit volume)
    S = R_5_3*(tau_n*L_n + tau_p*L_p) - T*(n_n*eta_n + n_p*eta_p)
    S = S/T !/n

  END SUBROUTINE SKYRME_ENTROPY

END MODULE Skyrme_Entropy_Mod
