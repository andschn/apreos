!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Tau_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_TAU_DENSITY_DERIVATIVES(n_n,n_p,tau_n,tau_p, &
        Meff_n,Meff_p,F12_n,F12_p,F32n_n,F32n_p,G_n,G_p, &
        DMeff_n_Dn_n,DMeff_n_Dn_p,DMeff_p_Dn_n,DMeff_p_Dn_p, &
        Q_nn,Q_np,Q_pn,Q_pp,Dtau_n_Dn_n,Dtau_n_Dn_p,Dtau_p_Dn_n,Dtau_p_Dn_p)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : one, R_3_2, R_5_2

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n_n, n_p
    REAL(DP), INTENT(IN)  :: tau_n, tau_p
    REAL(DP), INTENT(IN)  :: Meff_n, Meff_p
    REAL(DP), INTENT(IN)  :: F12_n, F12_p
    REAL(DP), INTENT(IN)  :: F32n_n, F32n_p
    REAL(DP), INTENT(IN)  :: G_n, G_p
    REAL(DP), INTENT(IN)  :: DMeff_n_Dn_n, DMeff_n_Dn_p
    REAL(DP), INTENT(IN)  :: DMeff_p_Dn_n, DMeff_p_Dn_p
    REAL(DP), INTENT(OUT) :: Dtau_n_Dn_n, Dtau_n_Dn_p
    REAL(DP), INTENT(OUT) :: Dtau_p_Dn_n, Dtau_p_Dn_p
    REAL(DP), INTENT(OUT) :: Q_nn,Q_np,Q_pn,Q_pp

!   Q_tt' values
    Q_nn = F12_n*(one - R_3_2*n_n/Meff_n*DMeff_n_Dn_n)/n_n
    Q_np = F12_n*(    - R_3_2*n_n/Meff_n*DMeff_n_Dn_p)/n_n
    Q_pn = F12_p*(    - R_3_2*n_p/Meff_p*DMeff_p_Dn_n)/n_p
    Q_pp = F12_p*(one - R_3_2*n_p/Meff_p*DMeff_p_Dn_p)/n_p

!   tau density derivatives
    Dtau_n_Dn_n = R_5_2*tau_n/Meff_n*DMeff_n_Dn_n + R_3_2*tau_n*G_n*Q_nn/F32n_n
    Dtau_n_Dn_p = R_5_2*tau_n/Meff_n*DMeff_n_Dn_p + R_3_2*tau_n*G_n*Q_np/F32n_n
    Dtau_p_Dn_n = R_5_2*tau_p/Meff_p*DMeff_p_Dn_n + R_3_2*tau_p*G_p*Q_pn/F32n_p
    Dtau_p_Dn_p = R_5_2*tau_p/Meff_p*DMeff_p_Dn_p + R_3_2*tau_p*G_p*Q_pp/F32n_p

  END SUBROUTINE SKYRME_TAU_DENSITY_DERIVATIVES

END MODULE Skyrme_Tau_Density_Derivatives_Mod
