!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Effective_Mass_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_EFFECTIVE_MASS(n_n,n_p,n,y,T, &
    M_n,M_p,L_n,L_p,Meff_n,Meff_p, &
    DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,derivatives)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Skyrme_Coefficients_Mod
    USE Physical_Constants_Mod, ONLY : one, two, Hbarc_Square, &
                                Mass_n, Mass_p

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n_n, n_p, n, y, T
    INTEGER(I4B), INTENT(IN) :: derivatives
    REAL(DP), INTENT(OUT) :: M_n, M_p, L_n, L_p, Meff_n, Meff_p
    REAL(DP), INTENT(OUT) :: DM_n_Dn_n, DM_n_Dn_p, DM_p_Dn_n, DM_p_Dn_p
    REAL(DP) :: p4n, EXP_mp4n, aux1, aux2

    p4n = cp4*n

!   compute exponentials present in APR EOS
    EXP_mp4n  = EXP(-p4n)

!   compute effective masses
!   set values of auxiliary functions M_n and M_p
    M_n = n*(cp3 + (one-y)*cp5)*EXP_mp4n
    M_p = n*(cp3 + (    y)*cp5)*EXP_mp4n

!   set values of auxiliary functions L_n and L_p
    L_n = Hbarc_Square/(two*Mass_n) + M_n
    L_p = Hbarc_Square/(two*Mass_p) + M_p

!   set values of effective masses Meff_n and Meff_p
    Meff_n = (Hbarc_Square/two) / L_n
    Meff_p = (Hbarc_Square/two) / L_p

    if (derivatives==0) return

!   derivatives of auxiliary functions M_t w.r.t. densities n_t'
    aux1 = (one-p4n)/n
    aux2 = cp5*EXP_mp4n

    DM_n_Dn_n = aux1*M_n + aux2*n_p/n
    DM_n_Dn_p = aux1*M_n - aux2*n_n/n
    DM_p_Dn_n = aux1*M_p - aux2*n_p/n
    DM_p_Dn_p = aux1*M_p + aux2*n_n/n

    if (derivatives==1) return

  END SUBROUTINE SKYRME_EFFECTIVE_MASS

END MODULE Skyrme_Effective_Mass_Mod
