!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!

MODULE Skyrme_Coefficients_Mod

  USE Kind_Types_Mod, ONLY : DP, I4B, LGCL

  IMPLICIT NONE

  SAVE

  INTEGER(I4B) :: Skyrme_parametrization_type
  INTEGER(I4B) :: non_local_terms
  INTEGER(I4B) :: Use_default_constants
  LOGICAL(LGCL) :: High_Density_Stiffening

! APR coefficients
  REAL(DP), DIMENSION(1:21) :: Coeff_p

  REAL(DP) ::  cp1,  cp2,  cp3,  cp4,  cp5,  cp6,  cp7
  REAL(DP) ::  cp8,  cp9, cp10, cp11, cp12, cp13, cp14
  REAL(DP) :: cp15, cp16, cp17, cp18, cp19, cp20, cp21

END MODULE Skyrme_Coefficients_Mod
