!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Force_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_FORCE(tau_n,tau_p,DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
    DUAPR_Dn_n,DUAPR_Dn_p,v_n,v_p)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : Neut_Prot_Mass_Diff

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: tau_n, tau_p
    REAL(DP), INTENT(IN) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP), INTENT(IN) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP), INTENT(IN) :: DUAPR_Dn_n, DUAPR_Dn_p

    REAL(DP), INTENT(OUT) :: v_n, v_p

!   neutron and proton force potentials
    v_n = tau_n*DM_n_Dn_n + tau_p*DM_p_Dn_n + DUAPR_Dn_n
    v_p = tau_n*DM_n_Dn_p + tau_p*DM_p_Dn_p + DUAPR_Dn_p

  END SUBROUTINE SKYRME_FORCE

END MODULE Skyrme_Force_Mod
