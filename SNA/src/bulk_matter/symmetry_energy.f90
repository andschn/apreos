!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Symmetry_Energy_Mod

CONTAINS

! for now only works for dens_neut=dens_prot=dens_nucl_sat/two
  SUBROUTINE SKYRME_SYMMETRY_ENERGY(J_0, J_half, L_0, K_0, Q_0)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Skyrme_Coefficients_Mod
    USE Physical_Constants_Mod, &
    ONLY : zero, half, one, two, three, four, six, ten, pi, &
           R_1_3, R_2_3, R_3_5, R_5_3, R_5_9, R_5_18, R_10_9, R_9_8, &
           R_3_8, R_10_27, R_25_9, R_27_8, R_50_27, R_50_81, &
           Hbarc_Square, Mass_n, Mass_p
    USE Nuclear_Matter_Properties_Mod, ONLY : n0 => Nuc_Sat_Dens
    USE Make_tables_Mod, ONLY : output_directory

    IMPLICIT NONE

    INTEGER(I4B) :: i
    REAL(DP), INTENT(OUT) :: J_0, J_half, L_0, K_0, Q_0

    REAL(DP) :: J_sym, L_sym, K_sym, Q_sym

    REAL(DP) :: n_n, n_p, n, y, delta
    REAL(DP) :: k_n, k_p
    REAL(DP) :: tau_n, tau_p
    REAL(DP) :: Dtau_n_Dy, Dtau_p_Dy
    REAL(DP) :: Dtau_n_Dn, Dtau_p_Dn
    REAL(DP) :: D2tau_n_Dy2, D2tau_p_Dy2
    REAL(DP) :: D2tau_n_DyDn, D2tau_p_DyDn
    REAL(DP) :: D2tau_n_Dn2, D2tau_p_Dn2
    REAL(DP) :: D3tau_n_DyDn2, D3tau_p_DyDn2
    REAL(DP) :: D3tau_n_Dy2Dn, D3tau_p_Dy2Dn
    REAL(DP) :: D3tau_n_Dn3, D3tau_p_Dn3
    REAL(DP) :: D4tau_n_DyDn3, D4tau_p_DyDn3
    REAL(DP) :: D4tau_n_Dy2Dn2, D4tau_p_Dy2Dn2
    REAL(DP) :: D5tau_n_Dy2Dn3, D5tau_p_Dy2Dn3

    REAL(DP) :: p4n, p9n
    REAL(DP) :: EXP_mp4n, DEXP_mp4n_Dn, D2EXP_mp4n_Dn2, D3EXP_mp4n_Dn3
    REAL(DP) :: EXP_mp9n2, dEXP_mp9n2_Dn, d2EXP_mp9n2_Dn2, d3EXP_mp9n2_Dn3

    REAL(DP) :: g_n, g_p
    REAL(DP) :: L_n, L_p
    REAL(DP) :: M_n, M_p
    REAL(DP) :: DM_n_Dy, DM_p_Dy
    REAL(DP) :: DM_n_Dn, DM_p_Dn
    REAL(DP) :: D2M_n_DyDn, D2M_p_DyDn
    REAL(DP) :: D2M_n_Dn2, D2M_p_Dn2
    REAL(DP) :: D3M_n_DyDn2, D3M_p_DyDn2
    REAL(DP) :: D3M_n_Dn3, D3M_p_Dn3
    REAL(DP) :: D4M_n_DyDn3, D4M_p_DyDn3

    REAL(DP) :: g1L, g2L, f1L, f2L, h1L, h2L, w1L, w2L
    REAL(DP) :: g1H, g2H, f1H, f2H, h1H, h2H, w1H, w2H
    REAL(DP) :: g1,  g2,  f1,  f2,  h1,  h2,  w1,  w2

    REAL(DP) :: Dg1_Dn, Dg2_Dn, Df1_Dn, Df2_Dn, Dh1_Dn, Dh2_Dn
    REAL(DP) :: D2g1_Dn2, D2g2_Dn2, D3g1_Dn3, D3g2_Dn3

    REAL(DP) :: UAPR, DUAPR_Dy, D2UAPR_Dy2, D3UAPR_Dy2Dn
    REAL(DP) :: D4UAPR_Dy2Dn2, D5UAPR_Dy2Dn3

    REAL(DP) :: J_aux, L_aux, K_aux, Q_aux

    REAL(DP) :: x0

    CHARACTER(LEN=64) :: sym_file

    sym_file = trim(output_directory) // '/symmetry.dat'

    open (11,file=sym_file)

    do i = 1, 10000

!     densities
      n = dble(i)*n0/1.d3
      y = half
      n_n = (one-y)*n
      n_p = y*n

!     fermi momenta
      k_n = (three*pi**two*n*(one-y))**R_1_3
      k_p = (three*pi**two*n*     y )**R_1_3

!     kinetic energy densities and its derivatives
      tau_n = R_3_5*k_n**two*n_n
      tau_p = R_3_5*k_p**two*n_p

      Dtau_n_Dy = - R_5_3*tau_n/(one-y)
      Dtau_p_Dy = + R_5_3*tau_p/(    y)

      Dtau_n_Dn = R_5_3*tau_n/n
      Dtau_p_Dn = R_5_3*tau_p/n

      D2tau_n_Dy2 = R_10_9*tau_n/(one-y)**two
      D2tau_p_Dy2 = R_10_9*tau_p/(    y)**two

      D2tau_n_DyDn = - R_25_9*tau_n/n_n
      D2tau_p_DyDn = + R_25_9*tau_p/n_p

      D2tau_n_Dn2 = R_10_9*tau_n/n**two
      D2tau_p_Dn2 = R_10_9*tau_p/n**two

      D3tau_n_DyDn2 = - R_50_27*tau_n/n_n/n
      D3tau_p_DyDn2 = + R_50_27*tau_p/n_p/n

      D3tau_n_Dy2Dn = R_50_27*tau_n/n/(one-y)**two
      D3tau_p_Dy2Dn = R_50_27*tau_p/n/(    y)**two

      D3tau_n_Dn3 = - R_10_27 * tau_n/n**three
      D3tau_p_Dn3 = - R_10_27 * tau_p/n**three

      D4tau_n_DyDn3 = + R_50_81*tau_n/(one-y)/n**three
      D4tau_p_DyDn3 = - R_50_81*tau_p/(    y)/n**three

      D4tau_n_Dy2Dn2 = R_10_9*D2tau_n_Dy2/n**two
      D4tau_p_Dy2Dn2 = R_10_9*D2tau_p_Dy2/n**two

      D5tau_n_Dy2Dn3 = - R_10_27 * D2tau_n_Dy2/n**three
      D5tau_p_Dy2Dn3 = - R_10_27 * D2tau_p_Dy2/n**three

!     compute auxiliary quantites
      p4n = cp4*n
      EXP_mp4n       =   EXP(-p4n)
      DEXP_mp4n_Dn   = - EXP_mp4n*cp4
      D2EXP_mp4n_Dn2 =   EXP_mp4n*cp4**two
      D3EXP_mp4n_Dn3 = - EXP_mp4n*cp4**three

      g_n = (cp3 + (one-y)*cp5)
      g_p = (cp3 +      y *cp5)

!     set values of auxiliary functions M_n and M_p and its derivatives
      M_n = g_n*n*EXP_mp4n
      M_p = g_p*n*EXP_mp4n

      DM_n_Dy = - n*EXP_mp4n*cp5
      DM_p_Dy = + n*EXP_mp4n*cp5

      DM_n_Dn = g_n*(one-p4n)*EXP_mp4n
      DM_p_Dn = g_p*(one-p4n)*EXP_mp4n

      D2M_n_DyDn = - cp5*(one-p4n)*EXP_mp4n
      D2M_p_DyDn = + cp5*(one-p4n)*EXP_mp4n

      D2M_n_Dn2 = - g_n*cp4*(two-p4n)*EXP_mp4n
      D2M_p_Dn2 = - g_p*cp4*(two-p4n)*EXP_mp4n

      D3M_n_DyDn2 = + cp5*cp4*(two-p4n)*EXP_mp4n
      D3M_p_DyDn2 = - cp5*cp4*(two-p4n)*EXP_mp4n

      D3M_n_Dn3 = g_n*cp4*cp4*(three-p4n)*EXP_mp4n
      D3M_p_Dn3 = g_p*cp4*cp4*(three-p4n)*EXP_mp4n

      D4M_n_DyDn3 = - cp5*cp4*cp4*(three-p4n)*EXP_mp4n
      D4M_p_DyDn3 = + cp5*cp4*cp4*(three-p4n)*EXP_mp4n

!     set values of auxiliary functions L_n and L_p
      L_n = Hbarc_Square/(two*Mass_n) + M_n
      L_p = Hbarc_Square/(two*Mass_p) + M_p

!     set potential
      p9n = cp9*n
      EXP_mp9n2 = EXP(-p9n**TWO)
      dEXP_mp9n2_Dn = - two*cp9*p9n*EXP_mp9n2
      d2EXP_mp9n2_Dn2 = - two*cp9*cp9*EXP_mp9n2 * (one - two*p9n*p9n)
      d3EXP_mp9n2_Dn3 = - two*cp9*cp9*dEXP_mp9n2_Dn * (one - two*p9n*p9n) &
                        + 8.d0*cp9*cp9*cp9*p9n*EXP_mp9n2

!     compute density functions g1 and g2
      g1L = - (cp1 + cp2*n + cp6*n*n + (cp10 + cp11*n)*EXP_mp9n2)*n*n
      g2L = - (cp12/n + cp7 + cp8*n + cp13*EXP_mp9n2)*n*n

      g1 = g1L
      g2 = g2L

      delta = one - two*y

!     compute density functions f1 and f2
      f1L = - (cp2 + two*cp6*n + cp11*EXP_mp9n2  &
              + (cp10 + cp11*n)*dEXP_mp9n2_Dn)*n*n
      f2L = - (- cp12/n**two + cp8 + cp13*dEXP_mp9n2_Dn)*n*n

      f1 = f1L
      f2 = f2L

      Dg1_Dn = f1 + two*g1/n
      Dg2_Dn = f2 + two*g2/n

!     compute density functions h1 and h2
      h1L = - (two*cp6 + two*cp11*dEXP_mp9n2_Dn &
            + (cp10 + cp11*n)*d2EXP_mp9n2_Dn2)*n*n
      h2L = - (two*cp12/n**three + cp13*d2EXP_mp9n2_Dn2)*n*n

      h1 = h1L
      h2 = h2L

      D2g1_Dn2 = h1 + four*f1/n + two*g1/n**two
      D2g2_Dn2 = h2 + four*f2/n + two*g2/n**two

!     compute density functions w1 and w2
      w1L = - (three*cp11*d2EXP_mp9n2_Dn2 + &
                (cp10 + cp11*n)*d3EXP_mp9n2_Dn3)*n*n
      w2L = - (- six*cp12/n**four + cp13*d3EXP_mp9n2_Dn3)*n*n

      w1 = w1L
      w2 = w2L

      D3g1_Dn3 = w1 + six*h1/n + six*f1/n**two
      D3g2_Dn3 = w2 + six*h2/n + six*f2/n**two

!     compute potential and its derivatives
      UAPR = g1*(one-delta**two) + g2*delta**two
      DUAPR_Dy = four*delta*(g1 - g2)
      D2UAPR_Dy2    = - 8.d0*(g1       - g2)
      D3UAPR_Dy2Dn  = - 8.d0*(Dg1_Dn   - Dg2_Dn)
      D4UAPR_Dy2Dn2 = - 8.d0*(D2g1_Dn2 - D2g2_Dn2)
      D5UAPR_Dy2Dn3 = - 8.d0*(D3g1_Dn3 - D3g2_Dn3)

!     compute J_sym = S_2|_{n,1/2} where S_2 = (1/8) d²e_B/dy²
      J_aux = ( two*DM_n_Dy*Dtau_n_Dy + two*DM_p_Dy*Dtau_p_Dy + &
                L_n*D2tau_n_Dy2 + L_p*D2tau_p_Dy2 + D2UAPR_Dy2)
      J_sym = J_aux/8.d0/n

!     compute L_sym = 3n*dS_2/dn|_{n,1/2}
      L_aux = ( two*D2M_n_DyDn*Dtau_n_Dy + two*D2M_p_DyDn*Dtau_p_Dy + &
                two*DM_n_Dy*D2tau_n_DyDn + two*DM_p_Dy*D2tau_p_DyDn + &
                DM_n_Dn*D2tau_n_Dy2 + DM_p_Dn*D2tau_p_Dy2 + &
                L_n*D3tau_n_Dy2Dn + L_p*D3tau_p_Dy2Dn + D3UAPR_Dy2Dn)
      L_sym = R_3_8*(L_aux - J_aux/n)

!     compute L_sym = 9n²*d²S_2/dn²|_{n,1/2}
      K_aux = ( two*D3M_n_DyDn2*Dtau_n_Dy + two*D3M_p_DyDn2*Dtau_p_Dy + &
                four*(D2M_n_DyDn*D2tau_n_DyDn + D2M_p_DyDn*D2tau_p_DyDn) + &
                two*DM_n_Dy*D3tau_n_DyDn2 + two*DM_p_Dy*D3tau_p_DyDn2 + &
                D2M_n_Dn2*D2tau_n_Dy2 + D2M_p_Dn2*D2tau_p_Dy2 + &
                two*(DM_n_Dn*D3tau_n_Dy2Dn + DM_p_Dn*D3tau_p_Dy2Dn) + &
                L_n*D4tau_n_Dy2Dn2 + L_p*D4tau_p_Dy2Dn2 + D4UAPR_Dy2Dn2)
      K_sym = R_9_8*(K_aux*n - two*(L_aux - J_aux/n))

!     compute Q_sym = 27n³*d³S_2/dn³|_{n,1/2}
      Q_aux = ( two*D4M_n_DyDn3*Dtau_n_Dy + two*D4M_p_DyDn3*Dtau_p_Dy + & !
                six*(D3M_n_DyDn2*D2tau_n_DyDn + D3M_p_DyDn2*D2tau_p_DyDn + & !
                D2M_n_DyDn*D3tau_n_DyDn2 + D2M_p_DyDn*D3tau_p_DyDn2) + & !
                two*DM_n_Dy*D4tau_n_DyDn3 + two*DM_p_Dy*D4tau_p_DyDn3 + & !
                D3M_n_Dn3*D2tau_n_Dy2 + D3M_p_Dn3*D2tau_p_Dy2 + & !
                three*(D2M_n_Dn2*D3tau_n_Dy2Dn + D2M_p_Dn2*D3tau_p_Dy2Dn + & !
                DM_n_Dn*D4tau_n_Dy2Dn2 + DM_p_Dn*D4tau_p_Dy2Dn2) + & !
                L_n*D5tau_n_Dy2Dn3 + L_p*D5tau_p_Dy2Dn3 + & !
                D5UAPR_Dy2Dn3)

      Q_sym = R_27_8*(n**two*Q_aux - three*n*K_aux + 6.d0*(L_aux - J_aux/n))

      x0 = (n - n0)/three

      if (abs(x0) < 1.0d-10) then
        J_0 = J_sym
        L_0 = L_sym
        K_0 = K_sym
        Q_0 = Q_sym
      write (*,*) J_aux, L_aux, K_aux, Q_aux

        !write (*,*)  Q_aux, D5UAPR_Dy2Dn3
      endif

      if (abs(n - n0/2.d0) < 1.d-6) then
        J_half = J_sym
      endif

      write (11,"(2es20.12)") dble(i)/1.D3, &
             J_sym + L_sym*x0 + K_sym*x0*x0/2.D0 + Q_sym*x0*x0*x0/6.D0
    enddo

    close(11)

  END SUBROUTINE SKYRME_SYMMETRY_ENERGY

END MODULE Symmetry_Energy_Mod
