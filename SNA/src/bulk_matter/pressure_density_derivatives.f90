!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Pressure_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_PRESSURE_DENSITY_DERIVATIVES(n_n,n_p,&
    Dmu_n_Dn_n,Dmu_n_Dn_p,Dmu_p_Dn_n,Dmu_p_Dn_p,&
    DP_Dn_n,DP_Dn_p)

    USE Kind_Types_Mod, ONLY : DP, I4B

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: n_n, n_p
    REAL(DP), INTENT(IN)  :: Dmu_n_Dn_n, Dmu_n_Dn_p
    REAL(DP), INTENT(IN)  :: Dmu_p_Dn_n,Dmu_p_Dn_p
    REAL(DP), INTENT(OUT) :: DP_Dn_n, DP_Dn_p

!   pressure derivatives
    DP_Dn_n = n_n*Dmu_n_Dn_n + n_p*Dmu_p_Dn_n
    DP_Dn_p = n_n*Dmu_n_Dn_p + n_p*Dmu_p_Dn_p

  END SUBROUTINE SKYRME_PRESSURE_DENSITY_DERIVATIVES

END MODULE Skyrme_Pressure_Density_Derivatives_Mod
