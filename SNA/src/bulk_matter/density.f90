!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Density_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_DENSITY(log10_n_n,log10_n_p,n_n,n_p,n,y,delta)
!  Given log10(n_n) and log10(n_p) compute n_n, n_p, n, y

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : ZERO, ONE, TWO, TEN, &
                                log10_dens_min, log10_dens_max
    USE, INTRINSIC :: IEEE_ARITHMETIC

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: log10_n_n, log10_n_p
    REAL(DP), INTENT(OUT) :: n_n, n_p, n, y, delta
    REAL(DP) :: eff_n_n, eff_n_p

!   neutron and proton effective densities
!    Limits densities to avoid underflow/overflow in calculations
    eff_n_n = max(log10_n_n,log10_dens_min)
    eff_n_n = min(eff_n_n,log10_dens_max)
    eff_n_n = ten**eff_n_n

    eff_n_p = max(log10_n_p,log10_dens_min)
    eff_n_p = min(eff_n_p,log10_dens_max)
    eff_n_p = ten**eff_n_p

    n_n = eff_n_n
    n_p = eff_n_p

    n = n_n + n_p
    y = n_p/n
    delta = one - two*y

!   fix nuclear density and proton fractions in extreme cases
    IF (log10_n_n>log10_n_p+16.D0) THEN
      n = n_n
      y = n_p/n
      IF (ieee_is_nan(y)) y = zero
      n_p = eff_n_p
    ELSEIF (log10_n_n<log10_n_p-16.D0) THEN
      n = n_p
      y = one
      n_n = eff_n_n
    ENDIF

  END SUBROUTINE SKYRME_DENSITY

END MODULE Skyrme_Density_Mod
