!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Skyrme_Mu_Density_Derivatives_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE SKYRME_MU_DENSITY_DERIVATIVES(T,Deta_n_Dn_n, Deta_n_Dn_p,&
      Deta_p_Dn_n,Deta_p_Dn_p,Dv_n_Dn_n,Dv_n_Dn_p,Dv_p_Dn_n,Dv_p_Dn_p, &
      Dmu_n_Dn_n, Dmu_n_Dn_p,Dmu_p_Dn_n, Dmu_p_Dn_p)

    USE Kind_Types_Mod, ONLY : DP, I4B

    IMPLICIT NONE

    REAL(DP), INTENT(IN)  :: T
    REAL(DP), INTENT(IN)  :: Deta_n_Dn_n, Deta_n_Dn_p
    REAL(DP), INTENT(IN)  :: Deta_p_Dn_n, Deta_p_Dn_p
    REAL(DP), INTENT(IN)  :: Dv_n_Dn_n, Dv_n_Dn_p
    REAL(DP), INTENT(IN)  :: Dv_p_Dn_n, Dv_p_Dn_p
    REAL(DP), INTENT(OUT) :: Dmu_n_Dn_n, Dmu_n_Dn_p
    REAL(DP), INTENT(OUT) :: Dmu_p_Dn_n, Dmu_p_Dn_p

!   mu derivatives
    Dmu_n_Dn_n = T*Deta_n_Dn_n + Dv_n_Dn_n
    Dmu_n_Dn_p = T*Deta_n_Dn_p + Dv_n_Dn_p
    Dmu_p_Dn_n = T*Deta_p_Dn_n + Dv_p_Dn_n
    Dmu_p_Dn_p = T*Deta_p_Dn_p + Dv_p_Dn_p

  END SUBROUTINE SKYRME_MU_DENSITY_DERIVATIVES

END MODULE Skyrme_Mu_Density_Derivatives_Mod
