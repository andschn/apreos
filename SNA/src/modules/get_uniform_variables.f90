!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published
!    by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
Module Get_Uniform_Variables_Mod

  USE Kind_Types_Mod, ONLY : I4B, DP
  USE Physical_Constants_Mod, ONLY : ZERO, HALF, ONE, TWO, TEN, v_alpha

  IMPLICIT NONE

CONTAINS

  SUBROUTINE GET_UNIFORM_VARIABLES (density, Yp, x_in, log10_n_p, log10_n_n, log10_u)

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: density, Yp, x_in
    REAL(DP), INTENT(OUT) :: log10_n_p,log10_n_n, log10_u

    REAL(DP) :: Xp, nva

    Xp = TEN**x_in

    nva = density*v_alpha

    IF (Yp>HALF) THEN
      log10_n_p = log10(density*(-Xp*nva*Yp &
                      - two*((one-two*Yp)-Xp))/(two-nva*(one-Yp)))
      log10_n_n = log10(density*Xp)
      log10_u    = -290.D0
    ELSE
      log10_n_n = log10(density*(-Xp*nva*(one-Yp) &
                          + two*((one-two*Yp)+Xp))/(two-nva*Yp))
      log10_n_p = log10(density*Xp)
      log10_u    = -290.0D0
    ENDIF

  END SUBROUTINE GET_UNIFORM_VARIABLES

End Module Get_Uniform_Variables_Mod
