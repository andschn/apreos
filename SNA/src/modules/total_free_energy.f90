!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Free_Energy_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE FREE_ENERGY( log10_n_no, log10_n_po, log10_u, n, T, Yp, flag, &
  F_o, F_i, F_alpha, F_TR, F_SC, U_APR, &
  n_no, n_po, n_ni, n_pi, n_alpha, n_heavy, &
  A, Z, rad, F, P, S, E, &
  DF_Dn, DF_Dy, DF_DT, DP_Dn, DP_Dy, DP_DT, &
  DS_Dn, DS_Dy, DS_DT, DE_Dn, DE_Dy, DE_DT, &
  mu_no, mu_po, Meff_no, Meff_po, &
  Dmu_no_DT, Dmu_no_Dn, Dmu_no_Dy, &
  Dmu_po_DT, Dmu_po_Dn, Dmu_po_Dy, &
  dLog10_n_no_dn, dLog10_n_po_dn, dLog10_u_dn, &
  dLog10_n_no_dT, dLog10_n_po_dT, dLog10_u_dT, &
  non_uniform_eq, non_uniform_jac, objective, error )

    USE Kind_Types_Mod, ONLY : DP, I4B, LGCL

    USE Physical_Constants_Mod, &
    ONLY : ZERO, ONE, TWO, FOUR, TEN, R_3_2, v_alpha

    USE, INTRINSIC :: IEEE_ARITHMETIC

    USE Skyrme_Bulk_Mod, &
    ONLY : SKYRME_BULK_PROPERTIES

    USE Skyrme_Bulk_Density_Derivatives_Mod, &
    ONLY : SKYRME_BULK_DENSITY_DERIVATIVES

    USE Skyrme_Bulk_Eta_Derivatives_Mod, &
    ONLY : SKYRME_BULK_ETA_DERIVATIVES

    USE Skyrme_Bulk_Temperature_Derivatives_Mod, &
    ONLY : SKYRME_BULK_TEMPERATURE_DERIVATIVES

    USE Skyrme_Effective_Mass_Density_Derivatives_Mod, &
    ONLY : SKYRME_EFFECTIVE_MASS_DENSITY_DERIVATIVES

    USE Skyrme_Potential_Density_Derivatives_Mod, &
    ONLY : SKYRME_POTENTIAL_DENSITY_DERIVATIVES

    USE Find_Uniform_Matter_Derivatives_Mod
    USE Find_Non_Uniform_Matter_Derivatives_Mod
    USE Alpha_Properties_Mod
    USE Free_Energy_Heavy_Mod
    USE Outside_Properties_Mod
    USE Heavy_Properties_Mod

    IMPLICIT NONE
!
!   if objective = 0 only calculate free energies and densities
!                     used for solution of uniform system
!   if objective = 3 adds to calculation free energy
!                    first and second derivatives for uniform matter
!   if objective = 1 adds to calculation values for the
!                     non-uniform matter system of equations
!   if objective = 2 adds to calculation values for the
!                     analytical jacobian for non-uniform system of equations
!   if objective = 4 adds to calculation free energy
!                    first and second derivatives for non-uniform matter
!
!   flag =  0 determine whether low or high density phase
!   flag = -1 enforce low  density phase
!   flag = +1 enforce high density phase
!
!   error = 0 if all physical constranits are satisfied and u=0
!   error = 1 if all physical constranits are satisfied and u>0
!   error < 0 some physical constraint is not satisfied

    REAL(DP), INTENT(IN) :: log10_n_no, log10_n_po, log10_u
    REAL(DP), INTENT(IN) :: n, T, Yp
    INTEGER(I4B), INTENT(IN) :: flag

    REAL(DP), INTENT(OUT) :: F_o, F_i, F_alpha, F_TR, F_SC, U_APR
    REAL(DP), INTENT(OUT) :: n_no, n_po, n_ni, n_pi, n_alpha, n_heavy
    REAL(DP), INTENT(OUT) :: dLog10_n_no_dn, dLog10_n_po_dn, dLog10_u_dn
    REAL(DP), INTENT(OUT) :: dLog10_n_no_dT, dLog10_n_po_dT, dLog10_u_dT

    REAL(DP), DIMENSION(3), INTENT(OUT) :: non_uniform_eq
    REAL(DP), DIMENSION(3,3), INTENT(OUT) :: non_uniform_jac

    INTEGER(I4B), INTENT(IN) :: objective
    INTEGER(I4B), INTENT(OUT) :: error

    REAL(DP), INTENT(OUT) :: A, Z, rad

    REAL(DP), INTENT(OUT) :: F, P, S, E
    REAL(DP), INTENT(OUT) :: DF_Dn, DF_Dy, DF_DT
    REAL(DP), INTENT(OUT) :: DP_Dn, DP_Dy, DP_DT
    REAL(DP), INTENT(OUT) :: DS_Dn, DS_Dy, DS_DT
    REAL(DP), INTENT(OUT) :: DE_Dn, DE_Dy, DE_DT

    REAL(DP), INTENT(OUT) :: Dmu_no_DT, Dmu_no_Dn, Dmu_no_Dy
    REAL(DP), INTENT(OUT) :: Dmu_po_DT, Dmu_po_Dn, Dmu_po_Dy

    REAL(DP) :: D2F_DT2, D2F_DTDn, D2F_DyDT, D2F_DnDy, D2F_Dn2, D2F_DY2

    REAL(DP) :: u, odu, B(3), JAC(3,3)

    ! inside region properties
    REAL(DP) :: log10_n_ni,log10_n_pi
    REAL(DP) :: n_i,y_i,delta_i
    REAL(DP) :: g1_i, g2_i, f1_i, f2_i, h1_i, h2_i
    REAL(DP) :: UAPR_i, DUAPR_i_Dn_ni, DUAPR_i_Dn_pi
    REAL(DP) :: D2UAPR_i_Dn_ni2, D2UAPR_i_Dn_nin_pi
    REAL(DP) :: D2UAPR_i_Dn_pin_ni, D2UAPR_i_Dn_pi2
    REAL(DP) :: Meff_ni,Meff_pi
    REAL(DP) :: eta_ni,eta_pi
    REAL(DP) :: tau_ni,tau_pi
    REAL(DP) :: v_ni,v_pi
    REAL(DP) :: mu_ni,mu_pi
    REAL(DP) :: G_ni,G_pi,L_ni,L_pi,M_ni,M_pi
    REAL(DP) :: p_i, s_i, e_i
    REAL(DP) :: DM_ni_Dn_ni, DM_ni_Dn_pi, DM_pi_Dn_ni, DM_pi_Dn_pi
    REAL(DP) :: DMeff_ni_Dn_ni, DMeff_ni_Dn_pi, DMeff_pi_Dn_ni, DMeff_pi_Dn_pi
    REAL(DP) :: DMeff_ni_Dni, DMeff_ni_Dyi, DMeff_pi_Dni, DMeff_pi_Dyi
    REAL(DP) :: D2M_ni_Dn_ni2, D2M_ni_Dn_nin_pi, D2M_ni_Dn_pi2
    REAL(DP) :: D2M_pi_Dn_ni2, D2M_pi_Dn_pin_ni, D2M_pi_Dn_pi2
    REAL(DP) :: Dtau_ni_Dn_ni, Dtau_ni_Dn_pi, Dtau_pi_Dn_ni, Dtau_pi_Dn_pi
    REAL(DP) :: DMu_ni_Dn_ni, DMu_ni_Dn_pi, DMu_pi_Dn_ni, DMu_pi_Dn_pi
    REAL(DP) :: Deta_ni_Dn_ni, Deta_ni_Dn_pi, Deta_pi_Dn_ni, Deta_pi_Dn_pi
    REAL(DP) :: DP_i_Dn_ni, DP_i_Dn_pi, DS_i_Dn_ni, DS_i_Dn_pi
    REAL(DP) :: DE_i_Dn_ni, DE_i_Dn_pi, DF_i_Dn_ni, DF_i_Dn_pi
    REAL(DP) :: Dv_ni_Dn_ni, Dv_ni_Dn_pi, Dv_pi_Dn_ni, Dv_pi_Dn_pi
    REAL(DP) :: Dtau_ni_Deta_ni,Dtau_ni_Deta_pi,Dtau_pi_Deta_ni,Dtau_pi_Deta_pi
    REAL(DP) :: DMu_ni_Deta_ni, DMu_ni_Deta_pi, DMu_pi_Deta_ni, DMu_pi_Deta_pi
    REAL(DP) :: Dn_ni_Deta_ni, Dn_ni_Deta_pi, Dn_pi_Deta_ni, Dn_pi_Deta_pi
    REAL(DP) :: DP_i_Deta_n_i, DP_i_Deta_p_i, DS_i_Deta_i_n, DS_i_Deta_p_i
    REAL(DP) :: DE_i_Deta_n_i, DE_i_Deta_p_i, DF_i_Deta_i_n, DF_i_Deta_p_i
    REAL(DP) :: Dn_ni_DT, Dn_pi_DT, Dtau_ni_DT, Dtau_pi_DT, Dmu_ni_DT, Dmu_pi_DT
    REAL(DP) :: DP_i_DT, DS_i_DT, DE_i_DT, DF_i_DT
    REAL(DP) :: DF_i_Dy_i, DF_i_Dn_i
    REAL(DP) :: DE_i_Dy_i, DE_i_Dn_i
    REAL(DP) :: DS_i_Dy_i, DS_i_Dn_i
    REAL(DP) :: Dmu_ni_Dn_i,  Dmu_ni_Dy_i,  Dmu_pi_Dn_i,  Dmu_pi_Dy_i
    REAL(DP) :: Dtau_ni_Dn_i, Dtau_ni_Dy_i, Dtau_pi_Dn_i, Dtau_pi_Dy_i
    REAL(DP) :: DP_i_Dn_i,  DP_i_Dy_i
    LOGICAL :: high_density_in

    ! outside region properties
    REAL(DP) :: n_o,y_o,delta_o
    REAL(DP) :: Meff_no,Meff_po
    REAL(DP) :: g1_o, g2_o, f1_o, f2_o, h1_o, h2_o
    REAL(DP) :: UAPR_o, DUAPR_o_Dn_no, DUAPR_o_Dn_po
    REAL(DP) :: D2UAPR_o_Dn_no2, D2UAPR_o_Dn_non_po
    REAL(DP) :: D2UAPR_o_Dn_pon_no, D2UAPR_o_Dn_po2
    REAL(DP) :: eta_no,eta_po
    REAL(DP) :: tau_no,tau_po
    REAL(DP) :: v_no,v_po
    REAL(DP) :: mu_no,mu_po
    REAL(DP) :: G_no,G_po,L_no,L_po,M_no,M_po
    REAL(DP) :: p_o, s_o, e_o
    REAL(DP) :: DM_no_Dn_no,DM_no_Dn_po,DM_po_Dn_no,DM_po_Dn_po
    REAL(DP) :: DMeff_no_Dn_no, DMeff_no_Dn_po, DMeff_po_Dn_no, DMeff_po_Dn_po
    REAL(DP) :: DMeff_no_Dno, DMeff_no_Dyo, DMeff_po_Dno, DMeff_po_Dyo
    REAL(DP) :: D2M_no_Dn_no2, D2M_no_Dn_non_po, D2M_no_Dn_po2
    REAL(DP) :: D2M_po_Dn_no2, D2M_po_Dn_pon_no, D2M_po_Dn_po2
    REAL(DP) :: Dtau_no_Dn_no, Dtau_no_Dn_po, Dtau_po_Dn_no, Dtau_po_Dn_po
    REAL(DP) :: DMu_no_Dn_no, DMu_no_Dn_po, DMu_po_Dn_no, DMu_po_Dn_po
    REAL(DP) :: Deta_no_Dn_no, Deta_no_Dn_po, Deta_po_Dn_no, Deta_po_Dn_po
    REAL(DP) :: DP_o_Dn_no, DP_o_Dn_po, DS_o_Dn_no, DS_o_Dn_po
    REAL(DP) :: DE_o_Dn_no, DE_o_Dn_po, DF_o_Dn_no, DF_o_Dn_po
    REAL(DP) :: Dv_no_Dn_no, Dv_no_Dn_po, Dv_po_Dn_no, Dv_po_Dn_po
    REAL(DP) :: Dtau_no_Deta_no,Dtau_no_Deta_po,Dtau_po_Deta_no,Dtau_po_Deta_po
    REAL(DP) :: DMu_no_Deta_no, DMu_no_Deta_po, DMu_po_Deta_no, DMu_po_Deta_po
    REAL(DP) :: Dn_no_Deta_no, Dn_no_Deta_po, Dn_po_Deta_no, Dn_po_Deta_po
    REAL(DP) :: DP_o_Deta_no, DP_o_Deta_po, DS_o_Deta_no, DS_o_Deta_po
    REAL(DP) :: DE_o_Deta_no, DE_o_Deta_po, DF_o_Deta_no, DF_o_Deta_po
    REAL(DP) :: Dn_no_DT, Dn_po_DT, Dtau_no_DT, Dtau_po_DT
    REAL(DP) :: DP_o_DT, DS_o_DT, DE_o_DT, DF_o_DT
    LOGICAL :: high_density_out
    ! alpha particle related quantities and properties
    REAL(DP) :: mass_Q, exc_v_alpha

    ! alpha particle properties
    REAL(DP) :: P_alpha, S_alpha, E_alpha, mu_alpha
    REAL(DP) :: Dn_alpha_Dn_no, Dn_alpha_Dn_po, Dmu_alpha_Dn_no, Dmu_alpha_Dn_po
    REAL(DP) :: Dn_alpha_Deta_no,  Dn_alpha_Deta_po,  Dn_alpha_DT
    REAL(DP) :: Dmu_alpha_Deta_no, Dmu_alpha_Deta_po, Dmu_alpha_DT
    REAL(DP) :: DP_alpha_Deta_no,  DP_alpha_Deta_po,  DP_alpha_DT
    REAL(DP) :: DS_alpha_Deta_no,  DS_alpha_Deta_po,  DS_alpha_DT
    REAL(DP) :: DE_alpha_Deta_no,  DE_alpha_Deta_po,  DE_alpha_DT
    REAL(DP) :: DF_alpha_Deta_no,  DF_alpha_Deta_po,  DF_alpha_DT

    ! derivatives of inside nucleon properties w.r.t. independent variable u
    REAL(DP) :: Dn_i_Du, Dy_i_Du

    REAL(DP) :: Dn_i_Dn_no, Dn_i_Dn_po
    REAL(DP) :: Dy_i_Dn_no, Dy_i_Dn_po
    REAL(DP) :: DP_i_Dn_no, DP_i_Dn_po
    REAL(DP) :: DP_i_Dn_o, DP_i_Dy_o
    ! partial derivatives of inside variables w.r.t. independent variables
    REAL(DP) :: DA_in_Du(1:3), DA_in_Dn_no(1:3), DA_in_Dn_po(1:3)
    ! partial derivatives of outside variables w.r.t. independent variables
    REAL(DP) :: DA_out_Du(1:3), DA_out_Dn_no(1:3), DA_out_Dn_po(1:3)
    ! partial derivatives of equilibrium eqs. w.r.t. independent variables
    REAL(DP) :: DB_Dup(1:3), DB_Dn_i(1:3), DB_Dy_i(1:3), DB_DT(1:3)
    ! full derivatives of equilibrium eqs. w.r.t. independent variables
    REAL(DP) :: DB_Du(1:3), DB_Dn_no(1:3), DB_Dn_po(1:3)
    ! total density of nucleons and protons outside
    ! (includes nucleons in alphas)
    REAL(DP) :: tot_n_o, tot_n_po
    ! some auxiliary densities
    REAL(DP) :: u_out, n_1, n_2
    ! derivatives of translational and surface+coulomb free energies
    REAL(DP) :: DF_SC_Du, DF_SC_Dn_i, DF_SC_DT, DF_SC_Dy_i
    REAL(DP) :: D2F_SC_Du2, D2F_SC_DuDn_i, D2F_SC_DuDT, D2F_SC_DuDy_i
    REAL(DP) :: D2F_SC_Dn_i2, D2F_SC_Dn_iDT, D2F_SC_Dn_iDy_i
    REAL(DP) :: D2F_SC_DT2, D2F_SC_DTDy_i, D2F_SC_Dy_i2
    REAL(DP) :: DF_TR_Du, DF_TR_Dn_i, DF_TR_DT, DF_TR_Dy_i
    REAL(DP) :: D2F_TR_Du2, D2F_TR_DuDn_i, D2F_TR_DuDT, D2F_TR_DuDy_i
    REAL(DP) :: D2F_TR_Dn_i2, D2F_TR_Dn_iDT, D2F_TR_Dn_iDy_i
    REAL(DP) :: D2F_TR_DT2, D2F_TR_DTDy_i, D2F_TR_Dy_i2
    ! total derivatives of variables (u,n_i,y_i,eta_n,eta_p) w.r.t. (n,y,T)
    REAL(DP) :: DU_DN, Dy_i_DN, Dn_i_DN, Deta_no_DN, Deta_po_DN
    REAL(DP) :: DU_DT, Dy_i_DT, Dn_i_DT, Deta_no_DT, Deta_po_DT
    REAL(DP) :: DU_DY, Dy_i_DY, Dn_i_DY, Deta_no_DY, Deta_po_DY
    ! total outside derivatives of free energy, entropy and chemical potentials
    REAL(DP) :: F_out, DF_out_DT, DF_out_Dn, DF_out_Dy
    REAL(DP) :: S_out, DS_out_DT, DS_out_Dn, DS_out_Dy
    ! total heavy nuclei derivatives of free energy and entropy
    REAL(DP) :: F_h, DF_h_DT, DF_h_Dn, DF_h_Dy
    REAL(DP) :: S_h, DS_h_DT, DS_h_Dn, DS_h_Dy

!   set output to zero (?)
    F_o = 1.d100 ; F_i = 1.d100; F_alpha = 1.d100
    F_TR = 1.d00; F_SC = 1.d100
    n_no = zero; n_po = zero
    n_ni = zero; n_pi = zero
    n_alpha = zero; n_heavy = zero
    dLog10_n_no_dn = zero; dLog10_n_po_dn = zero; dLog10_u_dn = zero
    dLog10_n_no_dT = zero; dLog10_n_po_dT = zero; dLog10_u_dT = zero

    F = 1.d100 ; P = 1.d100 ; S = 1.d100 ; E = 1.d100
    DF_Dn = zero ; DF_Dy = zero ; DF_DT = zero
    DP_Dn = zero ; DP_Dy = zero ; DP_DT = zero
    DS_Dn = zero ; DS_Dy = zero ; DS_DT = zero
    DE_Dn = zero ; DE_Dy = zero ; DE_DT = zero
    Dmu_no_DT = zero ; Dmu_no_Dn = zero ; Dmu_no_Dy = zero
    Dmu_po_DT = zero ; Dmu_po_Dn = zero ; Dmu_po_Dy = zero

    non_uniform_eq = 1.d100

!   compute volume fraction occupied by heavy nuclei
    u = TEN**log10_u
    odu = ONE - u
!   set no errors so far
    error = 0

!   check if volume fraction of heavy nuclei not in range 0<=u<=1.
    IF (u > ONE .OR. u < ZERO) THEN
      error = -1
      RETURN
    ENDIF

!   get properties of nucleons outside of heavy nuclei
    CALL SKYRME_BULK_PROPERTIES(log10_n_no, log10_n_po, T, flag, &
      n_no, n_po, n_o, y_o, delta_o, &
      Meff_no, Meff_po, M_no, M_po, L_no,L_po, &
      DM_no_Dn_no, DM_no_Dn_po, DM_po_Dn_no, DM_po_Dn_po, &
      eta_no, eta_po, tau_no, tau_po, v_no, v_po, mu_no, mu_po,&
      g1_o, g2_o, f1_o, f2_o, UAPR_o, DUAPR_o_Dn_no, DUAPR_o_Dn_po,&
      P_o, F_o, S_o, E_o, high_density_out)

    U_APR = UAPR_o

!   check if outside density larger than total density
!    (add small tolerance to sum of n_no and n_po)
    IF (n_o > (one+5.d-2)*n) THEN
      error = -2
      RETURN
    ENDIF

!   get properties of alpha particles
    CALL ALPHA_PROPERTIES (objective, n_no, n_po, mu_no, mu_po, T, F_o, P_o, &
       n_alpha, mu_alpha, P_alpha, S_alpha, E_alpha, F_alpha)

!   chech if alpha particles occupy more than 100% of Wigner-Seiz cell volume
    exc_v_alpha = ONE - v_alpha*n_alpha
    IF (exc_v_alpha < ZERO) THEN
      error = -3
      RETURN
    ENDIF

!   if contribution due to heavy nuclei is very small ignore them
    IF (log10_u < -100.0D0) THEN
      error = 1
      F_i  = ZERO
      F_TR = ZERO
      F_SC = ZERO
      IF (objective == 0) THEN
        F_o = exc_v_alpha*F_o
        RETURN
      ENDIF
    ENDIF

!   if need to compute derivatives of nucleons outside heavy nuclei do this:
    IF (objective == 2 .or. objective == 3 .or. objective ==4) THEN
!     compute density derivatives of nucleons outside heavy nuclei
      CALL SKYRME_BULK_DENSITY_DERIVATIVES( flag, &
            log10_n_no, log10_n_po, T, G_no, G_po, &
            Deta_no_Dn_no, Deta_no_Dn_po, Deta_po_Dn_no, Deta_po_Dn_po, &
            Dtau_no_Dn_no, Dtau_no_Dn_po, Dtau_po_Dn_no, Dtau_po_Dn_po, &
            Dmu_no_Dn_no, Dmu_no_Dn_po,   Dmu_po_Dn_no, Dmu_po_Dn_po,   &
            Dv_no_Dn_no, Dv_no_Dn_po,     Dv_po_Dn_no, Dv_po_Dn_po,     &
            DMeff_no_Dn_no, DMeff_no_Dn_po, DMeff_po_Dn_no, DMeff_po_Dn_po, &
            DP_o_Dn_no, DP_o_Dn_po, DS_o_Dn_no, DS_o_Dn_po, &
            DE_o_Dn_no, DE_o_Dn_po, DF_o_Dn_no, DF_o_Dn_po) 

!     compute eta derivatives of nucleons outside heavy nuclei
      CALL SKYRME_BULK_ETA_DERIVATIVES(n, T, Yp, n_no, n_po, s_o, &
            Meff_no, Meff_po, tau_no, tau_po, mu_no, mu_po, &
            eta_no, eta_po, G_no, G_po, L_no, L_po, &
            Deta_no_Dn_no, Deta_no_Dn_po, Deta_po_Dn_no, Deta_po_Dn_po, &
            Dtau_no_Dn_no, Dtau_no_Dn_po, Dtau_po_Dn_no, Dtau_po_Dn_po, &
            Dmu_no_Dn_no,  Dmu_no_Dn_po,  Dmu_po_Dn_no,  Dmu_po_Dn_po,  &
            DM_no_Dn_no, DM_no_Dn_po, DM_po_Dn_no, DM_po_Dn_po, &
            DP_o_Dn_no, DP_o_Dn_po, &
            DP_o_Deta_no, DP_o_Deta_po, DF_o_Deta_no, DF_o_Deta_po, &
            DS_o_Deta_no, DS_o_Deta_po, DE_o_Deta_no, DE_o_Deta_po, &
            Dn_no_Deta_no,  Dn_no_Deta_po,  Dn_po_Deta_no,  Dn_po_Deta_po,  &
            Dmu_no_Deta_no, Dmu_no_Deta_po, Dmu_po_Deta_no, Dmu_po_Deta_po, &
            Dtau_no_Deta_no, Dtau_no_Deta_po, Dtau_po_Deta_no, Dtau_po_Deta_po)

      CALL SKYRME_EFFECTIVE_MASS_DENSITY_DERIVATIVES(n_o, y_o, &
            n_no, n_po, Meff_no, Meff_po, M_no, M_po, &
            DM_no_Dn_no, DM_no_Dn_po, DM_po_Dn_no, DM_po_Dn_po, &
            DMeff_no_Dn_no, DMeff_no_Dn_po, DMeff_po_Dn_no, DMeff_po_Dn_po, &
            DMeff_no_Dno, DMeff_po_Dno, DMeff_no_Dyo, DMeff_po_Dyo, &
            D2M_no_Dn_no2, D2M_no_Dn_non_po, D2M_no_Dn_po2, &
            D2M_po_Dn_no2, D2M_po_Dn_pon_no, D2M_po_Dn_po2)

      CALL SKYRME_POTENTIAL_DENSITY_DERIVATIVES(high_density_out,&
            n_o,delta_o,n_no,n_po,g1_o,g2_o,f1_o,f2_o, &
            UAPR_o,DUAPR_o_Dn_no,DUAPR_o_Dn_po,h1_o,h2_o,&
            D2UAPR_o_Dn_no2,D2UAPR_o_Dn_non_po,&
            D2UAPR_o_Dn_pon_no,D2UAPR_o_Dn_po2)

!     compute temperature derivatives of nucleons outside heavy nuclei
      CALL SKYRME_BULK_TEMPERATURE_DERIVATIVES(n_no, n_po, n_o, T, &
            P_o, F_o, S_o, E_o, Meff_no, Meff_po,&
            L_no, L_po, DM_no_Dn_no, DM_no_Dn_po, DM_po_Dn_no, DM_po_Dn_po, &
            DMeff_no_Dn_no, DMeff_no_Dn_po, DMeff_po_Dn_no, DMeff_po_Dn_po, &
            D2M_no_Dn_no2, D2M_no_Dn_non_po, D2M_no_Dn_po2, &
            D2M_po_Dn_no2, D2M_po_Dn_pon_no, D2M_po_Dn_po2, &
            Dtau_no_Dn_no, Dtau_no_Dn_po, Dtau_po_Dn_no, Dtau_po_Dn_po, &
            DMu_no_Dn_no, DMu_no_Dn_po, DMu_po_Dn_no, DMu_po_Dn_po, &
            DUAPR_o_Dn_no,DUAPR_o_Dn_po, &
            D2UAPR_o_Dn_no2,D2UAPR_o_Dn_non_po, &
            D2UAPR_o_Dn_pon_no,D2UAPR_o_Dn_po2, &
            DP_o_Dn_no, DP_o_Dn_po, DS_o_Dn_no, DS_o_Dn_po, &
            DE_o_Dn_no, DE_o_Dn_po, DF_o_Dn_no, DF_o_Dn_po, &
            eta_no, eta_po, tau_no, tau_po, &
            v_no, v_po, mu_no, mu_po, G_no, G_po, &
            Dtau_no_Deta_no, Dtau_no_Deta_po, Dtau_po_Deta_no, Dtau_po_Deta_po,&
            DMu_no_Deta_no, DMu_no_Deta_po, DMu_po_Deta_no, DMu_po_Deta_po, &
            DP_o_Deta_no, DP_o_Deta_po, DS_o_Deta_no, DS_o_Deta_po, &
            DE_o_Deta_no, DE_o_Deta_po, DF_o_Deta_no, DF_o_Deta_po, &
            Dn_no_DT, Dn_po_DT, Dtau_no_DT, Dtau_po_DT, Dmu_no_DT, Dmu_po_DT, &
            DP_o_DT, DS_o_DT, DE_o_DT, DF_o_DT, 0 )

!     compute density, eta, and T derivatives of alpha particles
      CALL ALPHA_DERIVATIVES  ( n_no, n_po, n_alpha, mu_alpha, T,            &
            P_alpha, S_alpha, E_alpha, F_alpha,                              &
            Dmu_no_Dn_no,   Dmu_po_Dn_no,   Dmu_no_Dn_po,   Dmu_po_Dn_po,    &
            Dmu_no_Deta_no, Dmu_po_Deta_no, Dmu_no_Deta_po, Dmu_po_Deta_po,  &
            Dmu_no_DT, Dmu_po_DT, DP_o_DT,                                   &
            Dn_no_Deta_no,  Dn_po_Deta_no,  Dn_no_Deta_po,  Dn_po_Deta_po,   &
            DP_o_Dn_no, DP_o_Dn_po, DP_o_Deta_no, DP_o_Deta_po,              &
            Dn_alpha_Dn_no, Dn_alpha_Dn_po, Dmu_alpha_Dn_no, Dmu_alpha_Dn_po,&
            Dn_alpha_Deta_no,  Dn_alpha_Deta_po,  Dn_alpha_DT,  &
            Dmu_alpha_Deta_no, Dmu_alpha_Deta_po, Dmu_alpha_DT, &
            DP_alpha_Deta_no,  DP_alpha_Deta_po,  DP_alpha_DT,  &
            DS_alpha_Deta_no,  DS_alpha_Deta_po,  DS_alpha_DT,  &
            DE_alpha_Deta_no,  DE_alpha_Deta_po,  DE_alpha_DT,  &
            DF_alpha_Deta_no,  DF_alpha_Deta_po,  DF_alpha_DT   )
    ENDIF

!   if looking only for uniform matter solutions then do this:
    IF (objective == 3) THEN
!     compute uniform matter derivatives w.r.t. n, T, and y
      CALL Find_Uniform_Matter_Derivatives ( n, T, Yp, n_no, n_po, n_alpha,  &
        Dn_no_Deta_no, Dn_po_Deta_no, Dn_no_Deta_po, Dn_po_Deta_po,          &
        Dn_no_DT, Dn_po_DT, Dn_alpha_Deta_no, Dn_alpha_Deta_po, Dn_alpha_DT, &
        Deta_no_Dn, Deta_po_Dn, Deta_no_DT, Deta_po_DT, Deta_no_Dy, Deta_po_Dy )

!      write (*,*) '--------------------------------------------'
!    write (*,"(10E15.5)") n_no, n_po, n_alpha, mu_alpha, T
!    write (*,"(10E15.5)") Dmu_no_Dn_no,   Dmu_po_Dn_no,   Dmu_no_Dn_po,   Dmu_po_Dn_po
!    write (*,"(10E15.5)") Dmu_no_Deta_no, Dmu_po_Deta_no, Dmu_no_Deta_po, Dmu_po_Deta_po
!    write (*,"(10E15.5)") Dv_no_Dn_no, Dv_no_Dn_po,     Dv_po_Dn_no, Dv_po_Dn_po
!    write (*,"(10E15.5)") Dmu_no_DT, Dmu_po_DT, DP_o_DT
!    write (*,"(10E15.5)") Deta_no_Dn-Yp/n*Deta_no_Dy, Deta_po_Dn-Yp/n*Deta_po_Dy 
!    write (*,"(10E15.5)") Dn_no_Deta_no,  Dn_po_Deta_no,  Dn_no_Deta_po,  Dn_po_Deta_po
!    write (*,"(10E15.5)") DP_o_Dn_no, DP_o_Dn_po, DP_o_Deta_no, DP_o_Deta_po
!    write (*,"(10E15.5)") Dn_alpha_Dn_no, Dn_alpha_Dn_po, Dmu_alpha_Dn_no, Dmu_alpha_Dn_po
!    write (*,"(10E15.5)") Dn_alpha_Deta_no, Dn_alpha_Deta_po
!    write (*,"(10E15.5)") Dmu_alpha_Deta_no, Dmu_alpha_Deta_po
!    write (*,"(10E15.5)") DP_alpha_Deta_no, DP_alpha_Deta_po
!    write (*,"(10E15.5)") Dn_alpha_DT, DP_alpha_DT, Dmu_alpha_DT
!      write (*,*) '--------------------------------------------'


!     set derivatives of heavy nuclei occupation volume w.r.t. n, T, and y to 0
      Du_DT = ZERO
      Du_Dn = ZERO
      Du_Dy = ZERO

!     combine properties of nucleons outside of heavy nuclei and
!     with those of alpha particles
      CALL OUTSIDE_PROPERTIES (u, T, &
            Du_DT, Du_Dn, Du_Dy, &
            Deta_no_DT, Deta_no_Dn, Deta_no_Dy,     &
            Deta_po_DT, Deta_po_Dn, Deta_po_Dy,     &
            Dmu_no_Deta_no, Dmu_no_Deta_po, Dmu_no_DT, &
            Dmu_po_Deta_no, Dmu_po_Deta_po, Dmu_po_DT, &
            P_o, DP_o_Deta_no, DP_o_Deta_po, DP_o_DT, &
            S_o, DS_o_Deta_no, DS_o_Deta_po, DS_o_DT, &
            E_o, DE_o_Deta_no, DE_o_Deta_po, DE_o_DT, &
            F_o, DF_o_Deta_no, DF_o_Deta_po, DF_o_DT, &
            n_alpha, Dn_alpha_Deta_no, Dn_alpha_Deta_po, Dn_alpha_DT, &
            P_alpha, DP_alpha_Deta_no, DP_alpha_Deta_po, DP_alpha_DT, &
            S_alpha, DS_alpha_Deta_no, DS_alpha_Deta_po, DS_alpha_DT, &
            E_alpha, DE_alpha_Deta_no, DE_alpha_Deta_po, DE_alpha_DT, &
            F_alpha, DF_alpha_Deta_no, DF_alpha_Deta_po, DF_alpha_DT, &
            F_out, DF_out_DT, DF_out_Dn, DF_out_Dy, &
            S_out, DS_out_DT, DS_out_Dn, DS_out_Dy, &
            Dmu_no_DT, Dmu_no_Dn, Dmu_no_Dy,  &
            Dmu_po_DT, Dmu_po_Dn, Dmu_po_Dy )

!     compute f, s, and e of outside nucleons + alphas
      F     = F_out
      S     = S_out
      E     = F + T*S

!     compute first derivatives of f, s, and e w.r.t. n, y, and T
      DF_Dn = DF_out_Dn
      DF_Dy = DF_out_Dy
      DF_DT = DF_out_DT

      DS_Dn = DS_out_Dn
      DS_Dy = DS_out_Dy
      DS_DT = DS_out_DT

      DE_Dn = DF_Dn + T*DS_Dn
      DE_Dy = DF_Dy + T*DS_Dy
      DE_DT = DF_DT + T*DS_DT + S

!     compute second derivatives of free energy f w.r.t. n, y, and T
      D2F_DT2  = - DS_DT
      D2F_DTDn = (ONE-Yp)*Dmu_no_DT + Yp*Dmu_po_DT
      D2F_DyDT = - n*(Dmu_no_DT-Dmu_po_DT)
      D2F_DnDy = - (mu_no-mu_po) - n*(Dmu_no_Dn-Dmu_po_Dn)
      D2F_Dn2  = (ONE-Yp)*Dmu_no_Dn + Yp*Dmu_po_Dn
      D2F_DY2  = - n*(Dmu_no_Dy - Dmu_po_Dy)

!     compute p and its derivatives w.r.t. n, y, and T
      P     = n*DF_Dn - F
      DP_Dn = n*D2F_DN2
      DP_Dy = n*(mu_no - mu_po + D2F_DNDy)
      DP_DT = S + n*D2F_DTDn

      u_out    = odu*exc_v_alpha

      F_alpha = odu*F_alpha
      F_o     = u_out*F_o
      F_i     = u*F_i

      RETURN
    ENDIF

!   if heavies present compute volume and density of nucleons outside heavies
    u_out    = odu*exc_v_alpha
    tot_n_o  =  n_o*exc_v_alpha + four*n_alpha
    tot_n_po = n_po*exc_v_alpha +  two*n_alpha

!   obtain properties of nucleons inside heavies
    n_i = (n-odu*tot_n_o)/u
    n_1  = n_i - tot_n_o
    y_i = (n*Yp-odu*tot_n_po)
    y_i = y_i/u/n_i
    n_2  = n_i*y_i-tot_n_po
    n_ni = n_i*(ONE-y_i)
    n_pi = n_i*(y_i)

!   check if inside neutron and proton densities smaller than outside densities
    IF (n_i < 1.05d0*n_o) THEN
      error = -3
      RETURN
    ENDIF

!   check if inside density too similar to total density
    IF (n_i < n*1.105D0) THEN
      error = -4
      RETURN
    ENDIF

!   check if proton fraction inside is outside (0,1) range
    IF (y_i <= 0.0D0 .OR. y_i >= ONE) THEN
      error = -5
      RETURN
    ENDIF

    log10_n_ni = log10(n_i*(one-y_i))
    log10_n_pi = log10(n_i*y_i)
!
!   compute properties of nucleons inside heavy nuclei
    CALL SKYRME_BULK_PROPERTIES(log10_n_ni, log10_n_pi, T, flag, &
        n_ni, n_pi, n_i, y_i, delta_i, &
        Meff_ni, Meff_pi, M_ni, M_pi, L_ni, L_pi, &
        DM_ni_Dn_ni, DM_ni_Dn_pi, DM_pi_Dn_ni, DM_pi_Dn_pi, &
        eta_ni, eta_pi, tau_ni, tau_pi, v_ni, v_pi, mu_ni, mu_pi,&
        g1_i, g2_i, f1_i, f2_i, UAPR_i, DUAPR_i_Dn_ni, DUAPR_i_Dn_pi,&
        P_i, F_i, S_i, E_i, high_density_in)

!   compute surface and translational free energy components
!   and their derivatives for heavy nuclei
    CALL Free_Energy_Heavy( objective, n_ni, n_pi, u, T, A, Z, rad, &
      F_SC, DF_SC_Du, DF_SC_Dn_i, DF_SC_DT, DF_SC_Dy_i, &
      D2F_SC_Du2, D2F_SC_DuDn_i, D2F_SC_DuDT, D2F_SC_DuDy_i, D2F_SC_Dn_i2, &
      D2F_SC_Dn_iDT, D2F_SC_Dn_iDy_i, D2F_SC_DT2, D2F_SC_DTDy_i, D2F_SC_Dy_i2, &
      F_TR, DF_TR_Du, DF_TR_Dn_i, DF_TR_DT, DF_TR_Dy_i, &
      D2F_TR_Du2, D2F_TR_DuDn_i, D2F_TR_DuDT, D2F_TR_DuDy_i, D2F_TR_Dn_i2, &
      D2F_TR_Dn_iDT, D2F_TR_Dn_iDy_i, D2F_TR_DT2, D2F_TR_DTDy_i, D2F_TR_Dy_i2 )

!   if a heavy nucleus is too small disregard it.
!   if F_TR or F_SC does not exist, disregard solution also
!    this happens whenever a heavy nuclei has
!    a very small proton fraction y_i (y_i ≲ 0.20)
!    or if the surface tension \sigma(y_i,T) == NaN
    IF (A < 1.0d1 .OR. A > 1.0D3) THEN
      error = -6
      RETURN
    ENDIF

    IF (n_i > 0.5d0) THEN
      error = -8
      RETURN
    ENDIF

    IF (ieee_is_nan(F_TR) .OR. ieee_is_nan(F_SC)) THEN
      error = -7
      RETURN
    ENDIF

!   get heavy nuclei density
    n_heavy = (n_ni+n_pi)*TEN**log10_u/A

!   ignore solutions with too few nucleons
!    though these types of solution can be found 
!    in such cases F_uniforn ≃ F_non_uniform
!    and it makes code very slow
!    IF (n_heavy < 0.01d0) THEN
!      error = -9
!      RETURN
!    ENDIF

!   correct F_alpha and F_o due to exluded heavy nuclei volume "u"
!    F_alpha = odu*F_alpha
!    F_o     = u_out*F_o
!    F_i     = u*F_i

!   if OBJECTIVE = 1 then use this subroutine
!   to obtain equations to be minimimized for non_uniform matter
    IF (objective==0) RETURN

    B(1) = DF_TR_Du - n_i/u*DF_TR_Dn_i + DF_SC_Du - n_i/u*DF_SC_Dn_i
    B(2) = y_i/u/n_i*(DF_TR_Dy_i - n_i/y_i*DF_TR_Dn_i) &
         + y_i/u/n_i*(DF_SC_Dy_i - n_i/y_i*DF_SC_Dn_i)
    B(3) = -(ONE-y_i)/u/n_i*(DF_TR_Dy_i + DF_SC_Dy_i) &
                    - ONE/u*(DF_TR_Dn_i + DF_SC_Dn_i)

    non_uniform_eq(1) = (P_i - P_o - P_alpha) - B(1)
    non_uniform_eq(2) = (mu_ni - mu_no) - B(2)
    non_uniform_eq(3) = (mu_pi - mu_po) - B(3)


!    write (*,"(3ES18.10,16ES13.5)") log10_n_no, log10_n_po, log10_u, &
!     B, P_i, P_o, P_alpha, mu_ni, mu_no, mu_pi, mu_po, &
!     non_uniform_eq, dot_product(non_uniform_eq,non_uniform_eq)

    IF (objective==1) THEN
      F_alpha = odu*F_alpha
      F_o     = u_out*F_o
      F_i     = u*F_i
      !write (*,*) F_alpha, F_o, F_i, F_SC, F_TR
      RETURN
    ENDIF

    Dy_i_Du = - (n_2-y_i*n_1)/u/n_i
    Dn_i_Du = - n_1/u

!   compute density derivatives of nucleons outside heavy nuclei
    CALL SKYRME_BULK_DENSITY_DERIVATIVES ( flag, &
          log10_n_no, log10_n_po, T, G_no, G_po, &
          Deta_no_Dn_no, Deta_no_Dn_po, Deta_po_Dn_no, Deta_po_Dn_po, &
          Dtau_no_Dn_no, Dtau_no_Dn_po, Dtau_po_Dn_no, Dtau_po_Dn_po, &
          Dmu_no_Dn_no, Dmu_no_Dn_po,   Dmu_po_Dn_no, Dmu_po_Dn_po,   &
          Dv_no_Dn_no, Dv_no_Dn_po,     Dv_po_Dn_no, Dv_po_Dn_po,     &
          DMeff_no_Dn_no, DMeff_no_Dn_po, DMeff_po_Dn_no, DMeff_po_Dn_po, &
          Dp_o_Dn_no, Dp_o_Dn_po, Ds_o_Dn_no, Ds_o_Dn_po, &
          De_o_Dn_no, De_o_Dn_po, Df_o_Dn_no, Df_o_Dn_po )

!
!   compute density, eta, and T derivatives of alpha particles
    CALL ALPHA_DERIVATIVES  ( n_no, n_po, n_alpha, mu_alpha, T,            &
          p_alpha, s_alpha, e_alpha, f_alpha,                              &
          Dmu_no_Dn_no,   Dmu_po_Dn_no,   Dmu_no_Dn_po,   Dmu_po_Dn_po,    &
          Dmu_no_Deta_no, Dmu_po_Deta_no, Dmu_no_Deta_po, Dmu_po_Deta_po,  &
          Dmu_no_DT, Dmu_po_DT, DP_o_DT,                                   &
          Dn_no_Deta_no,  Dn_po_Deta_no,  Dn_no_Deta_po,  Dn_po_Deta_po,   &
          DP_o_Dn_no, DP_o_Dn_po, DP_o_Deta_no, DP_o_Deta_po,              &
          Dn_alpha_Dn_no, Dn_alpha_Dn_po, Dmu_alpha_Dn_no, Dmu_alpha_Dn_po,&
          Dn_alpha_Deta_no,  Dn_alpha_Deta_po,  Dn_alpha_DT,  &
          Dmu_alpha_Deta_no, Dmu_alpha_Deta_po, Dmu_alpha_DT, &
          Dp_alpha_Deta_no,  Dp_alpha_Deta_po,  Dp_alpha_DT,  &
          Ds_alpha_Deta_no,  Ds_alpha_Deta_po,  Ds_alpha_DT,  &
          De_alpha_Deta_no,  De_alpha_Deta_po,  De_alpha_DT,  &
          Df_alpha_Deta_no,  Df_alpha_Deta_po,  Df_alpha_DT   )

!   compute density derivatives of nucleons inside heavy nuclei
    CALL SKYRME_BULK_DENSITY_DERIVATIVES ( flag, &
          log10_n_ni, log10_n_pi, T, G_ni, G_pi, &
          Deta_ni_Dn_ni, Deta_ni_Dn_pi, Deta_pi_Dn_ni, Deta_pi_Dn_pi, &
          Dtau_ni_Dn_ni, Dtau_ni_Dn_pi, Dtau_pi_Dn_ni, Dtau_pi_Dn_pi, &
          Dmu_ni_Dn_ni, Dmu_ni_Dn_pi,   Dmu_pi_Dn_ni, Dmu_pi_Dn_pi,   &
          Dv_ni_Dn_ni, Dv_ni_Dn_pi,     Dv_pi_Dn_ni, Dv_pi_Dn_pi,     &
          DMeff_ni_Dn_ni, DMeff_ni_Dn_pi, DMeff_pi_Dn_ni, DMeff_pi_Dn_pi, &
          DP_i_Dn_ni, DP_i_Dn_pi, DS_i_Dn_ni, DS_i_Dn_pi, &
          DE_i_Dn_ni, DE_i_Dn_pi, DF_i_Dn_ni, DF_i_Dn_pi)

!   derivatives of total "inside" nucleon density w.r.t. "outside" densities
    Dn_i_Dn_no = -odu/u*(exc_v_alpha + (FOUR-v_alpha*n_o)*Dn_alpha_Dn_no)
    Dn_i_Dn_po = -odu/u*(exc_v_alpha + (FOUR-v_alpha*n_o)*Dn_alpha_Dn_po)

!   derivatives of "inside" nucleon chemical pot. w.r.t. "inside" properties
    Dmu_ni_Dn_i = (ONE-y_i)*Dmu_ni_Dn_ni + y_i*Dmu_ni_Dn_pi
    Dmu_pi_Dn_i = (ONE-y_i)*Dmu_pi_Dn_ni + y_i*Dmu_pi_Dn_pi

!   derivatives of "inside" nucleon chemical pot. w.r.t. "inside" properties
    Dmu_ni_Dy_i = n_i*(Dmu_ni_Dn_pi - Dmu_ni_Dn_ni)
    Dmu_pi_Dy_i = n_i*(Dmu_pi_Dn_pi - Dmu_pi_Dn_ni)

!   derivatives of "inside" nucleon tau w.r.t. "inside" properties
    Dtau_ni_Dn_i = (ONE-y_i)*Dtau_ni_Dn_ni + y_i*Dtau_ni_Dn_pi
    Dtau_pi_Dn_i = (ONE-y_i)*Dtau_pi_Dn_ni + y_i*Dtau_pi_Dn_pi

!   derivatives of "inside" nucleon tau w.r.t. "inside" properties
    Dtau_ni_Dy_i = n_i*(Dtau_ni_Dn_pi - Dtau_ni_Dn_ni)
    Dtau_pi_Dy_i = n_i*(Dtau_pi_Dn_pi - Dtau_pi_Dn_ni)

!   derivatives of total "inside" proton fraction w.r.t. "outside" densities
    Dy_i_Dn_no = (TWO-v_alpha*n_po)*Dn_alpha_Dn_no
    Dy_i_Dn_no = - ( u*y_i*Dn_i_Dn_no + odu*Dy_i_Dn_no)/n_i/u

    Dy_i_Dn_po = exc_v_alpha + (TWO-v_alpha*n_po)*Dn_alpha_Dn_po
    Dy_i_Dn_po = - ( u*y_i*Dn_i_Dn_po + odu*Dy_i_Dn_po)/n_i/u

!   derivative of inside pressure w.rt. inside properties
    DP_i_Dn_i = (ONE-y_i)*DP_i_Dn_ni + y_i*DP_i_Dn_pi
    DP_i_Dy_i = n_i*(DP_i_Dn_pi - DP_i_Dn_ni)

!   derivative of inside pressure w.r.t. outside nucleon densities
    DP_i_Dn_no = DP_i_Dy_i*Dy_i_Dn_no
    DP_i_Dn_po = DP_i_Dy_i*Dy_i_Dn_po

!   partial derivatives of Ai_in w.r.t. independent variables
!   A1_in = P_in ; A2_in = mu_ni ; A3_in = mu_pi
    DA_in_Du(1)    = DP_i_Dn_i*Dn_i_Du    + DP_i_Dy_i*Dy_i_Du
    DA_in_Dn_no(1) = DP_i_Dn_i*Dn_i_Dn_no + DP_i_Dy_i*Dy_i_Dn_no
    DA_in_Dn_po(1) = DP_i_Dn_i*Dn_i_Dn_po + DP_i_Dy_i*Dy_i_Dn_po

    DA_in_Du(2)    = Dmu_ni_Dn_i*Dn_i_Du    + Dmu_ni_Dy_i*Dy_i_Du
    DA_in_Dn_no(2) = Dmu_ni_Dn_i*Dn_i_Dn_no + Dmu_ni_Dy_i*Dy_i_Dn_no
    DA_in_Dn_po(2) = Dmu_ni_Dn_i*Dn_i_Dn_po + Dmu_ni_Dy_i*Dy_i_Dn_po

    DA_in_Du(3)    = Dmu_pi_Dn_i*Dn_i_Du    + Dmu_pi_Dy_i*Dy_i_Du
    DA_in_Dn_no(3) = Dmu_pi_Dn_i*Dn_i_Dn_no + Dmu_pi_Dy_i*Dy_i_Dn_no
    DA_in_Dn_po(3) = Dmu_pi_Dn_i*Dn_i_Dn_po + Dmu_pi_Dy_i*Dy_i_Dn_po

!   partial derivatives of Ai_out w.r.t. independent variables
!   A1_out = P_out ; A2_out = mu_no ; A3_out = mu_po
    DA_out_Du(1)    = ZERO
    DA_out_Dn_no(1) = DP_o_Dn_no +  T*Dn_alpha_Dn_no
    DA_out_Dn_po(1) = DP_o_Dn_po +  T*Dn_alpha_Dn_po
!
    DA_out_Du(2)    = ZERO
    DA_out_Dn_no(2) = Dmu_no_Dn_no
    DA_out_Dn_po(2) = Dmu_no_Dn_po
!
    DA_out_Du(3)    = ZERO
    DA_out_Dn_no(3) = Dmu_po_Dn_no
    DA_out_Dn_po(3) = Dmu_po_Dn_po

!   partial derivatives of B_i w.r.t. independent variables
    DB_Dup(1)     = D2F_TR_Du2 - n_i/u*D2F_TR_DuDn_i &
                  + D2F_SC_Du2 - n_i/u*D2F_SC_DuDn_i &
                  + n_i/u/u*(DF_TR_Dn_i + DF_SC_Dn_i)
    DB_Dn_i(1)    = D2F_TR_DuDn_i + D2F_SC_DuDn_i &
                  - n_i/u*( D2F_TR_Dn_i2 + D2F_SC_Dn_i2 ) &
                  - ( DF_TR_Dn_i + DF_SC_Dn_i )/u
    DB_Dy_i(1)    = D2F_TR_DuDy_i - n_i/u*D2F_TR_Dn_iDy_i &
                  + D2F_SC_DuDy_i - n_i/u*D2F_SC_Dn_iDy_i
    DB_DT(1)      = D2F_TR_DuDT + D2F_SC_DuDT &
                  - n_i/u*( D2F_TR_Dn_iDT + D2F_SC_Dn_iDT )
!
    DB_Dup(2)     = - B(2)/u + (y_i/n_i*D2F_TR_DuDy_i - D2F_TR_DuDn_i)/u &
                             + (y_i/n_i*D2F_SC_DuDy_i - D2F_SC_DuDn_i)/u
    DB_Dn_i(2)    = y_i/u/n_i*( ( D2F_TR_Dn_iDy_i - n_i/y_i*D2F_TR_Dn_i2   &
                                + D2F_SC_Dn_iDy_i - n_i/y_i*D2F_SC_Dn_i2 ) &
                                - ( DF_TR_Dy_i + DF_SC_Dy_i )/n_i )
    DB_Dy_i(2)    = y_i/u/n_i*( ( D2F_TR_Dy_i2 - n_i/y_i*D2F_TR_Dn_iDy_i   &
                                + D2F_SC_Dy_i2 - n_i/y_i*D2F_SC_Dn_iDy_i ) &
                                + ( DF_TR_Dy_i + DF_SC_Dy_i )/y_i )
    DB_DT(2)      = y_i/u/n_i*( ( D2F_TR_DTDy_i - n_i/y_i*D2F_TR_Dn_iDT   &
                                + D2F_SC_DTDy_i - n_i/y_i*D2F_SC_Dn_iDT ) )
!
    DB_Dup(3)     = - (D2F_SC_DuDn_i + (ONE-y_i)*D2F_SC_DuDy_i/n_i)/u &
                    - (D2F_TR_DuDn_i + (ONE-y_i)*D2F_TR_DuDy_i/n_i)/u &
                    - B(3)/u
    DB_Dn_i(3)    = - (D2F_TR_Dn_i2 + D2F_SC_Dn_i2)/u &
                    - (ONE-y_i)/u/n_i*(D2F_TR_Dn_iDy_i + D2F_SC_Dn_iDy_i) &
                    + (ONE-y_i)/u/n_i/n_i*(DF_TR_Dy_i + DF_SC_Dy_i)
    DB_Dy_i(3)    = - (ONE-y_i)/u/n_i*(D2F_TR_Dy_i2 + D2F_SC_Dy_i2) &
                    - ONE/u*(D2F_TR_Dn_iDy_i + D2F_SC_Dn_iDy_i) &
                    + ONE/u/n_i*(DF_TR_Dy_i + DF_SC_Dy_i)
    DB_DT(3)      = - (D2F_SC_Dn_iDT + (ONE-y_i)*D2F_SC_DTDy_i/n_i)/u &
                    - (D2F_TR_Dn_iDT + (ONE-y_i)*D2F_TR_DTDy_i/n_i)/u

    DB_Du       = DB_Dup + DB_Dy_i*Dy_i_Du    + DB_Dn_i*Dn_i_Du
    DB_Dn_no    =          DB_Dy_i*Dy_i_Dn_no + DB_Dn_i*Dn_i_Dn_no
    DB_Dn_po    =          DB_Dy_i*Dy_i_Dn_po + DB_Dn_i*Dn_i_Dn_po

    IF (objective == 2 .OR. objective == 4) THEN
!     determine jacobian
      jac(1,1) = (DA_in_Dn_no(1) - DB_Dn_no(1) - DA_out_Dn_no(1))*n_no*Log(ten)
      jac(1,2) = (DA_in_Dn_po(1) - DB_Dn_po(1) - DA_out_Dn_po(1))*n_po*Log(ten)
      jac(1,3) = (DA_in_Du(1)    - DB_Du(1)    - DA_out_Du(1)   )*u   *Log(ten)

      jac(2,1) = (DA_in_Dn_no(2) - DB_Dn_no(2) - DA_out_Dn_no(2))*n_no*Log(ten)
      jac(2,2) = (DA_in_Dn_po(2) - DB_Dn_po(2) - DA_out_Dn_po(2))*n_po*Log(ten)
      jac(2,3) = (DA_in_Du(2)    - DB_Du(2)    - DA_out_Du(2)   )*u   *Log(ten)

      jac(3,1) = (DA_in_Dn_no(3) - DB_Dn_no(3) - DA_out_Dn_no(3))*n_no*Log(ten)
      jac(3,2) = (DA_in_Dn_po(3) - DB_Dn_po(3) - DA_out_Dn_po(3))*n_po*Log(ten)
      jac(3,3) = (DA_in_Du(3)    - DB_Du(3)    - DA_out_Du(3)   )*u   *Log(ten)

      non_uniform_jac = jac

      IF (objective == 2) THEN
        F_alpha = odu*F_alpha
        F_o     = u_out*F_o
        F_i     = u*F_i

        RETURN
      ENDIF
    ENDIF

    CALL SKYRME_EFFECTIVE_MASS_DENSITY_DERIVATIVES(n_i, y_i, &
          n_ni, n_pi, Meff_ni, Meff_pi, M_ni, M_pi, &
          DM_ni_Dn_ni, DM_ni_Dn_pi, DM_pi_Dn_ni, DM_pi_Dn_pi, &
          DMeff_ni_Dn_ni, DMeff_ni_Dn_pi, DMeff_pi_Dn_ni, DMeff_pi_Dn_pi, &
          DMeff_ni_Dni, DMeff_pi_Dni, DMeff_ni_Dyi, DMeff_pi_Dyi, &
          D2M_ni_Dn_ni2, D2M_ni_Dn_nin_pi, D2M_ni_Dn_pi2, &
          D2M_pi_Dn_ni2, D2M_pi_Dn_pin_ni, D2M_pi_Dn_pi2)
!
    CALL SKYRME_POTENTIAL_DENSITY_DERIVATIVES(high_density_in,&
          n_i,delta_i,n_ni,n_pi,g1_i,g2_i,f1_i,f2_i, &
          UAPR_i,DUAPR_i_Dn_ni,DUAPR_i_Dn_pi,h1_i,h2_i,&
          D2UAPR_i_Dn_ni2,D2UAPR_i_Dn_nin_pi,&
          D2UAPR_i_Dn_pin_ni,D2UAPR_i_Dn_pi2)

!     compute temperature derivatives of nucleons inside heavy nuclei
    CALL SKYRME_BULK_TEMPERATURE_DERIVATIVES(n_ni, n_pi, n_i, T, &
          P_i, F_i, S_i, E_i, Meff_ni, Meff_pi, &
          L_ni, L_pi, DM_ni_Dn_ni, DM_ni_Dn_pi, DM_pi_Dn_ni, DM_pi_Dn_pi, &
          DMeff_ni_Dn_ni, DMeff_ni_Dn_pi, DMeff_pi_Dn_ni, DMeff_pi_Dn_pi, &
          D2M_ni_Dn_ni2, D2M_ni_Dn_nin_pi, D2M_ni_Dn_pi2, &
          D2M_pi_Dn_ni2, D2M_pi_Dn_pin_ni, D2M_pi_Dn_pi2, &
          Dtau_ni_Dn_ni, Dtau_ni_Dn_pi, Dtau_pi_Dn_ni, Dtau_pi_Dn_pi, &
          DMu_ni_Dn_ni, DMu_ni_Dn_pi, DMu_pi_Dn_ni, DMu_pi_Dn_pi, &
          DUAPR_i_Dn_ni, DUAPR_i_Dn_pi, &
          D2UAPR_i_Dn_ni2, D2UAPR_i_Dn_nin_pi,&
          D2UAPR_i_Dn_pin_ni, D2UAPR_i_Dn_pi2,&
          DP_i_Dn_ni, DP_i_Dn_pi, DS_i_Dn_ni, DS_i_Dn_pi, &
          DE_i_Dn_ni, DE_i_Dn_pi, DF_i_Dn_ni, DF_i_Dn_pi, &
          eta_ni, eta_pi, tau_ni, tau_pi, &
          v_ni, v_pi, mu_ni, mu_pi, G_ni, G_pi, &
          Dtau_ni_Deta_ni, Dtau_ni_Deta_pi, Dtau_pi_Deta_ni, Dtau_pi_Deta_pi,&
          DMu_ni_Deta_ni, DMu_ni_Deta_pi, DMu_pi_Deta_ni, DMu_pi_Deta_pi, &
          DP_i_Deta_n_i, DP_i_Deta_p_i, DS_i_Deta_i_n, DS_i_Deta_p_i, &
          DE_i_Deta_n_i, DE_i_Deta_p_i, DF_i_Deta_i_n, DF_i_Deta_p_i, &
          Dn_ni_DT, Dn_pi_DT, Dtau_ni_DT, Dtau_pi_DT, Dmu_ni_DT, Dmu_pi_DT, &
          DP_i_DT, DS_i_DT, DE_i_DT, DF_i_DT, 1 )

!   compute non-uniform matter derivatives w.r.t. n, T, and y
    CALL Find_Non_Uniform_Matter_Derivatives ( n, Yp, T, &
          u, n_ni, n_pi, n_i, y_i, n_no, n_po, n_o, n_alpha, &
          Dn_no_Deta_no,  Dn_po_Deta_no,  Dn_no_Deta_po,  Dn_po_Deta_po, &
          Dn_no_DT, Dn_po_DT, &
          Dmu_no_Deta_no, Dmu_po_Deta_no, Dmu_no_Deta_po, Dmu_po_Deta_po, &
          Dmu_no_DT, Dmu_po_DT, &
          DP_o_Deta_no, DP_o_Deta_po, DP_o_DT, &
          Dn_alpha_Deta_no, Dn_alpha_Deta_po, Dn_alpha_DT, &
          DP_alpha_Deta_no, DP_alpha_Deta_po, DP_alpha_DT, &
          Dmu_ni_Dn_ni, Dmu_pi_Dn_ni, Dmu_ni_Dn_pi, Dmu_pi_Dn_pi, &
          Dmu_ni_DT, Dmu_pi_DT, DP_i_Dn_ni, DP_i_Dn_pi, DP_i_DT, &
          DB_Dup(1), DB_Dn_i(1), DB_Dy_i(1), DB_DT(1), &
          DB_Dup(2), DB_Dn_i(2), DB_Dy_i(2), DB_DT(2), &
          DB_Dup(3), DB_Dn_i(3), DB_Dy_i(3), DB_DT(3), &
          DU_DN, Dy_i_DN, Dn_i_DN, Deta_no_DN, Deta_po_DN, &
          DU_DT, Dy_i_DT, Dn_i_DT, Deta_no_DT, Deta_po_DT, &
          DU_DY, Dy_i_DY, Dn_i_DY, Deta_no_DY, Deta_po_DY )

!     combine properties of nucleons outside of heavy nuclei and
!     with those of alpha particles
!    F_o = F_o/u_out
!    F_alpha = F_alpha/odu

    CALL OUTSIDE_PROPERTIES(u, T, &
          Du_DT, Du_Dn, Du_Dy, &
          Deta_no_DT, Deta_no_Dn, Deta_no_Dy,     &
          Deta_po_DT, Deta_po_Dn, Deta_po_Dy,     &
          Dmu_no_Deta_no, Dmu_no_Deta_po, Dmu_no_DT, &
          Dmu_po_Deta_no, Dmu_po_Deta_po, Dmu_po_DT, &
          p_o, Dp_o_Deta_no, Dp_o_Deta_po, Dp_o_DT, &
          s_o, Ds_o_Deta_no, Ds_o_Deta_po, Ds_o_DT, &
          e_o, De_o_Deta_no, De_o_Deta_po, De_o_DT, &
          f_o, Df_o_Deta_no, Df_o_Deta_po, Df_o_DT, &
          n_alpha, Dn_alpha_Deta_no, Dn_alpha_Deta_po, Dn_alpha_DT, &
          p_alpha, Dp_alpha_Deta_no, Dp_alpha_Deta_po, Dp_alpha_DT, &
          s_alpha, Ds_alpha_Deta_no, Ds_alpha_Deta_po, Ds_alpha_DT, &
          e_alpha, De_alpha_Deta_no, De_alpha_Deta_po, De_alpha_DT, &
          f_alpha, Df_alpha_Deta_no, Df_alpha_Deta_po, Df_alpha_DT, &
          F_out, DF_out_DT, DF_out_Dn, DF_out_Dy, &
          S_out, DS_out_DT, DS_out_Dn, DS_out_Dy, &
          Dmu_no_DT, Dmu_no_Dn, Dmu_no_Dy,  &
          Dmu_po_DT, Dmu_po_Dn, Dmu_po_Dy )

!     correct F_o for output
!    F_o = F_o*u_out
!    F_alpha = F_alpha*odu

!   correct F_i for heavy subroutine
!    F_i = F_i/u

    CALL HEAVY_PROPERTIES (n_i, y_i, u, T,     &
      Du_DT, Du_Dn, Du_Dy,           &
      Dn_i_DT, Dn_i_Dn, Dn_i_Dy,     &
      Dy_i_DT, Dy_i_Dn, Dy_i_Dy,     &
      F_i, DF_i_Dn_ni, DF_i_Dn_pi, DF_i_DT, &
      E_i, DE_i_Dn_ni, DE_i_Dn_pi, DE_i_DT, &
      S_i, DS_i_Dn_ni, DS_i_Dn_pi, DS_i_DT, &
      F_SC, DF_SC_Du, DF_SC_Dn_i, DF_SC_DT, DF_SC_Dy_i,                        &
      D2F_SC_Du2, D2F_SC_DuDn_i, D2F_SC_DuDT, D2F_SC_DuDy_i, D2F_SC_Dn_i2,     &
      D2F_SC_Dn_iDT, D2F_SC_Dn_iDy_i, D2F_SC_DT2, D2F_SC_DTDy_i, D2F_SC_Dy_i2, &
      F_TR, DF_TR_Du, DF_TR_Dn_i, DF_TR_DT, DF_TR_Dy_i,                        &
      D2F_TR_Du2, D2F_TR_DuDn_i, D2F_TR_DuDT, D2F_TR_DuDy_i, D2F_TR_Dn_i2,     &
      D2F_TR_Dn_iDT, D2F_TR_Dn_iDy_i, D2F_TR_DT2, D2F_TR_DTDy_i, D2F_TR_Dy_i2, &
      F_h, DF_h_DT, DF_h_Dn, DF_h_Dy, &
      S_h, DS_h_DT, DS_h_Dn, DS_h_Dy )

!     correct F_i for output
!      F_i = F_i*u

      F     = F_h + F_out
      S     = S_h + S_out
      E     = F   + T*S

      DF_Dn = DF_h_Dn + DF_out_Dn
      DF_Dy = DF_h_Dy + DF_out_Dy
      DF_DT = DF_h_DT + DF_out_DT

      DS_Dn = DS_h_Dn + DS_out_Dn
      DS_Dy = DS_h_Dy + DS_out_Dy
      DS_DT = DS_h_DT + DS_out_DT

      DE_Dn = DF_Dn + T*DS_Dn
      DE_Dy = DF_Dy + T*DS_Dy
      DE_DT = DF_DT + T*DS_DT + S

      D2F_DT2  = - DS_DT
      D2F_DTDn = (ONE-Yp)*Dmu_no_DT + Yp*Dmu_po_DT
      D2F_DyDT = - n*(Dmu_no_DT-Dmu_po_DT)
      D2F_DnDy = - (mu_no - mu_po) - n*(Dmu_no_Dn - Dmu_po_Dn)
      D2F_Dn2  = (ONE-Yp)*Dmu_no_Dn + Yp*Dmu_po_Dn
      D2F_DY2  = - n*(Dmu_no_Dy - Dmu_po_Dy)

      P     = n*DF_Dn - F
      DP_Dn = n*D2F_Dn2
      DP_Dy = n*(mu_no - mu_po + D2F_DnDy)
      DP_DT = S + n*D2F_DTDn

      ! get x_nu derivatives w.r.t. n and T
      dLog10_n_no_dn = ONE/G_no*Deta_no_Dn/LOG(TEN)
      dLog10_n_po_dn = ONE/G_po*Deta_po_Dn/LOG(TEN)
      dLog10_u_dn    = DU_DN/u/LOG(TEN)
      dLog10_n_no_dT = (R_3_2/T + ONE/G_no*Deta_no_DT)/LOG(TEN)
      dLog10_n_po_dT = (R_3_2/T + ONE/G_po*Deta_po_DT)/LOG(TEN)
      dLog10_u_dT    = DU_DT/u/LOG(TEN)


      F_alpha = odu*F_alpha
      F_o     = u_out*F_o
      F_i     = u*F_i

      !write (*,*) F_alpha, F_o, F_i, F_SC, F_TR

  END SUBROUTINE FREE_ENERGY

END MODULE
