!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Outside_Properties_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE OUTSIDE_PROPERTIES(u, T, &
    Du_DT, Du_Dn, Du_Dy, &
    Deta_no_DT, Deta_no_Dn, Deta_no_Dy,     &
    Deta_po_DT, Deta_po_Dn, Deta_po_Dy,     &
    Dmu_no_Deta_no, Dmu_no_Deta_po, Dmu_no_DT, &
    Dmu_po_Deta_no, Dmu_po_Deta_po, Dmu_po_DT, &
    p_o, Dp_o_Deta_no, Dp_o_Deta_po, Dp_o_DT, &
    s_o, Ds_o_Deta_no, Ds_o_Deta_po, Ds_o_DT, &
    e_o, De_o_Deta_no, De_o_Deta_po, De_o_DT, &
    f_o, Df_o_Deta_no, Df_o_Deta_po, Df_o_DT, &
    n_alpha, Dn_alpha_Deta_no, Dn_alpha_Deta_po, Dn_alpha_DT, &
    p_alpha, Dp_alpha_Deta_no, Dp_alpha_Deta_po, Dp_alpha_DT, &
    s_alpha, Ds_alpha_Deta_no, Ds_alpha_Deta_po, Ds_alpha_DT, &
    e_alpha, De_alpha_Deta_no, De_alpha_Deta_po, De_alpha_DT, &
    f_alpha, Df_alpha_Deta_no, Df_alpha_Deta_po, Df_alpha_DT, &
    F_out, DF_out_DT, DF_out_Dn, DF_out_Dy, &
    S_out, DS_out_DT, DS_out_Dn, DS_out_Dy, &
    Dmu_nout_DT, Dmu_nout_Dn, Dmu_nout_Dy,  &
    Dmu_pout_DT, Dmu_pout_Dn, Dmu_pout_Dy )

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : ONE, TWO, FOUR, R_3_2, &
        v_alpha, b_alpha, HC2 => Hbarc_Square, Neut_Prot_Mass_Diff

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: u, T
    REAL(DP), INTENT(IN) :: Du_DT, Du_Dn, Du_Dy
    REAL(DP), INTENT(IN) :: Deta_no_DT, Deta_no_Dn, Deta_no_Dy
    REAL(DP), INTENT(IN) :: Deta_po_DT, Deta_po_Dn, Deta_po_Dy
    REAL(DP), INTENT(IN) :: Dmu_no_Deta_no, Dmu_no_Deta_po, Dmu_no_DT
    REAL(DP), INTENT(IN) :: Dmu_po_Deta_no, Dmu_po_Deta_po, Dmu_po_DT

    REAL(DP), INTENT(IN) :: p_o, Dp_o_Deta_no, Dp_o_Deta_po, Dp_o_DT
    REAL(DP), INTENT(IN) :: s_o, Ds_o_Deta_no, Ds_o_Deta_po, Ds_o_DT
    REAL(DP), INTENT(IN) :: e_o, De_o_Deta_no, De_o_Deta_po, De_o_DT
    REAL(DP), INTENT(IN) :: f_o, Df_o_Deta_no, Df_o_Deta_po, Df_o_DT
    REAL(DP), INTENT(IN) :: n_alpha, Dn_alpha_DT
    REAL(DP), INTENT(IN) :: Dn_alpha_Deta_no, Dn_alpha_Deta_po
    REAL(DP), INTENT(IN) :: p_alpha, Dp_alpha_DT
    REAL(DP), INTENT(IN) :: Dp_alpha_Deta_no, Dp_alpha_Deta_po
    REAL(DP), INTENT(IN) :: s_alpha, Ds_alpha_DT
    REAL(DP), INTENT(IN) :: Ds_alpha_Deta_no, Ds_alpha_Deta_po
    REAL(DP), INTENT(IN) :: e_alpha, De_alpha_DT
    REAL(DP), INTENT(IN) :: De_alpha_Deta_no, De_alpha_Deta_po
    REAL(DP), INTENT(IN) :: f_alpha, Df_alpha_DT
    REAL(DP), INTENT(IN) :: Df_alpha_Deta_no, Df_alpha_Deta_po

    REAL(DP), INTENT(OUT) :: F_out, DF_out_DT, DF_out_Dn, DF_out_Dy
    REAL(DP), INTENT(OUT) :: S_out, DS_out_DT, DS_out_Dn, DS_out_Dy
    REAL(DP), INTENT(OUT) :: Dmu_nout_DT, Dmu_nout_Dn, Dmu_nout_Dy
    REAL(DP), INTENT(OUT) :: Dmu_pout_DT, Dmu_pout_Dn, Dmu_pout_Dy

    REAL(DP) :: odu, exc_v_alpha, u_out
    REAL(DP) :: Dn_a_DT, Dn_a_Dn, Dn_a_Dy
    REAL(DP) :: DF_a_DT, DF_a_Dn, DF_a_Dy
    REAL(DP) :: DF_u_DT, DF_u_Dn, DF_u_Dy
    REAL(DP) :: DS_a_DT, DS_a_Dn, DS_a_Dy
    REAL(DP) :: DS_u_DT, DS_u_Dn, DS_u_Dy

!   write (*,*) u, T
!   write (*,*) Du_DT, Du_Dn, Du_Dy
!   write (*,*) Deta_no_DT, Deta_no_Dn, Deta_no_Dy
!   write (*,*) Deta_po_DT, Deta_po_Dn, Deta_po_Dy
!   write (*,*) Dmu_no_Deta_no, Dmu_no_Deta_po, Dmu_no_DT
!   write (*,*) Dmu_po_Deta_no, Dmu_po_Deta_po, Dmu_po_DT
!   write (*,*) p_o, Dp_o_Deta_no, Dp_o_Deta_po, Dp_o_DT
!   write (*,*) s_o, Ds_o_Deta_no, Ds_o_Deta_po, Ds_o_DT
!   write (*,*) e_o, De_o_Deta_no, De_o_Deta_po, De_o_DT
!   write (*,*) f_o, Df_o_Deta_no, Df_o_Deta_po, Df_o_DT
!   write (*,*) n_alpha, Dn_alpha_Deta_no, Dn_alpha_Deta_po, Dn_alpha_DT
!   write (*,*) p_alpha, Dp_alpha_Deta_no, Dp_alpha_Deta_po, Dp_alpha_DT
!   write (*,*) s_alpha, Ds_alpha_Deta_no, Ds_alpha_Deta_po, Ds_alpha_DT
!   write (*,*) e_alpha, De_alpha_Deta_no, De_alpha_Deta_po, De_alpha_DT
!   write (*,*) f_alpha, Df_alpha_Deta_no, Df_alpha_Deta_po, Df_alpha_DT

    odu = one - u
    exc_v_alpha = one - v_alpha*n_alpha
    u_out = odu*exc_v_alpha

!   total alpha particle free energy and entropy
!   P_out = odu*(f_alpha + exc_v_alpha*f_o)
    S_out = odu*(S_alpha + exc_v_alpha*S_o)
!   E_out = odu*(e_alpha + exc_v_alpha*e_o)
    F_out = odu*(F_alpha + exc_v_alpha*F_o)

!   total free energy first derivatives w.r.t. (T,n,y)
    Dn_a_DT = Dn_alpha_DT + Dn_alpha_Deta_no*Deta_no_DT &
                          + Dn_alpha_Deta_po*Deta_po_DT

    DF_a_DT = DF_alpha_DT + DF_alpha_Deta_no*Deta_no_DT &
                          + DF_alpha_Deta_po*Deta_po_DT

    DF_u_DT = DF_o_DT     + DF_o_Deta_no*Deta_no_DT &
                          + DF_o_Deta_po*Deta_po_DT

    DS_a_DT = DS_alpha_DT + DS_alpha_Deta_no*Deta_no_DT &
                          + DS_alpha_Deta_po*Deta_po_DT

    DS_u_DT = DS_o_DT     + DS_o_Deta_no*Deta_no_DT &
                          + DS_o_Deta_po*Deta_po_DT

    DF_out_DT = - Du_DT*F_out/odu - odu*v_alpha*Dn_a_DT*F_o &
                + odu*DF_a_DT + u_out*DF_u_DT

    DS_out_DT = - Du_DT*S_out/odu - odu*v_alpha*Dn_a_DT*S_o &
                + odu*DS_a_DT + u_out*DS_u_DT

    Dn_a_Dn = Dn_alpha_Deta_no*Deta_no_Dn + Dn_alpha_Deta_po*Deta_po_Dn

    DF_a_Dn = DF_alpha_Deta_no*Deta_no_Dn + DF_alpha_Deta_po*Deta_po_Dn
    DF_u_Dn = DF_o_Deta_no*Deta_no_Dn + DF_o_Deta_po*Deta_po_Dn

    DS_a_Dn = DS_alpha_Deta_no*Deta_no_Dn + DS_alpha_Deta_po*Deta_po_Dn
    DS_u_Dn = DS_o_Deta_no*Deta_no_Dn + DS_o_Deta_po*Deta_po_Dn

    DF_out_Dn = - Du_Dn*F_out/odu - odu*v_alpha*Dn_a_Dn*F_o &
                + odu*DF_a_Dn + u_out*DF_u_Dn

    DS_out_Dn = - Du_Dn*S_out/odu - odu*v_alpha*Dn_a_Dn*S_o &
                + odu*DS_a_Dn + u_out*DS_u_Dn

    Dn_a_Dy = Dn_alpha_Deta_no*Deta_no_Dy + Dn_alpha_Deta_po*Deta_po_Dy

    DF_a_Dy = DF_alpha_Deta_no*Deta_no_Dy + DF_alpha_Deta_po*Deta_po_Dy
    DF_u_Dy = DF_o_Deta_no*Deta_no_Dy + DF_o_Deta_po*Deta_po_Dy

    DS_a_Dy = DS_alpha_Deta_no*Deta_no_Dy + DS_alpha_Deta_po*Deta_po_Dy
    DS_u_Dy = DS_o_Deta_no*Deta_no_Dy + DS_o_Deta_po*Deta_po_Dy

    DF_out_Dy = - Du_Dy*F_out/odu - odu*v_alpha*Dn_a_Dy*F_o &
                + odu*DF_a_Dy + u_out*DF_u_Dy

    DS_out_Dy = - Du_Dy*S_out/odu - odu*v_alpha*Dn_a_Dy*S_o &
                + odu*DS_a_Dy + u_out*DS_u_Dy

!   chemical potential first derivatives w.r.t. (T,n,y)
    Dmu_nout_DN = Dmu_no_Deta_no*Deta_no_DN + Dmu_no_Deta_po*Deta_po_DN
    Dmu_pout_DN = Dmu_po_Deta_no*Deta_no_DN + Dmu_po_Deta_po*Deta_po_DN

    Dmu_nout_DY = Dmu_no_Deta_no*Deta_no_DY + Dmu_no_Deta_po*Deta_po_DY
    Dmu_pout_DY = Dmu_po_Deta_no*Deta_no_DY + Dmu_po_Deta_po*Deta_po_DY

    Dmu_nout_DT = Dmu_no_Deta_no*Deta_no_DT + Dmu_no_Deta_po*Deta_po_DT &
                + Dmu_no_DT
    Dmu_pout_DT = Dmu_po_Deta_no*Deta_no_DT + Dmu_po_Deta_po*Deta_po_DT &
                + Dmu_po_DT

  END SUBROUTINE OUTSIDE_PROPERTIES

END MODULE Outside_Properties_Mod
