!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Alpha_Properties_Mod

  IMPLICIT NONE

CONTAINS

  SUBROUTINE ALPHA_PROPERTIES (objective, n_n, n_p, mu_n, mu_p, T, F_o, P_o, &
    n_alpha, mu_alpha, p_alpha, s_alpha, e_alpha, f_alpha)

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, &
        ONLY : ZERO, ONE, TWO, THREE, FOUR, R_3_2, R_5_2, &
               v_alpha, b_alpha, Hbarc_Square, PI, Mass_n

    IMPLICIT NONE

    INTEGER(I4B), INTENT(IN) :: objective
    REAL(DP), INTENT(IN)  :: n_n, n_p, mu_n, mu_p, T, F_o, P_o
    REAL(DP), INTENT(OUT) :: n_alpha, mu_alpha
    REAL(DP), INTENT(OUT) :: p_alpha, s_alpha
    REAL(DP), INTENT(OUT) :: e_alpha, f_alpha
    REAL(DP) :: mass_Q, mu_over_T

!   alpha particle's properties
    mass_Q = Mass_n*T/TWO/PI/Hbarc_Square
    mass_Q = mass_Q**R_3_2

    mu_alpha = TWO*(mu_n+mu_p) + b_alpha - P_o*v_alpha
    mu_over_T = mu_alpha/T

!   check if alpha particle chemical potential too large/small
    IF (mu_alpha>ZERO) mu_over_T = MIN(mu_over_T, 2.d2)
    IF (mu_alpha<ZERO) mu_over_T = MAX(mu_over_T,-2.d2)

!   density of alpha particles
    n_alpha = 8.D0*mass_Q*EXP(mu_over_T)

!   alpha particles pressure
    p_alpha = n_alpha*T

!   alpha particles entropy
    S_alpha = (R_5_2 - mu_over_T)*n_alpha

!   alpha particles energy
    E_alpha = (R_3_2*T - b_alpha)*n_alpha

!   alpha particles free energy
    F_alpha = n_alpha*(mu_alpha-b_alpha-T)

    IF (objective == 0) RETURN

  END SUBROUTINE ALPHA_PROPERTIES

  SUBROUTINE ALPHA_DERIVATIVES ( n_n, n_p, n_alpha, mu_alpha, T,&
      P_alpha, S_alpha, E_alpha, F_alpha,                       &
      Dmu_n_Dn_n,   Dmu_p_Dn_n,   Dmu_n_Dn_p,   Dmu_p_Dn_p,     &
      Dmu_n_Deta_n, Dmu_p_Deta_n, Dmu_n_Deta_p, Dmu_p_Deta_p,   &
      Dmu_n_DT, Dmu_p_DT, DP_DT,                                &
      Dn_n_Deta_n,  Dn_p_Deta_n,  Dn_n_Deta_p,  Dn_p_Deta_p,    &
      DP_Dn_n, DP_Dn_p, DP_Deta_n, DP_Deta_p,                   &
      Dn_alpha_Dn_n,  Dn_alpha_Dn_p,                            &
      Dmu_alpha_Dn_n, Dmu_alpha_Dn_p,                           &
      Dn_alpha_Deta_n, Dn_alpha_Deta_p, Dn_alpha_DT,            &
      Dmu_alpha_Deta_n, Dmu_alpha_Deta_p, Dmu_alpha_DT,         &
      Dp_alpha_Deta_n, Dp_alpha_Deta_p, Dp_alpha_DT,            &
      Ds_alpha_Deta_n, Ds_alpha_Deta_p, Ds_alpha_DT,            &
      De_alpha_Deta_n, De_alpha_Deta_p, De_alpha_DT,            &
      Df_alpha_Deta_n, Df_alpha_Deta_p, Df_alpha_DT )

    USE Kind_Types_Mod, ONLY : DP, I4B
    USE Physical_Constants_Mod, ONLY : ZERO, ONE, TWO, V_ALPHA, R_3_2, R_5_2

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: n_n, n_p, n_alpha, mu_alpha, T
    REAL(DP), INTENT(IN) :: P_alpha, S_alpha, E_alpha, F_alpha
    REAL(DP), INTENT(IN) :: Dmu_n_Dn_n, Dmu_p_Dn_n
    REAL(DP), INTENT(IN) :: Dmu_n_Dn_p, Dmu_p_Dn_p
    REAL(DP), INTENT(IN) :: Dmu_n_Deta_n, Dmu_p_Deta_n
    REAL(DP), INTENT(IN) :: Dmu_n_Deta_p, Dmu_p_Deta_p
    REAL(DP), INTENT(IN) :: Dp_Dn_n, Dp_Dn_p
    REAL(DP), INTENT(IN) :: Dn_n_Deta_n, Dn_p_Deta_n
    REAL(DP), INTENT(IN) :: Dn_n_Deta_p, Dn_p_Deta_p
    REAL(DP), INTENT(IN) :: Dp_Deta_n, Dp_Deta_p

    REAL(DP), INTENT(IN) :: Dmu_n_DT, Dmu_p_DT
    REAL(DP), INTENT(IN) :: DP_DT

    REAL(DP), INTENT(OUT) :: Dn_alpha_Dn_n, Dn_alpha_Dn_p
    REAL(DP), INTENT(OUT) :: Dmu_alpha_Dn_n, Dmu_alpha_Dn_p
    REAL(DP), INTENT(OUT) :: Dn_alpha_Deta_n, Dn_alpha_Deta_p
    REAL(DP), INTENT(OUT) :: Dmu_alpha_Deta_n, Dmu_alpha_Deta_p
    REAL(DP), INTENT(OUT) :: Dn_alpha_DT, Dmu_alpha_DT
    REAL(DP), INTENT(OUT) :: DP_alpha_DT, DS_alpha_DT
    REAL(DP), INTENT(OUT) :: DE_alpha_DT, DF_alpha_DT
    REAL(DP), INTENT(OUT) :: DP_alpha_Deta_n, DP_alpha_Deta_p
    REAL(DP), INTENT(OUT) :: DS_alpha_Deta_n, DS_alpha_Deta_p
    REAL(DP), INTENT(OUT) :: DE_alpha_Deta_n, DE_alpha_Deta_p
    REAL(DP), INTENT(OUT) :: DF_alpha_Deta_n, DF_alpha_Deta_p

    REAL(DP) :: DP_alpha_Dn_n, DP_alpha_Dn_p, mu_over_T

    IF (n_alpha == zero) THEN
      Dn_alpha_Dn_n  = ZERO
      Dn_alpha_Dn_p  = ZERO
      Dmu_alpha_Dn_n = ZERO
      Dmu_alpha_Dn_p = ZERO
      DP_alpha_Dn_n  = ZERO
      DP_alpha_Dn_p  = ZERO
      Dn_alpha_Deta_n  = ZERO
      Dn_alpha_Deta_p  = ZERO
      Dmu_alpha_Deta_n = ZERO
      Dmu_alpha_Deta_p = ZERO
      Dp_alpha_Deta_n  = ZERO
      Dp_alpha_Deta_p  = ZERO
      Ds_alpha_Deta_n  = ZERO
      Ds_alpha_Deta_p  = ZERO
      De_alpha_Deta_n  = ZERO
      De_alpha_Deta_p  = ZERO
      Df_alpha_Deta_n  = ZERO
      Df_alpha_Deta_p  = ZERO
      Dn_alpha_DT  = ZERO
      Dmu_alpha_DT = ZERO
      Dp_alpha_DT  = ZERO
      Ds_alpha_DT  = ZERO
      De_alpha_DT  = ZERO
      Df_alpha_DT  = ZERO
      RETURN
    ENDIF

    mu_over_T = mu_alpha/T

!   derivatives of alpha particle pressure w.r.t. nucleon densities
    Dmu_alpha_Dn_n = two*(Dmu_n_Dn_n+Dmu_p_Dn_n) - DP_Dn_n*v_alpha
    Dmu_alpha_Dn_p = two*(Dmu_n_Dn_p+Dmu_p_Dn_p) - DP_Dn_p*v_alpha

!   derivatives of alpha particle pressure w.r.t. nucleon densities
    DP_alpha_Dn_n = n_alpha*Dmu_alpha_Dn_n
    DP_alpha_Dn_p = n_alpha*Dmu_alpha_Dn_p

!   derivatives of alpha particle density w.r.t. nucleon densities
    Dn_alpha_Dn_n = DP_alpha_Dn_n/T
    Dn_alpha_Dn_p = DP_alpha_Dn_p/T

!   derivatives of alpha particle density w.r.t. eta_t
    Dn_alpha_Deta_n = Dn_alpha_Dn_n*Dn_n_Deta_n + Dn_alpha_Dn_p*Dn_p_Deta_n
    Dn_alpha_Deta_p = Dn_alpha_Dn_n*Dn_n_Deta_p + Dn_alpha_Dn_p*Dn_p_Deta_p

!   derivatives of alpha particle pressure w.r.t. eta_t
    DP_alpha_Deta_n = T*Dn_alpha_Deta_n
    DP_alpha_Deta_p = T*Dn_alpha_Deta_p

!   derivatives of alpha particle chem. pot. w.r.t. eta_t
    Dmu_alpha_Deta_n = TWO*(Dmu_n_Deta_n+Dmu_p_Deta_n) - v_alpha*DP_Deta_n
    Dmu_alpha_Deta_p = TWO*(Dmu_n_Deta_p+Dmu_p_Deta_p) - v_alpha*DP_Deta_p

!   derivatives of alpha particle pressure w.r.t. eta_t
    DS_alpha_Deta_n = - n_alpha/T*Dmu_alpha_Deta_n &
                      + S_alpha/n_alpha*Dn_alpha_Deta_n
    DS_alpha_Deta_p = - n_alpha/T*Dmu_alpha_Deta_p &
                      + S_alpha/n_alpha*Dn_alpha_Deta_p

!   derivatives of alpha particle pressure w.r.t. eta_t
    DE_alpha_Deta_n = E_alpha/n_alpha*Dn_alpha_Deta_n
    DE_alpha_Deta_p = E_alpha/n_alpha*Dn_alpha_Deta_p

!   derivatives of alpha particle pressure w.r.t. eta_t
    DF_alpha_Deta_n = + n_alpha*Dmu_alpha_Deta_n &
                      + F_alpha/n_alpha*Dn_alpha_Deta_n
    DF_alpha_Deta_p = + n_alpha*Dmu_alpha_Deta_p &
                      + F_alpha/n_alpha*Dn_alpha_Deta_p

!   derivatives of alpha particle pressure w.r.t. nucleon densities
    Dmu_alpha_DT = TWO*(Dmu_n_DT + Dmu_p_DT) - DP_DT*v_alpha

!   derivative of n_alpha w.r.t. T (fixed eta)
    Dn_alpha_DT = n_alpha/T*(R_3_2 - mu_alpha/T + Dmu_alpha_DT)

!   derivative of p_alpha w.r.t. T (fixed eta)
    Dp_alpha_DT = n_alpha + T*Dn_alpha_DT

!   derivative of P_alpha w.r.t. T (fixed eta)
    DS_alpha_DT = - (Dmu_alpha_DT - mu_over_T)/T*n_alpha  &
                  + S_alpha/n_alpha*Dn_alpha_DT

!   derivative of P_alpha w.r.t. T (fixed eta)
    DE_alpha_DT = R_3_2*n_alpha + E_alpha/n_alpha*Dn_alpha_DT

!   derivative of P_alpha w.r.t. T (fixed eta)
    DF_alpha_DT = (Dmu_alpha_DT-one)*n_alpha + F_alpha/n_alpha*Dn_alpha_DT

  END SUBROUTINE ALPHA_DERIVATIVES

END MODULE Alpha_Properties_Mod
