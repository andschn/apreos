!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  If not, see <http://www.gnu.org/licenses/>.
!
MODULE Adiabatic_Index_Mod

  IMPLICIT NONE

CONTAINS 

  SUBROUTINE GET_ADIABATIC_INDEX (n,A,Z,T,P,S,E,&
             DP_Dn,DP_DT,DS_Dn,DS_DT,DE_Dn,DE_DT,gamma)

    USE Kind_Types_Mod, ONLY : DP, I4B, LGCL 
    USE Global_Variables_Mod, ONLY : IS_TEST, include_muons
    USE Physical_Constants_Mod, ONLY : energy_cgs_to_EOS, temp_mev_to_kelvin, &
                          press_cgs_to_EOS, rho_cgs_to_EOS, entropy_cgs_to_eos

    USE wrap, ONLY : wrap_timmes

    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: n,A,Z,T,P,S,E
    REAL(DP), INTENT(IN) :: DP_Dn,DP_DT
    REAL(DP), INTENT(IN) :: DS_Dn,DS_DT
    REAL(DP), INTENT(IN) :: DE_Dn,DE_DT
    REAL(DP), INTENT(OUT) :: gamma

    REAL(DP) :: P_tot
    REAL(DP) :: De_DT_tot, De_Dn_tot
    REAL(DP) :: Ds_DT_tot, Ds_Dn_tot
    REAL(DP) :: DP_DT_tot, DP_Dn_tot
    REAL(DP) :: rho,T_K,etot,ptot,stot
    REAL(DP) :: dedT,dpdT,dsdT
    REAL(DP) :: dedr,dpdr,dsdr
    REAL(DP) :: dedy,dpdy,dsdy
    REAL(DP) :: detadT,detadr,detady
    REAL(DP) :: etaele,sound
    REAL(DP) :: xxe,xxp,xxmu,xxam

!     get adiabatic index
!     TODO: make subroutine
!     convert temperature to K and density to g/cm^3 before call to wrap_timmes
      T_K = T*temp_mev_to_kelvin
      rho = n/rho_cgs_to_EOS

      CALL WRAP_TIMMES(A,Z,rho,T_K,etot,ptot,stot,&
          dedT,dpdT,dsdT,dedr,dpdr,dsdr,dedy,dpdy,dsdy,gamma,&
          etaele,detadT,detadr,detady,&
          sound,xxe,xxp,xxmu,xxam,include_muons)

      P_tot = P + ptot*press_cgs_to_EOS

      DP_Dn_tot = DP_Dn + dpdr*press_cgs_to_EOS/rho_cgs_to_EOS
      DP_DT_tot = DP_DT + dpdT*press_cgs_to_EOS*temp_mev_to_kelvin

      Ds_DT_tot = DS_DT/n + dsdt*entropy_cgs_to_EOS*temp_mev_to_kelvin
      Ds_Dn_tot = DS_Dn/n - S/n/n + dsdr*entropy_cgs_to_EOS/rho_cgs_to_EOS

      De_Dn_tot = DE_Dn/n - E/n/n + dedr*energy_cgs_to_EOS/rho_cgs_to_EOS
      De_DT_tot = DE_DT/n + dedt*energy_cgs_to_EOS*temp_mev_to_kelvin

!      Gamma = n/P_tot*DP_Dn_tot + T*(DP_DT_tot*DP_DT_tot)/(n*P_tot*De_DT_tot)

      Gamma = n/P_tot*DP_Dn_tot - n/P_tot*Ds_Dn_tot*(DP_DT_tot/Ds_DT_tot)

!      write (*,*) n, T, Z/A
!      write (*,*) P_tot, DP_Dn_tot, DP_DT_tot
!      write (*,*) De_DT_tot, Ds_Dn_tot, Ds_DT_tot
!      write (*,*)  Gamma

  END SUBROUTINE GET_ADIABATIC_INDEX

END MODULE Adiabatic_Index_Mod
