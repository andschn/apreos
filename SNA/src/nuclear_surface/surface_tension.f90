!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it AND/or modIFy
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  IF not, see <http://www.gnu.org/licenses/>.
!
MODULE Surface_Tension_Mod

  USE Kind_Types_Mod, ONLY : I4B, DP
  USE Physical_Constants_Mod, ONLY : ZERO, ONE, TWO, HALF, R_1_2, R_1_4
  USE Skyrme_Coefficients_Mod
  USE Skyrme_Bulk_Mod, ONLY : SKYRME_BULK_PROPERTIES, SKYRME_BULK_PRESSURE

  IMPLICIT NONE

CONTAINS

  FUNCTION Woods_Saxon(z,rt,at,n_ti,n_to) RESULT(WS)
!*****************************************************************************80
!
!! g_surf calculates the surface thermodinamic potential
!   considerina a woods-saxon density distribution for
!   neutron and proton densities with radii R_n, R_p
!   and thickness a_n and a_p, i.e.,
!
!   n_t(r) = n_ti + (n_to-n_ti)/(1+exp((r-R_t)/a_t)), where t = n, p
!
    IMPLICIT NONE

    REAL(DP), INTENT(IN) :: z,rt,at,n_ti,n_to
    REAL(DP) :: den, WS

    den = exp((z-rt)/at)
    if ((z-rt)/at> 200.d0) den = exp( 200.d0)
    if ((z-rt)/at<-200.d0) den = exp(-200.d0)
    WS = n_to + (n_ti - n_to)/(one+den)

  END FUNCTION Woods_Saxon

  FUNCTION Surface_Tension (r_neut, r_prot, a_neut, a_prot, &
        log10n_nin, log10n_pin, log10n_nout, log10n_pout, Temp) &
        RESULT(thermo_potential)
!*****************************************************************************80
!
!! g_surf calculates the surface thermodinamic potential
!   and from it and neutron excess obtains the surface free energy.
!   We consider woods-saxon type density distributions for both
!   neutron and proton densities with radii R_n, R_p
!   and thickness a_n and a_p, i.e.,
!   n_t(r) = n_ti + (n_to-n_ti)/(1+exp((r-R_t)/a_t))
!    where t = n, p
!
    IMPLICIT NONE

    INTEGER(I4B) :: i, imax
    REAL(DP), INTENT(IN) :: r_neut, r_prot, a_neut, a_prot
    REAL(DP), INTENT(IN) :: log10n_nin, log10n_pin
    REAL(DP), INTENT(IN) :: log10n_nout, log10n_pout
    REAL(DP), INTENT(IN) :: Temp

    ! inside region properties
    ! REAL(DP) :: log10n_nin, log10n_pin
    REAL(DP) :: n_nin, n_pin, n_in, y_in, delta_in
    REAL(DP) :: Meff_nin, Meff_pin
    REAL(DP) :: M_nin, M_pin
    REAL(DP) :: L_nin, L_pin
    REAL(DP) :: DM_nin_Dn_nin, DM_nin_Dn_pin
    REAL(DP) :: DM_pin_Dn_nin, DM_pin_Dn_pin
    REAL(DP) :: eta_nin,eta_pin
    REAL(DP) :: tau_nin,tau_pin
    REAL(DP) :: v_nin,v_pin
    REAL(DP) :: mu_nin,mu_pin
    REAL(DP) :: g1in, g2in, f1in, f2in
    REAL(DP) :: UAPRin, DUAPRin_Dn_nin, DUAPRin_Dn_pin
    REAL(DP) :: P_in, F_in, S_in, E_in
    LOGICAL :: high_density_in
    ! outside region properties
    ! REAL(DP) :: log10n_nout, log10n_pout
    REAL(DP) :: n_nout, n_pout, n_out, y_out, delta_out
    REAL(DP) :: Meff_nout, Meff_pout
    REAL(DP) :: M_nout, M_pout
    REAL(DP) :: L_nout, L_pout
    REAL(DP) :: DM_nout_Dn_nout, DM_nout_Dn_pout
    REAL(DP) :: DM_pout_Dn_nout, DM_pout_Dn_pout
    REAL(DP) :: eta_nout,eta_pout
    REAL(DP) :: tau_nout,tau_pout
    REAL(DP) :: v_nout,v_pout
    REAL(DP) :: mu_nout,mu_pout
    REAL(DP) :: g1out, g2out, f1out, f2out
    REAL(DP) :: UAPRout, DUAPRout_Dn_nout, DUAPRout_Dn_pout
    REAL(DP) :: P_out, F_out, S_out, E_out
    LOGICAL :: high_density_out
    ! interface region properties
    REAL(DP) :: log10n_n, log10n_p
    REAL(DP) :: n_n, n_p, n, y, delta
    REAL(DP) :: Meff_n, Meff_p
    REAL(DP) :: M_n, M_p
    REAL(DP) :: L_n, L_p
    REAL(DP) :: DM_n_Dn_n, DM_n_Dn_p
    REAL(DP) :: DM_p_Dn_n, DM_p_Dn_p
    REAL(DP) :: eta_n,eta_p
    REAL(DP) :: tau_n,tau_p
    REAL(DP) :: v_n,v_p
    REAL(DP) :: mu_n,mu_p
    REAL(DP) :: g1, g2, f1, f2
    REAL(DP) :: UAPR, DUAPR_Dn_n, DUAPR_Dn_p
    REAL(DP) :: P, F, S, E
    LOGICAL :: high_density
    INTEGER(I4B) :: flag

    REAL(DP) :: p4n, EXP_mp4n, DEXP_mp4n_Dn
    REAL(DP) :: k1, k2
    REAL(DP) :: P1, P2, P1F, P2F
    REAL(DP) :: Q1, Q2, Q1F, Q2F
    REAL(DP) :: Dn_n_Dz, Dn_p_Dz
    REAL(DP) :: D2n_n_Dz2, D2n_p_Dz2
    REAL(DP) :: DQ2_Dn

    REAL(DP) :: Woods_Saxon_n_0, Woods_Saxon_n_dn, Woods_Saxon_n_up
    REAL(DP) :: Woods_Saxon_p_0, Woods_Saxon_p_dn, Woods_Saxon_p_up
    REAL(DP) :: thermo_inc, thermo_potential

    REAL(DP) :: d_dens_neut_out_dz, d_dens_prot_out_dz
    REAL(DP) :: z, z0, dz, eps, tol, q_tt

    REAL(DP) :: Coeff_qnn, Coeff_qpp
    REAL(DP) :: Coeff_qnp, Coeff_qpn

    EPS = 2.D-3
    TOL = 1.D-6

    flag = 0

!   OBTAIN OUTSIDE CHEMICAL POTENTIAL AND PRESSURE
    CALL SKYRME_BULK_PROPERTIES(log10n_nout,log10n_pout,Temp,flag, &
          n_nout,n_pout,n_out,y_out,delta_out,&
          Meff_nout,Meff_pout,M_nout,M_pout,L_nout,L_pout,&
          DM_nout_Dn_nout,DM_nout_Dn_pout,DM_pout_Dn_nout,DM_pout_Dn_pout,&
          eta_nout,eta_pout,tau_nout,tau_pout,v_nout,v_pout,mu_nout,mu_pout,&
          g1out,g2out,f1out,f2out,UAPRout,DUAPRout_Dn_nout,DUAPRout_Dn_pout,&
          P_out,F_out,S_out,E_out,high_density_out)

!   OBTAIN INSIDE CHEMICAL POTENTIAL AND PRESSURE
    CALL SKYRME_BULK_PROPERTIES(log10n_nin,log10n_pin,Temp,flag, &
          n_nin,n_pin,n_in,y_in,delta_in,&
          Meff_nin,Meff_pin,M_nin,M_pin,L_nin,L_pin,&
          DM_nin_Dn_nin,DM_nin_Dn_pin,DM_pin_Dn_nin,DM_pin_Dn_pin,&
          eta_nin,eta_pin,tau_nin,tau_pin,v_nin,v_pin,mu_nin,mu_pin,&
          g1in,g2in,f1in,f2in,UAPRin,DUAPRin_Dn_nin,DUAPRin_Dn_pin,&
          P_in,F_in,S_in,E_in,high_density_in)

!   START INTEGRATING ONCE DIFFERENCE
!    BETWEEN NEUTRON DENSITY AND NEUTRON DENSSITY WITHIN tol.
    DO i = -100, -1
      z = DBLE(i)
      n_n = Woods_Saxon(Z,r_neut,a_neut,n_nin,n_nout)
      IF ((n_nin-n_n)/n_nin<tol) Z0 = Z
    ENDDO

!   SET NUMBER OF POINTS USED IN THE INTEGRAL
    imax = 2000
!   SET TIME STEPS
    dz = 1.0d-2 !-TWO*z0/DBLE(imax)
!   SET LOCATION DETERMINED ABOVE AS FIRST STEP
    i = 0
!   SET DENSITY TO INNER DENSITY TO
!   FIND OUT LAST INTEGRATION POINT
    n_n = n_nin

!   SET INTEGRAL TO ZERO
    thermo_potential = ZERO

!   PERFORM INTEGRAL UNTIL DIFFERENCE BETWEEN
!   NEUTRON DENSITY AND NEUTRON DENSITY OUTSIDE LARGER THAN tol
    DO WHILE ( (n_n-n_nout)/n_nout > tol)

!     OBTAIN POSITION ACCROSS INTERFACE
      z = z0 + dz*DBLE(i)
      i = i + 1

!     OBTAIN DENSITIES
      n_n = Woods_Saxon(Z,r_neut,a_neut,n_nin,n_nout)
      n_p = Woods_Saxon(Z,r_prot,a_prot,n_pin,n_pout)

      n = n_n + n_p
      log10n_n = log10(n_n)
      log10n_p = log10(n_p)

!     OBTAIN DENSITY DERIVATIVES NUMERICALLY
      Woods_Saxon_n_up = Woods_Saxon(z+eps,r_neut,a_neut,n_nin,n_nout)
      Woods_Saxon_n_0  = Woods_Saxon(z    ,r_neut,a_neut,n_nin,n_nout)
      Woods_Saxon_n_dn = Woods_Saxon(z-eps,r_neut,a_neut,n_nin,n_nout)

      dn_n_dz   = (Woods_Saxon_n_up - Woods_Saxon_n_dn) / (two*eps)
      d2n_n_dz2 = (Woods_Saxon_n_up - two*Woods_Saxon_n_0 + Woods_Saxon_n_dn) &
                  / (eps*eps)

      Woods_Saxon_p_up = Woods_Saxon(z+eps,r_prot,a_prot,n_pin,n_pout)
      Woods_Saxon_p_0  = Woods_Saxon(z    ,r_prot,a_prot,n_pin,n_pout)
      Woods_Saxon_p_dn = Woods_Saxon(z-eps,r_prot,a_prot,n_pin,n_pout)

      dn_p_dz   = (Woods_Saxon_p_up - Woods_Saxon_p_dn) / (two*eps)
      d2n_p_dz2 = (Woods_Saxon_p_up - two*Woods_Saxon_p_0 + Woods_Saxon_p_dn) &
                  / (eps*eps)

!   OBTAIN CHEMICAL POTENTIAL AND PRESSURE AT Z
      CALL SKYRME_BULK_PROPERTIES(log10n_n,log10n_p,Temp,flag, &
            n_n,n_p,n,y,delta,Meff_n,Meff_p,M_n,M_p,L_n,L_p,&
            DM_n_Dn_n,DM_n_Dn_p,DM_p_Dn_n,DM_p_Dn_p,&
            eta_n,eta_p,tau_n,tau_p,v_n,v_p,mu_n,mu_p,&
            g1,g2,f1,f2,UAPR,DUAPR_Dn_n,DUAPR_Dn_p,P,F,S,E,high_density)

!     OBTAIN COEFFICIENTS OF GRADIENT PART OF HAMILTONIAN
      p4n = cp4*n
      EXP_mp4n = exp(-p4n)
      DEXP_mp4n_Dn = - cp4*EXP_mp4n
      
      k1 = half*cp3 - cp5
      k2 = half*cp3 + cp5
      P1 = k1*EXP_mp4n
      P2 = k2*EXP_mp4n
      Q1 = half*P1
      Q2 = half*P2

      P1f = k1*(one-EXP_mp4n)/p4n
      P2f = k2*(one-EXP_mp4n)/p4n
      Q1f = half*P1f
      Q2f = half*P2f

      DQ2_Dn = - half*k2*DEXP_mp4n_Dn

      Q_TT = - R_1_4*(TWO*P1 + P1f - P2F)*n*(d2n_n_dz2 + d2n_p_dz2) &
            + R_1_2*(Q1 + Q2)*(n_n*d2n_n_dz2 + n_p*d2n_p_dz2) &
            - R_1_4*(Q1 - Q2)*(dn_n_dz**two + dn_p_dz**two) &
            + R_1_2*DQ2_Dn*(n_n*dn_n_dz + n_p*dn_p_dz)*(dn_n_dz + dn_p_dz)


     Coeff_qnn = - R_1_4*EXP_mp4n*(6.d0*cp5+cp4*(cp3-TWO*cp5)*(n_n+TWO*n_p)) 
     Coeff_qnp = R_1_2*R_1_4*EXP_mp4n*(4.d0*(cp3-4.d0*cp5)-3.d0*cp4*(cp3-TWO*cp5)*(n_n+n_p))
     Coeff_qpn = R_1_2*R_1_4*EXP_mp4n*(4.d0*(cp3-4.d0*cp5)-3.d0*cp4*(cp3-TWO*cp5)*(n_n+n_p))
     Coeff_qpp = - R_1_4*EXP_mp4n*(6.d0*cp5+cp4*(cp3-TWO*cp5)*(n_p+TWO*n_n))

      Q_TT = R_1_2*( Coeff_qnn*dn_n_dz*dn_n_dz &
                   + Coeff_qnp*dn_n_dz*dn_p_dz &
                   + Coeff_qpn*dn_p_dz*dn_n_dz &
                   + Coeff_qpp*dn_p_dz*dn_p_dz )
 
      thermo_inc = DZ*( F + Q_TT - F_out &
                        - mu_nout*(n_n-n_nout) - mu_pout*(n_p-n_pout) )

      thermo_potential = thermo_potential + thermo_inc

      if ( i > 1000 .and. abs(thermo_inc/thermo_potential) < 1.d-16) then
        !write (*,"(I8,10ES15.5)") i, z, a_neut, thermo_potential, &
        !thermo_inc/thermo_potential
        RETURN
      endif

    ENDDO

  END FUNCTION Surface_Tension

END MODULE Surface_Tension_Mod
