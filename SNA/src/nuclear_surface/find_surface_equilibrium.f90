!    This file is part of SRO_EOS.
!
!    SRO_EOS is free software: you can redistribute it AND/or modIFy
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    SRO_EOS is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with SRO_EOS.  IF not, see <http://www.gnu.org/licenses/>.
!
MODULE Surface_Equilibrium_Mod

  USE Kind_Types_Mod, ONLY : I4B, DP

  IMPLICIT NONE

  REAL(DP) :: prot_frac_inside, temp
!$omp threadprivate(prot_frac_inside,temp)
CONTAINS

  SUBROUTINE FIND_SURFACE_EQUILIBRIUM ( x_sol, prot_frac,  &
                                        temperature, residue, lgt )
!   x_sol(1) = inside neutron density
!   x_sol(2) = inside proton density
!   x_sol(3) = outside neutron density
!   x_sol(4) = outside proton density
!   lgt = .true. IF inside and outside proton densities different enough

    USE Kind_Types_Mod, ONLY : I4B, DP
    USE Physical_Constants_Mod, ONLY : ZERO, HALF, ONE, TWO, TEN
    USE Skyrme_Bulk_Mod, ONLY : SKYRME_BULK_PROPERTIES
    USE nwnleq_mod
    USE lautil_mod
    USE OMP_LIB

    IMPLICIT NONE

    REAL(DP), DIMENSION(4), INTENT(INOUT) :: x_sol, residue
    REAL(DP), INTENT(IN) :: prot_frac, temperature
    REAL(DP), DIMENSION(4) :: x1, x2, r
    REAL(DP), DIMENSION(4,4) :: g
    LOGICAL, INTENT(INOUT) :: lgt
    INTEGER(I4B) :: FLAG, retry
    REAL(DP) :: RES1, RES2

!   parameters for non-linear equation solver "nleqslv"
    INTEGER(I4B) :: N, MAXIT, JACFLG(1:4),  SIZE
    INTEGER(I4B) :: METHOD, GLOBAL, XSCALM, LDR, LRWORK
    INTEGER(I4B) :: NJCNT, NFCNT, ITER, TERMCD, QRWSIZ, OUTOPT(2)
    INTEGER(I4B), DIMENSION(:), ALLOCATABLE :: ICDWRK
    REAL(DP) :: XTOL, FTOL, BTOL, CNDTOL
    REAL(DP) :: STEPMX, DELTA, SIGMA, TRACE, DSUB
    REAL(DP), DIMENSION(:), ALLOCATABLE :: RJAC, RWORK, RCDWRK
    REAL(DP), DIMENSION(:), ALLOCATABLE :: QRWORK, SCALEX

    prot_frac_inside = prot_frac
    temp = temperature

    retry = 0

!   set parameters for non-linear equation solver
    N = 4 ; MAXIT = 250 ; JACFLG(1:4) = (/0,-1,-1, 1/) ; OUTOPT(1:2) = (/0,1/)
    METHOD = 0; GLOBAL = 4; XSCALM = 1; LDR = N; LRWORK = 9*N
    XTOL = 1.D-12; FTOL = 1.D-8; BTOL = 1.D-4; CNDTOL = 1.D-7
    STEPMX = -1.D0; DELTA = -1.D0; SIGMA = 0.5D0; TRACE = 1.D0; DSUB = 0.D0
    SIZE = (METHOD+1)*N*N

10  CONTINUE

    ALLOCATE(RJAC(SIZE),RWORK(9*N),RCDWRK(3*N),ICDWRK(N),SCALEX(N))
    CALL liqsiz(n,qrwsiz)
    ALLOCATE(qrwork(qrwsiz))

    RJAC = ZERO ; RWORK = ZERO; RCDWRK = ZERO ; QRWORK = ZERO ; ICDWRK = 0
    SCALEX = ONE

!   Set initial guesses for
!   inside proton fraction
    x2(1) = - 1.1d0 !- 3.d0*(prot_frac-half)/5.d0
!   inside neutron fraction
    x2(2) = x2(1) + (prot_frac-half)
!   outside neutron fraction
    x2(3) = -11.4d0 - 20.d0*(prot_frac-half) + dble(retry)
!   outside proton fraction
    x2(4) = x2(3) - 2.d0*dble(retry)
!    x2(4) = min(x2(4),50.d0)

!   if initial value not zero
!    then use them as initial guesses
    if (sum(x_sol(1:2))/=zero) x2(1:2) = x_sol(1:2)
    if (sum(x_sol(3:4))/=zero) x2(3:4) = x_sol(3:4)
    x2(4) = x2(4) - dble(retry)

    x1 = ZERO

!   solve equation for equilibrium inside AND outside nuclei
    CALL nwnleq(x2,n,scalex,maxit,jacflg,xtol,ftol,btol,cndtol,method,global, &
                xscalm,stepmx,delta,sigma,rjac,ldr,rwork,lrwork, &
                rcdwrk,icdwrk,qrwork,qrwsiz,&
                jacob_surface_equilibrium,surface_equilibrium,outopt,x1, &
                r,g,njcnt,nfcnt,iter,termcd)
    IF (TERMCD.NE.1) write (*,"(10es15.6,i4)") PROT_FRAC, TEMP, X1, R, termcd
    deallocate(RJAC, RWORK, RCDWRK, ICDWRK, QRWORK, SCALEX)
!   chech whether solution x1 found is actually a solution to eq being solved.
!   sotemimes output for x1 is not a solution, but a point where nlwleq stalled.
    flag = 0
    CALL surface_equilibrium( x1, r, n, flag )
    residue = r

    lgt = .FALSE.
!   IF x1 is a solution THEN set as so
    IF (DOT_PRODUCT(r,r)<1.d-16) THEN
      x_sol = x1
      ! only set as true solution with inside/outside matter
      ! if inside/outside densities are different
      RES1 = ABS((x_sol(1)-x_sol(3))/x_sol(1))
      RES2 = ABS((x_sol(2)-x_sol(4))/x_sol(2))
      if (res1>1.d-3 .and. res2>1.d-3) lgt = .true.
      !if (x_sol(4)>x_sol(3)) lgt = .false.
      if (lgt) return
    ENDIF

!   sometimes solution is not found with one method of nleqslv
!   but a different method can find the solution
    IF (retry<3) THEN
      retry = retry+1
!        IF (retry==1) nlglobal = 0
!        IF (retry==2) nlglobal = 2
!        IF (retry==2) nlglobal = 4
      IF (retry==1) global = 5
      IF (retry==2) global = 6
      GOTO 10
    ENDIF

    RETURN

CONTAINS

  SUBROUTINE surface_equilibrium ( x, s, m, surface_flag )

    USE Kind_Types_Mod, ONLY : I4B, DP
    USE Physical_Constants_Mod, ONLY : ZERO, HALF, ONE, TWO, TEN
    USE Skyrme_Bulk_Mod, ONLY : SKYRME_BULK_PROPERTIES
!
    IMPLICIT NONE

    INTEGER(I4B), INTENT(IN) :: m, surface_flag
    INTEGER(I4B) :: flag
    REAL(DP), INTENT(IN)  :: x(m)
    REAL(DP), INTENT(OUT) :: s(m)
    ! inside region properties
    REAL(DP) :: log10n_nin, log10n_pin
    REAL(DP) :: n_nin, n_pin, n_in, y_in, delta_in
    REAL(DP) :: Meff_nin, Meff_pin
    REAL(DP) :: M_nin, M_pin
    REAL(DP) :: L_nin, L_pin
    REAL(DP) :: DM_nin_Dn_nin, DM_nin_Dn_pin
    REAL(DP) :: DM_pin_Dn_nin, DM_pin_Dn_pin
    REAL(DP) :: eta_nin,eta_pin
    REAL(DP) :: tau_nin,tau_pin
    REAL(DP) :: v_nin,v_pin
    REAL(DP) :: mu_nin,mu_pin
    REAL(DP) :: g1in, g2in, f1in, f2in
    REAL(DP) :: UAPRin, DUAPRin_Dn_nin, DUAPRin_Dn_pin
    REAL(DP) :: p_in, f_in, s_in, e_in
    LOGICAL :: high_density_in
    ! outside region properties
    REAL(DP) :: log10n_nout, log10n_pout
    REAL(DP) :: n_nout, n_pout, n_out, y_out, delta_out
    REAL(DP) :: Meff_nout, Meff_pout
    REAL(DP) :: M_nout, M_pout
    REAL(DP) :: L_nout, L_pout
    REAL(DP) :: DM_nout_Dn_nout, DM_nout_Dn_pout
    REAL(DP) :: DM_pout_Dn_nout, DM_pout_Dn_pout
    REAL(DP) :: eta_nout,eta_pout
    REAL(DP) :: tau_nout,tau_pout
    REAL(DP) :: v_nout,v_pout
    REAL(DP) :: mu_nout,mu_pout
    REAL(DP) :: g1out, g2out, f1out, f2out
    REAL(DP) :: UAPRout, DUAPRout_Dn_nout, DUAPRout_Dn_pout
    REAL(DP) :: p_out, f_out, s_out, e_out
    LOGICAL :: high_density_out

    log10n_nin = X(1)
    log10n_pin = X(2)

    flag = 0

    CALL SKYRME_BULK_PROPERTIES(log10n_nin,log10n_pin,Temp,flag, &
          n_nin,n_pin,n_in,y_in,delta_in,Meff_nin,Meff_pin,&
          M_nin,M_pin,L_nin,L_pin,&
          DM_nin_Dn_nin,DM_nin_Dn_pin,DM_pin_Dn_nin,DM_pin_Dn_pin,&
          eta_nin,eta_pin,tau_nin,tau_pin,v_nin,v_pin,mu_nin,mu_pin,&
          g1in,g2in,f1in,f2in,UAPRin,DUAPRin_Dn_nin,DUAPRin_Dn_pin,&
          p_in,f_in,s_in,e_in,high_density_in)

    log10n_nout = X(3)
    log10n_pout = X(4)

    n_nout = TEN**log10n_nout
    n_pout = TEN**log10n_pout

    CALL SKYRME_BULK_PROPERTIES(log10n_nout,log10n_pout,Temp,flag, &
          n_nout,n_pout,n_out,y_out,delta_out,Meff_nout,Meff_pout,&
          M_nout,M_pout,L_nout,L_pout,&
          DM_nout_Dn_nout,DM_nout_Dn_pout,DM_pout_Dn_nout,DM_pout_Dn_pout,&
          eta_nout,eta_pout,tau_nout,tau_pout,v_nout,v_pout,mu_nout,mu_pout,&
          g1out,g2out,f1out,f2out,UAPRout,DUAPRout_Dn_nout,DUAPRout_Dn_pout,&
          p_out,f_out,s_out,e_out,high_density_out)

    S(1) = (p_in  - p_out)*1.d1
    S(2) = (mu_nin   - mu_nout)
    S(3) = (mu_pin   - mu_pout)
    S(4) = (y_in - prot_frac_inside)

    RETURN
  END SUBROUTINE surface_equilibrium

  SUBROUTINE jacob_surface_equilibrium ( jac, ldr, x1, m )
!
    USE Kind_Types_Mod, ONLY : I4B, DP
    USE Physical_Constants_Mod, ONLY : ZERO

    IMPLICIT NONE

    INTEGER(I4B), INTENT(IN) :: m, ldr
    REAL(DP), INTENT(IN)  :: x1(m)
    REAL(DP), INTENT(OUT) :: jac(ldr,*)

    jac(1:ldr,1:m) = zero

    RETURN
  END SUBROUTINE jacob_surface_equilibrium

  END SUBROUTINE FIND_SURFACE_EQUILIBRIUM

END MODULE Surface_Equilibrium_Mod
