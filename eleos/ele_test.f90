program ele_test

  use physical_constants_mod, &
       only : kb_erg, avo, clite, temp_mev_to_kelvin, &
       m_n_cgs, neutron_mass_MeV, rho_cgs_to_eos

  implicit none

  double precision :: rho, temp, ye, abar
  double precision :: e_th

  integer :: i, j, k, l

  rho = 1.d-5 / rho_cgs_to_eos
  temp = 1.0d0
  ye = 0.3d0

  abar = 1.0D+00

  call thermal_energy(rho,temp,ye,abar,e_th,.FALSE.)

  write (*,"(5ES15.6)") rho,temp,ye,abar,e_th*rho

  stop

end program
