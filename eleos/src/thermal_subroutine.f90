module thermal_energy_mod

  use wrap, only : wrap_timmes
  use physical_constants_mod, &
       only : kb_erg, avo, clite, temp_mev_to_kelvin, &
       m_n_cgs, neutron_mass_MeV, mass_e

  implicit none

end module thermal_energy_mod

subroutine thermal_energy(rho,temp,ye,abar,e_thermal,inc_muons)

  use thermal_energy_mod
  implicit none

  double precision, intent(in) :: rho, temp, ye, abar
  double precision, intent(out) :: e_thermal
  logical, intent(in) :: inc_muons

  double precision :: temp_cgs, amu_cgs
  double precision :: e_timmes, e_baryons, e_photons
  double precision :: A, Z
  double precision :: etot, ptot, stot
  double precision :: dedT, dpdT, dsdT
  double precision :: dedr, dpdr, dsdr
  double precision :: dedy, dpdy, dsdy
  double precision :: xxe, xxp, xxmu, xxam
  double precision :: gamma, etaele, sound

  double precision, parameter :: amu = 931.49432d0

!   convert temp to cgs
  temp_cgs = temp*temp_mev_to_kelvin

!   convert amu to cgs
  amu_cgs = m_n_cgs*amu/neutron_mass_MeV

!   timmes eos already include photons
!    e_photons = (4.d0*avo)*Temp**4./rho

  e_baryons = 3.d0/2.d0*kb_erg*Temp_cgs/(Abar*amu_cgs)

  A = abar
  Z = ye*abar

  call wrap_timmes(A,Z,rho,temp_cgs,etot,ptot,stot, &
       dedT,dpdT,dsdT,dedr,dpdr,dsdr,dedy,dpdy,dsdy,&
       gamma,etaele,sound,xxe,xxp,xxmu,xxam,inc_muons)

  write (*,*) 
  write (*,"(4ES15.6)") A,Z,rho,temp_cgs
  write (*,"(4ES15.6)") etot,ptot,stot
  write (*,"(4ES15.6)") dedT,dpdT,dsdT
  write (*,"(4ES15.6)") dedr,dpdr,dsdr
  write (*,"(4ES15.6)") dedy,dpdy,dsdy
  write (*,"(4ES15.6)") gamma,etaele,sound
  write (*,"(4ES15.6)") xxe,xxp,xxmu,xxam
  write (*,*) inc_muons
  write (*,*) 

  e_timmes = etot

  e_thermal = e_timmes !e_baryons + e_timmes !+ e_photons

  write (*,"(3ES20.12)") temp*etaele + mass_e

end subroutine thermal_energy

